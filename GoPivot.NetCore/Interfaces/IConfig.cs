﻿using GoPivot.NetCore.Configuration;

namespace GoPivot.NetCore.Interfaces
{
    public interface IConfig
    {   
        BrowserType? GetBrowser();
        string GetWebSite();
        string GetChromePath();
        int GetPageLoadTimeOut();
        int GetElementLoadTimeOut();
        AppSetting GetSettings();
    }
}
