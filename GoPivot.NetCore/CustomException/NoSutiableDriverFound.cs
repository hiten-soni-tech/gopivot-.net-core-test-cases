﻿using System;

namespace GoPivot.NetCore.CustomException
{
    public class NoSutiableDriverFound :Exception
    {
        public NoSutiableDriverFound(string msg) : base(msg)
        {
            
        }
    }
}
