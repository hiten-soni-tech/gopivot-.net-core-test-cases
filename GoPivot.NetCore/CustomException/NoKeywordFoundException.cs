﻿using System;

namespace GoPivot.NetCore.CustomException
{
    public class NoSuchKeywordFoundException : Exception
    {
        public NoSuchKeywordFoundException(string msg) : base(msg)
        {
            
        }
    }
}
