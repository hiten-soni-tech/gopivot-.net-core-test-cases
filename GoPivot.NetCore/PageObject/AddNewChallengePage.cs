﻿using GoPivot.NetCore.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using GoPivot.NetCore.ComponentHelper;
using System.Threading;

namespace GoPivot.NetCore.PageObject
{
    public class AddNewChallengePage : PageBase
    {


        private IWebDriver driver;
        public IWebElement ChallengeName
        {
            get
            {
                return driver.FindElement(By.Id("challengeName"));
            }
        }

        public IWebElement ChallengeStartDate
        {
            get
            {
                return driver.FindElement(By.XPath("//md-datepicker[@name='startDate']/div[@class='md-datepicker-input-container']/input"));
            }
        }


        public IWebElement ChallengeEndDate
        {
            get
            {
                return driver.FindElement(By.XPath("//md-datepicker[@name='endDate']/div[@class='md-datepicker-input-container']/input"));
            }
        }

        public IWebElement GoalValue
        {
            get
            {
                return driver.FindElement(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/input"));
            }
        }

        public IWebElement Timeline
        {
            get
            {
                return driver.FindElement(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/select"));
            }
        }

        public IWebElement Description
        {
            get
            {
                return driver.FindElement(By.Id("goal"));
            }
        }

        public IWebElement CustomChallenge_PerformAction
        {
            get
            {
                return driver.FindElement(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/input[1]"));
            }
        }

        public IWebElement CustomChallenge_GoalValue
        {
            get
            {
                return driver.FindElement(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/input[2]"));
            }
        }


        public IWebElement Step1_NextButton
        {
            get
            {
                return driver.FindElement(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/div/div[2]/button[2]"));
            }
        }

        public IWebElement Step2_SearchParticipantText
        {
            get
            {
                return driver.FindElement(By.XPath("//input[@placeholder = 'Search participants']"));
            }
        }

        public IList<IWebElement> Step2_ParticipantList
        {
            get
            {
                return driver.FindElements(By.XPath("//div[@class = 'scrollableList']/div[@class='ng-isolate-scope']/div[@class='ng-scope']"));
            }
        }

        public IWebElement Step2_CreateButton
        {
            get
            {
                return driver.FindElement(By.XPath("//button[contains(.,'Create')]"));
            }
        }

        public IWebElement Step2_SaveButton
        {
            get
            {
                return driver.FindElement(By.XPath("//button[contains(.,'Save')]"));
            }
        }

        public IWebElement Step2_EveryOne
        {
            get
            {
                return driver.FindElement(By.XPath("//button[contains(.,'Everyone')]"));
            }
        }

        public IWebElement Step2_Teams
        {
            get
            {
                return driver.FindElement(By.XPath("//button[contains(.,'Teams')]"));
            }
        }


        public AddNewChallengePage(IWebDriver _driver) : base(_driver)
        {


            this.driver = _driver;
        }

        public bool FillStep1(string challengeName, string startDate, string endDate, string goalValue, string timeline, string description, out string errorMessage)
        {
            bool success = false;
            errorMessage = string.Empty;
            try
            {
                ChallengeName.Clear();
                ChallengeName.SendKeys(challengeName);
                ChallengeStartDate.SendKeys(startDate);
                ChallengeEndDate.SendKeys(endDate);
                GoalValue.SendKeys(goalValue);
                ComboBoxHelper.SelectElementByValue(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/select"), timeline);
                Description.SendKeys(description);
                success = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            return success;

        }

        public bool FillStepForJumpStart(string challengeName, string startDate, string endDate, string description, out string errorMessage)
        {
            bool success = false;
            errorMessage = string.Empty;
            try
            {
                ChallengeName.Clear();
                ChallengeName.SendKeys(challengeName);
                ChallengeStartDate.SendKeys(startDate);
                ChallengeEndDate.SendKeys(endDate);
                Description.SendKeys(description);
                success = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            return success;

        }


        public bool FillStepForCustom(string challengename, string startdate, string enddate, string customchallengeactiontext, string customchallengegoal, string customchallengeunit, string customchallengetime, string goalText, out string errorMessage)
        {
            bool success = false;
            errorMessage = string.Empty;
            try
            {
                ChallengeName.Clear();
                ChallengeStartDate.Clear();
                ChallengeEndDate.Clear();
                CustomChallenge_GoalValue.Clear();
                CustomChallenge_PerformAction.Clear();
                Description.Clear();
                ChallengeName.SendKeys(challengename);

                ChallengeStartDate.SendKeys(startdate);
                ChallengeEndDate.SendKeys(enddate);
                CustomChallenge_PerformAction.SendKeys(customchallengeactiontext);
                CustomChallenge_GoalValue.SendKeys(customchallengegoal);

                ComboBoxHelper.SelectElementByValue(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/select[1]"), customchallengeunit);
                var customChallengeTimeBox = ComboBoxHelper.GetAllItem(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/select[2]"));
                var selectedValue = customChallengeTimeBox.FirstOrDefault(p => p.Contains(customchallengetime));
                if (!string.IsNullOrEmpty(selectedValue))
                {
                    ComboBoxHelper.SelectElementByValue(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/select[2]"), selectedValue);
                    Description.SendKeys(goalText);
                    success = true;
                }
                else
                {
                    errorMessage = "No Custom Challenge Time value found";
                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            return success;

        }

        public bool AddChallengers(string challengername, out string errorMessage)
        {
            bool success = false;
            errorMessage = string.Empty;
            try
            {
                Step2_SearchParticipantText.Clear();
                Step2_SearchParticipantText.SendKeys(challengername);
                Thread.Sleep(3000);
                string xPath = string.Format("//div[@class = 'user ng-scope' and contains(.,'{0}')]", challengername);
                var participantElements = driver.FindElements(By.XPath(xPath));
                int participantsAdded = 0;
                foreach (var element in participantElements)
                {
                    element.Click();
                    participantsAdded++;
                }
                if (participantsAdded > 0)
                {
                    success = true;
                }
                else
                {
                    errorMessage = "No Participants added";
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            return success;
        }
    }
}
