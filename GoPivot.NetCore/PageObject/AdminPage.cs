﻿using GoPivot.NetCore.BaseClasses;
using System;
using System.Collections.Generic;
using System.Threading;
using GoPivot.NetCore.ComponentHelper;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.DataEntities;
using OpenQA.Selenium.Interactions;

namespace GoPivot.NetCore.PageObject
{
    public class AdminPage : PageBase
    {




        private IWebDriver driver;

        public IWebElement Clients
        {
            get
            {
                return driver.FindElement(By.LinkText("Clients"));
            }
        }


        public IWebElement EditClient
        {
            get
            {
                return driver.FindElement(By.LinkText("Edit"));
            }
        }

        public IList<IWebElement> ProgramTabs
        {
            get
            {
                return driver.FindElements(By.XPath("/html/body/div/div/div/div/div[2]/div/div/div/div/div/ul/li"));
            }
        }

        public IWebElement AddChallengeButton;


        public AdminPage(IWebDriver _driver) : base(_driver)
        {

            this.driver = _driver;
        }






        private WebDriverWait GetWebDriverWait(IWebDriver driver, TimeSpan timeout)
        {
            WebDriverWait wait = new WebDriverWait(driver, timeout)
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        public IWebElement GetProgramTab(string tabName)
        {
            try
            {
                string path = string.Format("//ul[@class = 'nav nav-tabs']/li/a[contains(.,'{0}')]", tabName);
                WebDriverWait wait = GetWebDriverWait(driver, TimeSpan.FromSeconds(60));
                return wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(path)));
            }
            catch
            {
                return null;
            }

        }

        public IWebElement GetChallengeCategory(string category)
        {
            try
            {
                string path = string.Format("//div[@class='col-xs-12']/button[contains(.,'{0}')]", category);
                WebDriverWait wait = GetWebDriverWait(driver, TimeSpan.FromSeconds(60));
                return wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(path)));
            }
            catch
            {
                return null;
            }
        }

        public IWebElement GetClient(string client)
        {
            try
            {
                Thread.Sleep(3000);
                return driver.FindElement(By.LinkText(client));
            }
            catch
            {
                // element not found
                return null;
            }
        }

        public IWebElement GetProgram(string program)
        {
            try
            {
                Thread.Sleep(3000);
                return driver.FindElement(By.XPath(string.Format("//h3[contains(text(),'{0}')]", program)));
            }
            catch
            {
                return null;
            }
        }

        public IWebElement GetChallengeType(string challengeType)
        {
            try
            {
                return driver.FindElement(By.XPath(string.Format("//div[contains(@class,'challengeTemplate ng-scope')]/div[@class='templateName ng-binding' and contains(text(),'{0}')]", challengeType)));
            }
            catch
            {
                return null;
            }
        }

        public void GetAddChallengeButton()
        {

            try
            {
                string path = "/html/body/div/div/div/div/div[2]/div/div/div/div/div/div/div[6]/ng-include/div/div[1]/div[3]/button";
                WebDriverWait wait = GetWebDriverWait(driver, TimeSpan.FromSeconds(60));
                AddChallengeButton = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(path)));
            }
            catch
            {
                AddChallengeButton = null;
            }
            //bool staleElement = true;
            //int i = 0;
            //while (staleElement && i < 5)
            //{
            //    try
            //    {
            //        AddChallengeButton = driver.FindElement(By.XPath("/html/body/div/div/div/div/div[2]/div/div/div/div/div/div/div[6]/ng-include/div/div[1]/div[3]/button"));
            //        staleElement = false;
            //    }
            //    catch (StaleElementReferenceException e)
            //    {
            //        i++;
            //        AddChallengeButton = null;
            //    }
            //}
            //if (AddChallengeButton != null && !AddChallengeButton.Displayed)
            //{
            //    AddChallengeButton = null;
            //}
        }

        public NewActivity CreateNewActivity(NewActivity newActivity, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {

                Thread.Sleep(4000);
                // Step 1
                string randomNumber = new Random().Next(1000).ToString();
                string activityName = newActivity.Name.Replace("{randomnumber}", randomNumber);
                newActivity.Name = activityName;
                GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.Id("name"))).SendKeys(activityName);

                ComboBoxHelper.SelectElement(By.XPath("//select[@ng-model = 'model.activitySectionId']"),
                    newActivity.Section);
                ComboBoxHelper.SelectElement(By.XPath("//select[@ng-model = 'model.activityCategoryId']"), newActivity.Type);
                Thread.Sleep(5000);
                if (newActivity.HideActivityEndDate)
                {
                    CheckBoxHelper.CheckedCheckBox(By.XPath("//md-checkbox[@ng-model = 'model.hideActivityEndDate']"));
                }
                Thread.Sleep(3000);
                driver.FindElement(By.Id("activeDateRange")).Click();
                Thread.Sleep(2000);
                var element = GetWebDriverWait(driver, TimeSpan.FromSeconds(30)).Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[contains(@class,'dropup') and contains(@style,'display: block')]/div[contains(@class,'ranges')]/ul/li[contains(.,'{0}')]", newActivity.ActivityRangeMonths))));
                element.Click();
                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();

                Thread.Sleep(1000);
                ComboBoxHelper.SelectElement(By.XPath("//select[@ng-model = 'model.activityTypeId']"), newActivity.TypeOfActivity);
                Thread.Sleep(2000);
                if (string.IsNullOrEmpty(newActivity.ChallengeName))
                {
                    // Step 2
                    if (!string.IsNullOrEmpty(newActivity.Metric))
                    {
                        ComboBoxHelper.SelectElement(By.XPath("//select[@ng-model = 'model.metricId']"), newActivity.Metric);
                    }
                    if (newActivity.TypeOfActivity == "Recurring")
                    {
                        ComboBoxHelper.SelectElement(By.XPath("//select[@ng-model = 'model.recurrence']"), newActivity.RecurringType);
                    }
                    Thread.Sleep(1000);
                    if (newActivity.IsCompletionMeasured)
                    {
                        driver.FindElement(By.XPath("//input[@ng-model='model.pointValue']")).Clear();
                        driver.FindElement(By.XPath("//input[@ng-model='model.pointValue']")).SendKeys(newActivity.MeasurePointValue.ToString());
                    }
                    else
                    {
                        driver.FindElement(By.XPath("//input[@ng-model='thresh.value']")).Clear();
                        driver.FindElement(By.XPath("//input[@ng-model='thresh.pointValue']")).Clear();

                        driver.FindElement(By.XPath("//input[@ng-model='thresh.value']"))
                            .SendKeys(newActivity.MeasurePointValue.ToString());
                        driver.FindElement(By.XPath("//input[@ng-model='thresh.pointValue']")).SendKeys(newActivity.MeasureAward.ToString());
                    }
                }
                else
                {
                    ComboBoxHelper.SelectElement(By.XPath("//select[@ng-model = 'model.challengeId']"), newActivity.ChallengeName);
                    if (newActivity.JoinChallengePoints > 0)
                    {
                        driver.FindElement(By.XPath("//md-switch[@ng-model='model.challengeJoinEnabled']")).Click();
                        Thread.Sleep(2000);
                        driver.FindElement(By.XPath("//input[@ng-model='model.challengeJoinPoints']")).SendKeys(newActivity.JoinChallengePoints.ToString());
                        Thread.Sleep(2000);
                    }
                }
                driver.FindElement(By.XPath("//input[@ng-model='model.displayMinPoints']")).Clear();
                driver.FindElement(By.XPath("//input[@ng-model='model.displayMinPoints']"))
                    .SendKeys(newActivity.MinimumPoints.ToString());
                driver.FindElement(By.XPath("//input[@ng-model='model.displayMaxPoints']")).Clear();
                driver.FindElement(By.XPath("//input[@ng-model='model.displayMaxPoints']"))
                    .SendKeys(newActivity.MaximumPoints.ToString());
                if (newActivity.HideNonPointRewardTransaction)
                {
                    CheckBoxHelper.CheckedCheckBox(By.XPath("//md-checkbox[@ng-model='model.hideNonPointAwardsInActivityHistory']"));
                }
                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();

                if (!string.IsNullOrEmpty(newActivity.ExcludedActivity))
                {
                    driver.FindElement(By.XPath("//md-select[@ng-model='excludedActivities.selected']")).Click();

                    Thread.Sleep(5000);
                    var textSearchElement = GetWebDriverWait(driver, TimeSpan.FromSeconds(60)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//input[@ng-model='excludedActivities.searchTerm']")));
                    textSearchElement.SendKeys(newActivity.ExcludedActivity);
                    Thread.Sleep(5000);
                    GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[contains(@class,'selectHeaderSelect')]/md-select-menu/md-content/md-optgroup/md-option[1]"))).Click();

                    Thread.Sleep(2000);


                    Actions actions = new Actions(driver);
                    actions.MoveByOffset(300, 0).Click().Perform();
                    Thread.Sleep(3000);
                }

                if (newActivity.OnlyDisplayForSyncDevices)
                {
                    driver.FindElement(By.XPath("//md-checkbox[@ng-model='model.limitToUsersWithSyncedDevices']")).Click();
                }

                try
                {
                    driver.FindElement(By.XPath("//button[contains(.,'Stay on page')]")).Click();
                }
                catch
                {
                    // Do nothing
                }
                // Step 3
                Thread.Sleep(1000);
                // 
                IWebElement saveButtonElement = driver.FindElement(By.XPath("//button[contains(.,'Save')]"));

                saveButtonElement.Click();

            }
            catch (Exception ex)
            {
                newActivity = null;
                errorMessage = ex.ToString();
            }
            return newActivity;
        }
        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}
