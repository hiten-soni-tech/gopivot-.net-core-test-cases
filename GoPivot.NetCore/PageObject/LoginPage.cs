﻿using OpenQA.Selenium;
using GoPivot.NetCore.BaseClasses;

namespace GoPivot.NetCore.PageObject
{
    public class LoginPage : PageBase
    {
        private IWebDriver driver;


        #region WebElement
       
        //private IWebElement LoginTextBox => driver.FindElement(By.Id("Bugzilla_login"));

     
        //private IWebElement LoginButton => driver.FindElement(By.Id ("log_in"));

        public IWebElement LoginTextBox

        {
            get { 
            return driver.FindElement(By.Id("username"));
            }
        }

        public IWebElement PassTextBox
        {
            get { 
            return driver.FindElement(By.Id("password"));
            }

        }

        public IWebElement LoginButton
        {
            get{
                return driver.FindElement(By.XPath("/html/body/div/div/div/div/div[2]/form/div[4]/button"));

            }
        }

        #endregion

        public LoginPage(IWebDriver _driver) : base(_driver)
        {

            this.driver = _driver;

        }



    }
}
