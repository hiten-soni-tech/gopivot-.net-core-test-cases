﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{

    public class PIV1530Data
    {
        public NewActivity NewActivity { get; set; }
        public ActivityContent Content { get; set; }
    }

   
    public class ActivityContent
    {
        public string PointsDisplayedAttributeLabel { get; set; }
        public string PointsDisplayedText { get; set; }
        public string MinimumPointsLabel { get; set; }
        public string MaximumPointsLabel { get; set; }
    }

}
