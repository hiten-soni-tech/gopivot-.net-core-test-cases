﻿namespace GoPivot.NetCore.DataEntities
{
    public class PIV1580Data
    {
      
        public NutritionPageText NutritionPageText { get; set; }
    }
    public class NutritionPageText
    {
        public string HeadingText { get; set; }
        public string DailyTotalText { get; set; }
    }
}
