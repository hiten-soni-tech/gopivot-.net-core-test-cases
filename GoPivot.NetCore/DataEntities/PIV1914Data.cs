﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{
    public class PIV1914Data
    {
        public NewActivity NewActivity { get; set; }
        public int BeforeCreateActivityPoints { get; set; }
        public int AfterCreateActivityPoints { get; set; }
    }
}
