﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{
    public class PIV1112Data
    {
        public string Title { get; set; }
        public string ModalSection1 { get; set; }
        public string ModalSection2 { get; set; }
        public string ModalSection3 { get; set; }

    }
}
