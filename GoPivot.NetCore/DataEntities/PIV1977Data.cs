﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{

    public class PIV1977Data
    {
        public string InstructionsText { get; set; }
        public string InformationText { get; set; }
    }

}
