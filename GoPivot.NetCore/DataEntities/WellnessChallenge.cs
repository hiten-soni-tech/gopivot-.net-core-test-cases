﻿namespace GoPivot.NetCore.DataEntities
{
    public class WellnessChallenge
    {
        public string ChallengeName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int GoalValue { get; set; }
        public string TimeLine { get; set; }
        public string Description { get; set; }
        public string ChallengerName { get; set; }
        public string WellnessChallengeType { get; set; }
    }
}
     