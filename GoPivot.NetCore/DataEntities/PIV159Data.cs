﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{

    public class PIV159Data
    {
        public string TabName { get; set; }
        public string PageTitle { get; set; }
        public string SearchBoxResultLabel { get; set; }
        public string SearchBoxLabel { get; set; }
        public string ShowCompletedLabel { get; set; }
        public string AddButtonText { get; set; }
        public Tablecolumns TableColumns { get; set; }
        public int Pagination { get; set; }
        public string NoSearchResultLabel { get; set; }
    }

    public class Tablecolumns
    {
        public string StartDateLabel { get; set; }
        public string EndDateLabel { get; set; }
        public string NameLabel { get; set; }
        public string IdLabel { get; set; }
    }

}
