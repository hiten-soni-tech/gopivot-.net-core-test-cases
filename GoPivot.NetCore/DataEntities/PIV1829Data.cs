﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{

    public class PIV1829Data
    {
        public ExerciseGoal[] ExerciseGoals { get; set; }
    }

    public class ExerciseGoal
    {
        public string Name { get; set; }
        public string TimeText { get; set; }
        public int Time { get; set; }
        public string GoalError { get; set; }
        public bool Validate { get; set; }
        public int DisplayOrder { get; set; }

    }


}
