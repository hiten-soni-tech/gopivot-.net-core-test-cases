﻿namespace GoPivot.NetCore.DataEntities
{
    public class Administrator
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Client { get; set; }
        public string Program { get; set; }

    }
}
