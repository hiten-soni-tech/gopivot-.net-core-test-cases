﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{

    public class PIV1615Data
    {
        public Exerciseprogram[] ExerciseProgram { get; set; }
    }

    public class Exerciseprogram
    {
        public string Name { get; set; }
        public int VideoCount { get; set; }
    }

}
