﻿namespace GoPivot.NetCore.DataEntities
{
    public class CustomChallenge
    {
        public string ChallengeName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CustomChallengeActionText { get; set; }
        public int CustomChallengeGoal { get; set; }
        public string CustomChallengeUnit { get; set; }
        public string CustomChallengeTime { get; set; }
        public string GoalText { get; set; }
        public string ChallengerName { get; set; }
        public string[] Teams { get; set; }
    }
}
