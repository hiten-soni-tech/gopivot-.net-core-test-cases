﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{
    public class NewActivity
    {
        // Step 1
        public string Name { get; set; }
        public string Section { get; set; }
        public string Type { get; set; }
        public string ActivityRangeMonths { get; set; }
        public bool HideActivityEndDate { get; set; }

        // Step 2

        public string TypeOfActivity { get; set; }
        public string RecurringType { get; set; }
        public string Metric { get; set; }
        public int MinimumPoints { get; set; }
        public int MaximumPoints { get; set; }
        public bool HideNonPointRewardTransaction { get; set; }
        public bool IsCompletionMeasured { get; set; }
        public int MeasurePointValue { get; set; }
        public int MeasureAward { get; set; }

        public string ExcludedActivity { get; set; }
        public string ChallengeName { get; set; }
        public int JoinChallengePoints { get; set; }

        public bool OnlyDisplayForSyncDevices { get; set; }

    }
}
