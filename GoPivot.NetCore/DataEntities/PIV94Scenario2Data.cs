﻿namespace GoPivot.NetCore.DataEntities
{

    public class PIV94Scenario2Data
    {
     
        public Challenge Challenge { get; set; }
    }

   

    public class Challenge
    {
        public string ChallengeName { get; set; }
        public string WellnessChallengeType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Description { get; set; }
        public string ChallengerName { get; set; }
    }

}
