﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{
    public class Assessmentsteps
    {
        public Step1 Step1 { get; set; }
        public Step2 Step2 { get; set; }
        public Step3 Step3 { get; set; }
    }

    public class Step1
    {
        public string BreadcrumbText { get; set; }
        public string Option1Text { get; set; }
        public string Option1Summary { get; set; }
        public string Option2Text { get; set; }
        public string Option2Summary { get; set; }
        public string PageSummary { get; set; }
    }

    public class Step2
    {
        public string BreadcrumbText { get; set; }
    }

    public class Step3
    {
        public string BreadcrumbText { get; set; }
        public string PageSummary { get; set; }
    }
}
