﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{

    public class BuildExercisePlan
    {
        public int HeightFeet { get; set; }
        public int HeightInch { get; set; }
        public int Weight { get; set; }
        public int Age { get; set; }
        public string ExerciseGoal { get; set; }
        public string DaysOfExercise { get; set; }
        public string PhysicalActive { get; set; }
        public string AccessToEquipment { get; set; }
    }
}
