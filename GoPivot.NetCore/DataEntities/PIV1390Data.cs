﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{

    public class PIV1390Data
    {
        public string Header { get; set; }
        public string Week1Label { get; set; }
        public string Week2Label { get; set; }
        public string[] WorkoutTypes { get; set; }
    }

}
