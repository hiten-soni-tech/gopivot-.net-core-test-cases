﻿namespace GoPivot.NetCore.DataEntities
{
    public class PIV93Data
    {
        public CustomChallenge ChallengeNotStarted { get; set; }
        public CustomChallenge ChallengeAlreadyStarted { get; set; }
    }
}
