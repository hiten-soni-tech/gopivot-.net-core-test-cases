﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{
    public class PIV1596Data
    {
        public CustomChallenge NewChallenge { get; set; }
        public InboxContent InboxContent { get; set; }
    }

    public class InboxContent
    {
        public string Subject { get; set; }
    }
}
