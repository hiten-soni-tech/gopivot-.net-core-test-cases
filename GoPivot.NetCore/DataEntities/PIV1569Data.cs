﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{

    public class PIV1569Data
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageName { get; set; }
        public string HighResolutionImageName { get; set; }
        public int Points { get; set; }
    }

}
