﻿namespace GoPivot.NetCore.DataEntities
{

    public class PIV94Scenario1Data
    {
        public PIV94Scenarion1Admin Administrator { get; set; }
        public PIV94Scenario1Participant Participant { get; set; }
    }

    public class PIV94Scenarion1Admin
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Client { get; set; }
        public string Program { get; set; }
        public string WellnessChallengeType { get; set; }
        public string ChallengeName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int GoalValue { get; set; }
        public string Timeline { get; set; }
        public string Description { get; set; }
        public string ChallengerName { get; set; }
    }

    public class PIV94Scenario1Participant
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ChallengeName { get; set; }
        public string ChallengerName { get; set; }
        public string GoalDescription { get; set; }
    }

}
