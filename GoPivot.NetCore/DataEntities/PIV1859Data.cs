﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{

    public class PIV1859Data
    {
        public CustomChallenge Challenge { get; set; }
        public Challenger[] Challengers { get; set; }
        public string GeneratedChallengeName { get; set; }
    }

     
    public class Challenger
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
    }

}
