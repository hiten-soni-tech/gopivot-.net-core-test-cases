﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities
{

    public class PIV1862Data
    {
        public string ChallengeTemplate { get; set; }
        public string AssessmentNameLabel { get; set; }
        public string AssessmentNamePlaceholderText { get; set; }
        public string StartDateLabelText { get; set; }
        public string EndDateLabelText { get; set; }
        public string DisclaimerPlaceholderText { get; set; }
        public int AssessmentNameTextBoxLength { get; set; }
        public int DisclaimerTextAreaLength { get; set; }
        public string ReturnToAssessmentLinkText { get; set; }
        public Assessmentsteps AssessmentSteps { get; set; }
        public string PageHeader { get; set; }
    }

}
