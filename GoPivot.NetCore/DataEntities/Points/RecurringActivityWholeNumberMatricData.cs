﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.DataEntities.Points
{
    public class RecurringActivityWholeNumberMatricData
    {
        public NewActivity NewActivity { get; set; }
        public int InvalidActivitySteps { get; set; }
        public int ValidActivitySteps { get; set; }
    }
}
