﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace GoPivot.NetCore.BaseClasses
{
    public class PageBase
    {
        private IWebDriver driver;
  


        [FindsBy(How = How.LinkText, Using = "Log Out")]
        public IWebElement LogOut;

        public PageBase(IWebDriver _driver)
        {
           
            this.driver = _driver;
        }
         
        public string Title
        {
            get { return driver.Title; }
        }
    }
}
