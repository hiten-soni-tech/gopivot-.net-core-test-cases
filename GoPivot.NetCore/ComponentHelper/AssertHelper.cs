﻿using System;
using NUnit.Framework;

namespace GoPivot.NetCore.ComponentHelper
{
    public class AssertHelper
    {
        public static void AreEqual(string expected, string actual)
        {
            try
            {
                Assert.AreEqual(expected, actual);
            }
            catch (Exception)
            {
                //ignore
            }
        }
      
    }
}
