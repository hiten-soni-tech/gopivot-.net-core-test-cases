﻿using OpenQA.Selenium;

namespace GoPivot.NetCore.ComponentHelper
{
    public class ButtonHelper
    {
     
        private static IWebElement _element;

        public static void ClickButton(By locator)
        {
            _element = GenericHelper.GetElement(locator);
            _element.Click();
           
        }

        public static bool IsButtonEnabled(By locator)
        {
            _element = GenericHelper.GetElement(locator);
           
            return _element.Enabled;
        }

        public static string GetButtonText(By locator)
        {
            _element = GenericHelper.GetElement(locator);
            if (_element.GetAttribute("value") == null)
                return string.Empty;
            return _element.GetAttribute("value");
        }

     
    }
}
