﻿using OpenQA.Selenium;

namespace GoPivot.NetCore.ComponentHelper
{
    public class LinkHelper
    { 
        private static IWebElement element;

        public static void ClickLink(By Locator)
        {
            element = GenericHelper.GetElement(Locator);
            element.Click();
           
        }
    }
}
