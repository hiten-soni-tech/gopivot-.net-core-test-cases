﻿using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.ComponentHelper
{
    public class NavigationHelper
    {
 
        public static void NavigateToUrl(string Url)
        {
            ObjectRepository.Driver.Navigate().GoToUrl(Url);
         
        }
    }
}
