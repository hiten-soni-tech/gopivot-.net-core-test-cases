﻿using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.ComponentHelper
{
    public class WindowHelper
    {
        public static string GetTitle()
        {
            return ObjectRepository.Driver.Title;
        }
    }
}
