﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoPivot.NetCore.ComponentHelper
{
    public static class CommonHelper
    {
        public static string GetDynamicText(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }
            DateTime reference = DateTime.Now;
            DateTime firstDayThisMonth = new DateTime(reference.Year, reference.Month, 1);
            DateTime firstDayPlusTwoMonths = firstDayThisMonth.AddMonths(2);
            DateTime lastDayNextMonth = firstDayPlusTwoMonths.AddDays(-1);
            DateTime endOfLastDayNextMonth = firstDayPlusTwoMonths.AddTicks(-1);
            string randomNumber = new Random().Next(1000).ToString();

            str = str.Replace("{randomnumber}", randomNumber);
            str = str.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
            str = str.Replace("{currentday}", DateTime.Now.ToShortDateString());
            str = str.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
            str = str.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());
            str = str.Replace("{endofnextmonth}", endOfLastDayNextMonth.ToShortDateString());
            return str;
        }
    }
}
