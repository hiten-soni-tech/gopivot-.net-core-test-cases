﻿using OpenQA.Selenium;
using GoPivot.NetCore.Configuration;
using GoPivot.NetCore.Interfaces;

namespace GoPivot.NetCore.Settings
{
    public class ObjectRepository
    {
        public static IConfig Config { get; set; }
        public static IWebDriver Driver { get; set; }
        public static AppSetting Setting { get; set; }
        
    }
}
