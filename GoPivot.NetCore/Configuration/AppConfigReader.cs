﻿using GoPivot.NetCore.Interfaces;
using System;
using Newtonsoft.Json;
using NUnit.Framework;

namespace GoPivot.NetCore.Configuration
{
    public class AppConfigReader : IConfig
    {
        private readonly AppSetting appSetting;
        public AppConfigReader()
        {
            try
            {
                string filePath = TestContext.CurrentContext.TestDirectory + "//appSettings.json";
                string fileContent = System.IO.File.ReadAllText(filePath);
                appSetting = JsonConvert.DeserializeObject<AppSetting>(fileContent);
            }
            catch
            {
                // do nothing
            }
        }
        public AppSetting GetSettings()
        {
            return appSetting;
        }
        public BrowserType? GetBrowser()
        {
            if (appSetting != null)
            {

                return (BrowserType)Enum.Parse(typeof(BrowserType), appSetting.Browser);
            }
            return BrowserType.Chrome;
        }

        public string GetChromePath()
        {
            return appSetting.ChromePath;
        }

        public int GetElementLoadTimeOut()
        {
            if (appSetting != null)
            {
                return appSetting.ElementLoadTimeOut;
            }
            return 2;
        }

        public int GetPageLoadTimeOut()
        {
            if (appSetting != null)
            {
                return appSetting.PageLoadTimeOut;
            }
            return 200;
        }

        public string GetWebSite()
        {
            if (appSetting != null)
            {
                return appSetting.Website;
            }
            return "https://pivotqa.gbehavior.com";
        }
    }
}
