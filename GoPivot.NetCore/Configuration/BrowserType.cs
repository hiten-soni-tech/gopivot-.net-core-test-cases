﻿namespace GoPivot.NetCore.Configuration
{
    public enum BrowserType
    {
        Firefox,
        Chrome,
        IExplorer,
        PhantomJs
    }
}
