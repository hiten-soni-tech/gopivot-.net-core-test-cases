﻿using GoPivot.NetCore.DataEntities;

namespace GoPivot.NetCore.Configuration
{
    public class AppSetting
    {
        public string Browser { get; set; }
        public string Website { get; set; }
        public int PageLoadTimeOut { get; set; }
        public int ElementLoadTimeOut { get; set; }
        public string ChromePath { get; set; }
        public Credential Credentials { get; set; }
    }

    public class Credential
    {
        public Administrator Administrator { get; set; }
        public Participant Participant { get; set; }
        public Participant NewParticipant { get; set; }
    }

}
