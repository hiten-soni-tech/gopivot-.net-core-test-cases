﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.DataEntities;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1580", Category = "Nutrition")]
    public class PIV1580 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1580Data Data { get; set; }

        [TestCase(Description = "PIV 1580  - Display Daily Total values for the user.",
            TestName = "Display Daily Total values for the user")]
        [Order(1)]
        public void VerifyDailyGoals()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                var loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
                loginPage.LoginButton.Click();

                Thread.Sleep(2000);

                var logoutElement =
                    GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    Console.WriteLine("Participant {0} Logged in successfully",
                        ObjectRepository.Setting.Credentials.Participant.Username);
                }
                else
                {
                    Assert.Fail("Participant Login Failed");
                    return;
                }

                var nutritionTab = GetWebDriverWait()
                    .Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }

                Thread.Sleep(1000);

                BuildRecipePlanIfNotExists();

                Assert.IsTrue(
                    driver.FindElement(
                        By.XPath("//div[contains(@class,'nutritionHeader') and contains(.,'Calorie Goal')]")).Displayed,
                    "Calorie Goal Label should be visible");
                Console.WriteLine("Calorie Goal Label available");

                Thread.Sleep(1000);


                var calorieGoalValueElement =
                    driver.FindElement(By.XPath("//div[contains(@class,'nutritionTotalsData')][1]"));
                Assert.IsTrue(calorieGoalValueElement != null && calorieGoalValueElement.Displayed,
                    "Calorie Goal element should be present");
                int.TryParse(calorieGoalValueElement.Text, out var calorieGoal);
                Assert.IsTrue(calorieGoal > 0, "Calorie Goal should be greater than 0");
                Console.WriteLine("Calorie goal value is {0}", calorieGoal);

                Thread.Sleep(3000);

                var recipeGoalValueElement = GetWebDriverWait()
                    .Until(ExpectedConditions.ElementIsVisible(
                        By.XPath("//div[contains(@class,'nutritionTotalsData')][2]")));
                Assert.IsTrue(recipeGoalValueElement != null && recipeGoalValueElement.Displayed,
                    "Recipe Goal element should be present");
                int.TryParse(recipeGoalValueElement.Text, out var recipeGoal);
                Console.WriteLine("Recipe goal value is {0}", recipeGoal);
                Assert.IsTrue(recipeGoal > 0, "Recipe Goal should be greater than 0");

                Assert.IsTrue(calorieGoal > recipeGoal, "Calories goes should be greater than recipe goal");

                var recipePercentage = Convert.ToSingle(recipeGoal) / calorieGoal * 100;
                Assert.IsTrue(recipePercentage > 0 && recipePercentage <= 75,
                    "Recipe goal should not be more than 75% of calories goal");
                Console.WriteLine("Recipes goal percentage is less than 75%. Current Percentages are {0}",
                    recipePercentage);

                IList<IWebElement> recipeCaloriesElements = driver.FindElements(
                    By.XPath("//div[contains(@class,'recipeDetails')]/div[contains(@class,'recipeDetailsHeader')][2]"));
                Assert.IsTrue(recipeCaloriesElements.Count > 0, "Recipes count should be greater than 0");

                var recipeCalorieTotalValue = 0;
                foreach (var recipeCalorieElement in recipeCaloriesElements)
                {
                    int.TryParse(recipeCalorieElement.Text, out var recipeCalorieValue);
                    recipeCalorieTotalValue = recipeCalorieTotalValue + recipeCalorieValue;
                }

                Assert.IsTrue(recipeCalorieTotalValue == recipeGoal,
                    "Recipe Goal should be matching with total recipe calories for all meals");
                Console.WriteLine("Recipe Calories Total value matching with recipe goal");

                var differenceValueElement = GetWebDriverWait()
                    .Until(ExpectedConditions.ElementIsVisible(
                        By.XPath("//div[contains(@class,'nutritionTotalsData')][3]")));
                Assert.IsTrue(differenceValueElement != null && differenceValueElement.Displayed,
                    "Difference Goal element should be present");
                int.TryParse(differenceValueElement.Text, out var differenceGoal);
                Console.WriteLine("Difference  goal value is {0}", differenceGoal);
                Assert.IsTrue(differenceGoal < 0, "Difference Goal should be less than 0");

                var calorieRecipeDifference = recipeGoal - calorieGoal;
                Assert.True(calorieRecipeDifference == differenceGoal,
                    "Calorie different against recipe should be equal to Difference Goal Label");


                var carbValueElement = GetWebDriverWait()
                    .Until(ExpectedConditions.ElementIsVisible(
                        By.XPath("//div[contains(@class,'nutritionTotalsData')][4]")));
                var proteinValueElement = GetWebDriverWait()
                    .Until(ExpectedConditions.ElementIsVisible(
                        By.XPath("//div[contains(@class,'nutritionTotalsData')][5]")));
                var fatValueElement = GetWebDriverWait()
                    .Until(ExpectedConditions.ElementIsVisible(
                        By.XPath("//div[contains(@class,'nutritionTotalsData')][6]")));
                Assert.IsTrue(carbValueElement != null && carbValueElement.Displayed,
                    "Carb Value element should be present");
                Assert.IsTrue(proteinValueElement != null && proteinValueElement.Displayed,
                    "Protein Value element should be present");
                Assert.IsTrue(fatValueElement != null && fatValueElement.Displayed,
                    "Fat Value element should be present");

                int.TryParse(carbValueElement.Text.Replace("g", "").Trim(), out var carbTotalValue);
                int.TryParse(proteinValueElement.Text.Replace("g", "").Trim(), out var proteinTotalValue);
                int.TryParse(fatValueElement.Text.Replace("g", "").Trim(), out var fatTotalValue);

                Console.WriteLine("Carb Total value is {0}", carbTotalValue);
                Console.WriteLine("Protein Total value is {0}", proteinTotalValue);
                Console.WriteLine("Fat Total value is {0}", fatTotalValue);

                var carbMealsElements = driver.FindElements(
                    By.XPath("//div[contains(@class,'recipeInfoFooter')]/div[contains(@class,'ng-binding')][1]"));
                Assert.IsTrue(carbMealsElements.Count > 0, "Carbs under meals count should be greater than 0");
                var carbTotal = 0;
                foreach (var carbMealElement in carbMealsElements)
                {
                    int.TryParse(carbMealElement.Text.Replace("g", ""), out var carbMealValue);
                    carbTotal = carbTotal + carbMealValue;
                }

                Assert.IsTrue(carbTotal == carbTotalValue, "Carb Total Label should be matching with meal value");
                Console.WriteLine("Carb Total Value matching with all meals carb value");

                IList<IWebElement> proteinMealsElements = driver.FindElements(
                    By.XPath("//div[contains(@class,'recipeInfoFooter')]/div[contains(@class,'ng-binding')][2]"));
                Assert.IsTrue(proteinMealsElements.Count > 0, "Protein under meals count should be greater than 0");
                var proteinTotal = 0;
                foreach (var proteinMealElement in proteinMealsElements)
                {
                    int.TryParse(proteinMealElement.Text.Replace("g", ""), out var proteinMealValue);
                    proteinTotal = proteinTotal + proteinMealValue;
                }

                Assert.IsTrue(proteinTotal == proteinTotalValue,
                    "Protein Total Label should be matching with meal value");
                Console.WriteLine("Protein Total Value matching with all meals protein value");

                IList<IWebElement> fatMealsElements = driver.FindElements(
                    By.XPath("//div[contains(@class,'recipeInfoFooter')]/div[contains(@class,'ng-binding')][3]"));
                Assert.IsTrue(fatMealsElements.Count > 0, "Fat under meals count should be greater than 0");
                var fatTotal = 0;
                foreach (var fatMealElement in fatMealsElements)
                {
                    int.TryParse(fatMealElement.Text.Replace("g", ""), out var fatMealValue);
                    fatTotal = fatTotal + fatMealValue;
                }

                Assert.IsTrue(fatTotal == fatTotalValue, "Fat Total Label should be matching with meal value");
                Console.WriteLine("Fat Total Value matching with all meals fat value");
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("PIV-1580 Scenario 1 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1580 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV 1580  - Display informational text", TestName = "Display informational text ")]
        [Order(2)]
        public void VerifyDailyGoalsText()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                var headingTextElement =
                    driver.FindElement(By.XPath("/html/body/div/div/div[2]/div/div/section/div/p"));
                Console.WriteLine(headingTextElement.Text);
                Assert.IsTrue(headingTextElement.Text == Data.NutritionPageText.HeadingText,
                    "Nutrition Heading Text should be matching with given data");
                Console.WriteLine("Nutrition Heading Text matching with given data");


                var dailyGoalTextElement =
                    driver.FindElement(By.XPath("/html/body/div/div/div[2]/div/div/section/div/div[2]/p"));
                Console.WriteLine(dailyGoalTextElement.Text);
                Assert.IsTrue(dailyGoalTextElement.Text == Data.NutritionPageText.DailyTotalText,
                    "Nutrition Daily Goal Text should be matching with given data");
                Console.WriteLine("Nutrition Daily Goal Text matching with given data");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.Error.WriteLine("PIV-1580 Scenario 2 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1580 Scenario 2 Failed. Error: {0}", ex);
            }
        }

        private void BuildRecipePlanIfNotExists()
        {
            try
            {
                driver.FindElement(By.LinkText("Build Your Recipe Plan")).Click();
                Console.WriteLine("Build Your Recipe Plan clicked");

                driver.FindElement(By.XPath("//input[@ng-model='model.desiredWeight']")).SendKeys("100");
                driver.FindElement(By.XPath("//input[@type='button' and @value = 'Save']")).Click();
                Console.WriteLine("Save Button clicked");

                Thread.Sleep(3000);

                var nutritionTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }
                Thread.Sleep(3000);
            }
            catch
            {
                Console.WriteLine("Button: Build Your Recipe Plan not found");
                Console.WriteLine("Recipe Plan already available for this user");
                // Do nothing
            }
        }

        private PIV1580Data GetData()
        {
            PIV1580Data data = null;
            try
            {
                var fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1580.json");
                data = JsonConvert.DeserializeObject<PIV1580Data>(fileData);
            }
            catch
            {
                // Do nothing
            }

            return data;
        }

        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}