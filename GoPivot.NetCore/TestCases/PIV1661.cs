﻿using System;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1661", Category = "Challenge")]
    public class PIV1661 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        [TestCase(
            Description =
                "PIV-1661 As a Pivot admin, I want to delete a challenge so participants can no longer access the challenge (Delete it!)",
            TestName = "Delete Challenge")]
        [Order(1)]
        public void DeleteChallenge()
        {
            try
            {
                var loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
                Thread.Sleep(1000);
                loginPage.LoginButton.Click();

                Thread.Sleep(1000);

                Console.WriteLine("Administrator Logged in successfully");

                var adminPage = new AdminPage(driver);
                if (adminPage.Clients != null && adminPage.Clients.Displayed)
                {
                    adminPage.Clients.Click();
                    Console.WriteLine("Clients link clicked");
                }
                else
                {
                    Console.WriteLine("Clients Link not found");
                    Assert.Fail("Clients link not found");
                }

                Thread.Sleep(2000);
                var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
                if (clientElement != null && clientElement.Displayed)
                {
                    Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                    clientElement.Click();
                }
                else
                {
                    Console.WriteLine("Client {0} not found",
                        ObjectRepository.Setting.Credentials.Administrator.Client);
                    Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                }

                if (!adminPage.EditClient.Displayed)
                {
                    Console.WriteLine("Clients Details not found");
                    Assert.Fail("Clients Details not found");
                }
                else
                {
                    Console.WriteLine("Client Details page loaded");
                }

                var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
                if (programElement != null && programElement.Displayed)
                {
                    programElement.Click();
                    Console.WriteLine("Program {0} selected",
                        ObjectRepository.Setting.Credentials.Administrator.Program);
                }
                else
                {
                    Console.WriteLine("Program {0} not found",
                        ObjectRepository.Setting.Credentials.Administrator.Program);
                }

                if (adminPage.ProgramTabs.Count > 0)
                {
                    Console.WriteLine("Program Listing page loaded");
                }
                else
                {
                    Console.WriteLine("Program details page not loaded");
                    Assert.Fail("Failed to load Program details");
                }

                Thread.Sleep(1000);

                var challengeTab = adminPage.GetProgramTab("Challenges");
                if (challengeTab == null)
                {
                    Console.WriteLine("Unable to find challenges tab");
                    Assert.Fail("Unable to find challenges tab");
                }
                else
                {
                    Console.WriteLine("Challenge Tab loaded");
                    challengeTab.Click();
                }

                Thread.Sleep(2000);
                var firstChallenge = driver.FindElement(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]"));
                var firstChallengeAction = new Actions(driver);
                firstChallengeAction.MoveToElement(firstChallenge).Build().Perform();

                Assert.IsTrue(firstChallenge != null && firstChallenge.Displayed,
                    "Challenges must be available to perform delete operation");

                var firstChallengeElementName = firstChallenge.FindElement(By.XPath("//td[3]"));
                var challengeName = firstChallengeElementName.Text;
                Console.WriteLine("{0} Challenge selected", challengeName);

                Thread.Sleep(3000);
                var deleteButton =
                    driver.FindElement(
                        By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]/td/i[@class='icon-trash']"));
                var builder = new Actions(driver);
                builder.MoveToElement(deleteButton).Build().Perform();
                deleteButton.Click();

                Thread.Sleep(1000);

                Assert.IsTrue(driver.FindElement(By.ClassName("deleteConfirmationModal")).Displayed,
                    "Delete Confirmation Model is visible");

                var modelHeading = string.Format("Delete '{0}'?", challengeName);
                Assert.IsTrue(driver.FindElement(By.XPath("//div[@class='modal-header']/h2/b")).Text
                    .Equals(modelHeading));

                Console.WriteLine("Heading matching in popup {0}", modelHeading);

                Assert.IsTrue(
                    driver.FindElement(By.XPath(
                            "//div[@class='modal-footer']/div[@class='pull-right']/button[contains(.,'Cancel')]"))
                        .Displayed, "Cancel button should be visible to select");
                Assert.IsTrue(
                    driver.FindElement(By.XPath(
                            "//div[@class='modal-footer']/div[@class='pull-right']/button[contains(.,'Delete')]"))
                        .Displayed, "Delete button should be visible to select");

                Console.WriteLine("Delete / Cancel button available in modal popup");

                driver.FindElement(
                        By.XPath("//div[@class='modal-footer']/div[@class='pull-right']/button[contains(.,'Delete')]"))
                    .Click();
                Console.WriteLine("Delete Button clicked");

                Thread.Sleep(2000);

                var challengeDeleted = true;
                var totalChallengeCount =
                    driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr")).Count;
                if (totalChallengeCount > 0)
                    challengeDeleted = !challengeName.Equals(driver
                        .FindElement(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]/td[3]")).Text);

                if (!challengeDeleted)
                {
                    Console.WriteLine("Failed to delete challenge");
                    Assert.Fail("Failed to delete challenge");
                }
                else
                {
                    Console.WriteLine("Challenge {0} deleted successfully", challengeName);
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1661 (Delete Scenario) - Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1661 (Delete Scenario) -  Delete Challenge Failed. Error: {0}", ex);
            }
        }

        [TestCase(
            Description =
                "PIV-1661 As a Pivot admin, I want to delete a challenge so participants can no longer access the challenge (Cancel it!)",
            TestName = "Cancel Delete Challenge")]
        [Order(2)]
        public void CancelDeleteChallenge()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                var firstChallenge = driver.FindElement(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]"));
                var firstChallengeAction = new Actions(driver);
                firstChallengeAction.MoveToElement(firstChallenge).Build().Perform();

                Assert.IsTrue(firstChallenge != null && firstChallenge.Displayed,
                    "Challenges must be available to perform delete/cancel operation");

                var firstChallengeElementName = firstChallenge.FindElement(By.XPath("//td[3]"));
                var challengeName = firstChallengeElementName.Text;
                Console.WriteLine("{0} Challenge selected", challengeName);

                Thread.Sleep(3000);
                var deleteButton =
                    driver.FindElement(
                        By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]/td/i[@class='icon-trash']"));
                var builder = new Actions(driver);
                builder.MoveToElement(deleteButton).Build().Perform();
                deleteButton.Click();

                Thread.Sleep(1000);

                Assert.IsTrue(driver.FindElement(By.ClassName("deleteConfirmationModal")).Displayed,
                    "Delete Confirmation Model is visible");
                Assert.IsTrue(
                    driver.FindElement(By.XPath(
                            "//div[@class='modal-footer']/div[@class='pull-right']/button[contains(.,'Cancel')]"))
                        .Displayed, "Cancel button should be visible to select");
                driver.FindElement(
                        By.XPath("//div[@class='modal-footer']/div[@class='pull-right']/button[contains(.,'Cancel')]"))
                    .Click();
                Console.WriteLine("Cancel Button clicked");

                Thread.Sleep(2000);

                var challengeDeleted = false;
                var totalChallengeCount =
                    driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr")).Count;
                if (totalChallengeCount > 0)
                    challengeDeleted = !challengeName.Equals(driver
                        .FindElement(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]/td[3]")).Text);

                if (challengeDeleted)
                {
                    Console.WriteLine("Challenge deleted on cancel button click");
                    Assert.Fail("Challenge deleted on cancel button click");
                }
                else
                {
                    Console.WriteLine("Challenge {0} not deleted. cancel event performed successfully", challengeName);
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1661 (Cancel Scenario) - Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1661 (Cancel Scenario) -  Delete Challenge Failed. Error: {0}", ex);
            }
        }
        
    }
}