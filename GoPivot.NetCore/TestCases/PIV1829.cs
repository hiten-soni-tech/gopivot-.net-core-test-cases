﻿using System;
using System.IO;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.DataEntities;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Linq;
using System.Collections.Generic;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1829", Category = "Exercise")]
    public class PIV1829 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1829Data Data { get; set; }
        [Order(1)]
        [TestCase(TestName = "Scenario 1: Update days requirements text in Unleashed Goal option")]
        public void Validate_Update_Requirement_Text_In_Unleashed_Goal_Option()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                LoginParticipant();
                Thread.Sleep(5000);
                var exerciseTab = GetWebDriverWait()
               .Until(ExpectedConditions.ElementToBeClickable(By.LinkText("Exercise")));
                if (exerciseTab != null && exerciseTab.Displayed)
                {
                    exerciseTab.Click();
                    Console.WriteLine("Exercise Tab Clicked");
                }
                Thread.Sleep(6000);

                try { 
                driver.FindElement(By.LinkText("Change Exercise Plan")).Click();
                Console.WriteLine("Change Exercise Plan clicked");
                }
                catch
                {
                    // No Exercise Plan available. Create new one
                    AddNewExercisePlan();
                    Thread.Sleep(4000);
                }
                Thread.Sleep(4000);

                var exerciseGoalList = Data.ExerciseGoals.ToList().OrderBy(p => p.DisplayOrder);

                for (int i = 1; i <= exerciseGoalList.Count(); i++)
                {
                    var goalNameElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'fitnessGoalHeight')][{0}]/div[contains(@class,'fitnessDescription')]", i)));
                    Assert.IsTrue(goalNameElement.Displayed, "{0} Goal element available", goalNameElement.Text);
                    Assert.IsTrue(goalNameElement.Text.Equals(Data.ExerciseGoals[i - 1].Name), "Goal Name {0} must be matching", goalNameElement.Text);
                    Console.WriteLine("Goal Name matching {0}", goalNameElement.Text);
                    var goalDaysElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'fitnessGoalHeight')][{0}]/div[contains(@class,'fitnessGoalDays')]", i)));
                    Assert.IsTrue(goalDaysElement.Text.Equals(Data.ExerciseGoals[i - 1].TimeText));
                    Console.WriteLine("Goal Time matching {0}", goalDaysElement.Text);

                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1829 Error: {0}", ex.Message);
                Assert.Fail("PIV-1829 Failed. Error: {0}", ex);
            }

        }

        [Order(2)]
        [TestCase(TestName = "Scenario 2: Scenario 2: Require 4 days when Unleashed Goal option is selected")]
        public void Validate_Requirement_Days_When_Unleashed_Goal_Option_Selected()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            if (Data == null)
            {
                Console.WriteLine("No Valid Input Data found");
                Assert.Fail("No Valid Input Data found");
            }
            var selectedDaysElement = driver.FindElements(By.XPath("//div[contains(@class,'fitnessSelectedDay')]/parent::*"));
            foreach (var element in selectedDaysElement)
            {

                element.Click();
                Thread.Sleep(3000);
                Console.WriteLine("{0} exercise day unselected", element.Text);
            }

            var validateExerciseGoal = Data.ExerciseGoals.FirstOrDefault(p => p.Validate);
            var goalNameElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'fitnessGoalHeight')]/div[contains(@class,'fitnessDescription') and contains(.,'{0}')]", validateExerciseGoal.Name)));
            Console.WriteLine("Exercise Goal selected for validation '{0}'", goalNameElement.Text);
            goalNameElement.Click();
            Thread.Sleep(3000);

            Assert.IsTrue(driver.FindElement(By.ClassName("alert-danger")).Displayed, "Alert message must be displayed");
            Console.WriteLine("Alert message displayed");
            Assert.IsTrue(driver.FindElement(By.ClassName("alert-danger")).Text.Equals(validateExerciseGoal.GoalError), "Goal Error message must be matching");
            Console.WriteLine("Goal Error message matching");

            for (int i = 1; i <= validateExerciseGoal.Time; i++)
            {
                var dayElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'btn-exercise')][{0}]", i)));
                dayElement.Click();
                Thread.Sleep(3000);

                if (i < validateExerciseGoal.Time)
                {
                    Assert.IsTrue(driver.FindElement(By.ClassName("alert-danger")).Displayed, "Alert message must be displayed");
                    Console.WriteLine("Alert message displayed on selection of day {0}", dayElement.Text);
                    Assert.IsTrue(driver.FindElement(By.ClassName("alert-danger")).Text.Equals(validateExerciseGoal.GoalError), "Goal Error message must be matching");
                    Console.WriteLine("Goal Error message matching on selection of day {0}", dayElement.Text);

                }
                else
                {
                    try
                    {
                        Assert.False(driver.FindElement(By.ClassName("alert-danger")).Displayed, "Alert message must NOT be displayed");
                        Console.WriteLine("Error: Alert Message still showing");
                    }
                    catch
                    {
                        Console.WriteLine("Alert message not showing");
                    }
                }
            }

        }

        #region Utilities
        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.NewParticipant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.NewParticipant.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();

            Thread.Sleep(2000);

            var logoutElement =
                GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement != null && logoutElement.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully",
                    ObjectRepository.Setting.Credentials.NewParticipant.Username);
            }
            else
            {
                Assert.Fail("Participant Login Failed");
                return;
            }
        }

        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private PIV1829Data GetData()
        {
            PIV1829Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1829.json");
                data = JsonConvert.DeserializeObject<PIV1829Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }


        private void AddNewExercisePlan()
        {
            //var heightFeetElement = driver.FindElement(By.Id("heightFeet"));
            //heightFeetElement.Clear();
            //heightFeetElement.SendKeys(Data.BuildExercisePlan.HeightFeet.ToString());

            //var heightInchesElement = driver.FindElement(By.Id("heightInches"));
            //heightInchesElement.Clear();
            //heightInchesElement.SendKeys(Data.BuildExercisePlan.HeightInch.ToString());
            //Console.WriteLine("Height {0}'{1}", Data.BuildExercisePlan.HeightFeet, Data.BuildExercisePlan.HeightInch);
            //var weightElement = driver.FindElement(By.XPath("//input[@ng-model='fitnessPreferences.weight']"));
            //weightElement.Clear();
            //weightElement.SendKeys(Data.BuildExercisePlan.Weight.ToString());
            //Console.WriteLine("Weight: {0}", Data.BuildExercisePlan.Weight);

            var ageElement = driver.FindElement(By.Id("fitnessAge"));
            ageElement.Clear();
            ageElement.SendKeys("40");
              var exerciseGoalElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'fitnessGoalHeight')]/div[contains(@class,'fitnessDescription') and contains(.,'I just want to get started with exercise.')]")));
            exerciseGoalElement.Click();
            Console.WriteLine("Exercise Goal: I just want to get started with exercise.");

            string[] daysForExercise = "Mon,Tue,Wed,Thu,Fri".Split(new char[] { ',' });
            foreach (var day in daysForExercise)
            {
                string elementId = string.Format("{0}Selection", day.ToLower());
                var dayElement = driver.FindElement(By.Id(elementId));
                try
                {
                    var selectedIcon = dayElement.FindElement(By.ClassName("selectedIcon"));
                }
                catch
                {
                    dayElement.Click();
                    Console.WriteLine("{0} Day Clicked for exercise", day);
                    Thread.Sleep(3000);
                }

            }

            var physicalActiveElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'fitnessActiveHeight')]/div[contains(@class,'fitnessDescription') and contains(.,'I exercise once a week or less.')]")));
            physicalActiveElement.Click();
            Console.WriteLine("Physical Activeness: I exercise once a week or less.");

            var accessToEquipmentElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'fitnessEquipmentHeight')]/div[contains(@class,'fitnessDescription') and contains(.,'A treadmill or someplace to run')]")));
            accessToEquipmentElement.Click();
            Console.WriteLine("Access to Equipments: A treadmill or someplace to run");


            Thread.Sleep(3000);
            driver.FindElement(By.XPath("//input[@type='button' and @value='Save']")).Click();
            Console.WriteLine("Save button clicked");
            Thread.Sleep(5000);

            var exerciseTab = GetWebDriverWait()
   .Until(ExpectedConditions.ElementIsVisible(By.LinkText("Exercise")));
            if (exerciseTab != null && exerciseTab.Displayed)
            {
                exerciseTab.Click();
                Console.WriteLine("Exercise Tab Clicked");
            }
            Thread.Sleep(3000);
            try
            {
                Assert.IsFalse(driver.FindElement(By.LinkText("Build Your Workout Plan")).Displayed, "Build Your Workout Plan should not be displayed");
            }
            catch
            {
                Console.WriteLine("Build Your Workout plan completed successfully");
            }
        }

        #endregion

    }
}
