﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;


namespace GoPivot.NetCore.TestCases
{

    [TestFixture(TestName = "PIV-1112", Category = "FitScore")]
    public class PIV1112 : BaseStepDefinition
    {
        public PIV1112Data Data { get; set; }
        public bool IsSuccess { get; set; }
        [TestCase(Description = "PIV-1112 Display FitScore information modal/page", TestName = "Scenario 1: Display FitScore information modal/page")]
        [Order(1)]
        public void Validate_FitScore_Model()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginParticipant();
                Thread.Sleep(8000);
                IWebElement exerciseTab = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.LinkText("Exercise")));
                exerciseTab.Click();
                Console.WriteLine("Exercise Link clicked");
                Thread.Sleep(6000);

                driver.FindElement(By.LinkText("Learn More")).Click();
                Thread.Sleep(2000);

                Assert.IsTrue(driver.FindElement(By.ClassName("modal-dialog")).Displayed, "FitScore modal popup must be displayed");
                Console.WriteLine("FitScore Modal popup displayed");

                var modalHeader = driver.FindElement(By.XPath("//div[@class='modal-content']/div[contains(@class,'modal-header')]/h2"));
                Assert.IsTrue(modalHeader.Text == Data.Title, "Modal Title matching");

                var para1Element = driver.FindElement(By.XPath("//div[contains(@class,'modal-body')]/p[1]"));
                Assert.IsTrue(para1Element.Text == Data.ModalSection1, "Modal Section 1 should be matching");


                var para2Element = driver.FindElement(By.XPath("//div[contains(@class,'modal-body')]/p[2]"));
                Assert.IsTrue(para2Element.Text == Data.ModalSection2, "Modal Section 2 should be matching");

                var para3Element = driver.FindElement(By.XPath("//div[contains(@class,'modal-body')]/p[3]"));
                Assert.IsTrue(para3Element.Text == Data.ModalSection3, "Modal Section 3 should be matching");
                Console.WriteLine("All Modal Sections text validated");
                Assert.IsTrue(driver.FindElement(By.LinkText("Take A FitIn")).Displayed, "Take a FitIn button should be displayed");
                Console.WriteLine("Take a FitIn button displayed");
                Console.WriteLine("FitScore Modal Validated successfully");
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1112 Scenarion 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1112 Scenarion 1 Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-1112 User selects Take a FitIn button", TestName = "Scenario 2: User selects Take a FitIn button")]
        [Order(2)]
        public void Validate_FitIn_Redirect_Button()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 1 not completed"); return;
                }
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                Thread.Sleep(3000);
                GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.LinkText("Take A FitIn"))).Click();
                Console.WriteLine("Take a FitIn button clicked");
                Thread.Sleep(2000);
                Assert.IsTrue(driver.Url.Contains("/#!/fitness/fitin"), "Url Changed");
                Thread.Sleep(1000);
                Assert.IsTrue(driver.FindElement(By.XPath("//h1[@class='hidden-xs']")).Displayed, "FitIn UI Loaded");
                try
                {
                    Assert.IsFalse(driver.FindElement(By.ClassName("modal-dialog")).Displayed, "FitScore modal popup must be closed");
                    Console.WriteLine("Invalid: Popup must be closed");
                }
                catch
                {
                    Console.WriteLine("Popup closed successfully");
                }
                Console.WriteLine("FitIn Redirection working properly");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1112 Scenarion 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1112 Scenarion 2 Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-1112 User navigates back", TestName = "Scenario 3: User navigates back")]
        [Order(3)]
        public void Validate_FitIn_Navigation()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 2 not completed"); return;
                }
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                try
                {
                    driver.FindElement(By.XPath("//button[contains(.,'Cancel')]")).Click();

                }
                catch
                {
                    // Do nothing
                }
                Thread.Sleep(2000);
                IWebElement exerciseTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Exercise")));
                exerciseTab.Click();
                Console.WriteLine("Exercise Link clicked");
                Thread.Sleep(6000);

                driver.FindElement(By.LinkText("Learn More")).Click();
                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//button[contains(.,'Cancel')]")).Click();
                Thread.Sleep(2000);
                try
                {
                    Assert.IsFalse(driver.FindElement(By.ClassName("modal-dialog")).Displayed, "FitScore modal popup must be closed");
                }
                catch
                {
                    // do nothing
                }
                Assert.IsTrue(driver.FindElement(By.XPath("//h1[@class='hidden-xs']")).Text == "Exercise", "Exercise section Loaded");
                Console.WriteLine("User navigated successfully");
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1112 Scenarion 3 Error: {0}", ex.Message);
                Assert.Fail("PIV-1112 Scenarion 3 Failed. Error: {0}", ex);
            }
        }


        #region Utilities

        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }


        private void LogoutParticipant()
        {
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private PIV1112Data GetData()
        {
            PIV1112Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1112.json");
                data = JsonConvert.DeserializeObject<PIV1112Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
