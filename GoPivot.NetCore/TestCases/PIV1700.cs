﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1700", Category = "Exercise")]
    public class PIV1700 : BaseStepDefinition
    {
        [TestCase(TestName = "Display a growl text on exercise preference changes")]
        public void Validate_Growl_Notification_On_Plan_Change()
        {
            try
            {
                LoginParticipant();
                Thread.Sleep(8000);
                IWebElement exerciseTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Exercise")));
                exerciseTab.Click();
                Console.WriteLine("Exercise Link clicked");
                Thread.Sleep(5000);
                driver.FindElement(By.LinkText("Change Exercise Plan")).Click();
                Console.WriteLine("Change Exercise Plan clicked");
                Thread.Sleep(5000);
                driver.FindElement(By.XPath("//div[contains(@class,'fitnessGoalHeight')][1]")).Click();
                Thread.Sleep(5000);
                Console.WriteLine("First Exercise Goal been set");

                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'ui-notification')]")).Displayed, "Growl Text notification been shown");
                Console.WriteLine("Growl Text notification been shown");

                var messageElement = driver.FindElement(By.XPath("//div[contains(@class,'ui-notification')]//div[contains(@class,'message')]"));
                Assert.IsTrue(messageElement.Displayed, "Message Displayed under Growl Notification section");
                Assert.IsTrue(messageElement.Text == "Your exercise preferences have been saved!", "Message text must be matching with 'Your exercise preferences have been saved!'");
                Console.WriteLine("Message text is matching");
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1700 Error: {0}", ex.Message);
                Assert.Fail("PIV-1700 Failed. Error: {0}", ex);
            }

        }

        #region Utilities

        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }


        private void LogoutParticipant()
        {
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
