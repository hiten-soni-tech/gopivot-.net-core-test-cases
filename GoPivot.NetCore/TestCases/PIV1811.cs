﻿
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1811", Category = "Account",Ignore ="True", IgnoreReason ="Disabled temporarily")]
    public class PIV1811 : BaseStepDefinition
    {
        public PIV1811Data Data { get; set; }
       public bool IsSuccess { get; set; }
        [TestCase(Description = "PIV-1811 Scenario 1  Display Landing Page dropdown in Account Settings", TestName = "Scenario 1: Display Landing Page")]
        [Order(1)]
        public void DisplayLandingPageDropdown()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                LoginAdministrator();
                var landingPageSelectElement = driver.FindElement(By.TagName("select"));
                Assert.IsTrue(landingPageSelectElement.Displayed, "Landing Page dropdown should be visible");
                Console.WriteLine("Landing Page dropdown is visible");
                SelectElement selectElement = new SelectElement(landingPageSelectElement);
                Assert.IsTrue(selectElement.SelectedOption.Text == Data.DefaultSelectOption, "By default activities option should be selected");
                Console.WriteLine("Activities option selected by default");
                IsSuccess = true;

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1811 (Scenario 1) - Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1811 (Scenario 1) Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-1811 Scenario 2  Only display menu options that are enabled in the section below", TestName = "Scenario 2: Display Enabled Menu Options")]
        [Order(2)]
        public void DisplayEnabledMenuOptions()
        {
            if (!IsSuccess)
            {
                Assert.Warn("First Scenario not completed"); return;
            }
            try
            {
                string exerciseElementPath = "//div[contains(@class,'settingLabel') and contains(.,'Exercise')]/preceding-sibling::md-switch[contains(@class,'md-checked')]";
                try
                {
                    driver.FindElement(By.XPath(exerciseElementPath)).Click();
                    Console.WriteLine("Disabled Exercise Option from account setting");
                }
                catch
                {
                    Console.WriteLine("Excercise option unchecked");
                    // Element already disabled no actions needed.
                }
                Thread.Sleep(2000);

                var landingPageSelectElement = driver.FindElement(By.TagName("select"));
                SelectElement selectElement = new SelectElement(landingPageSelectElement);
                var exerciseOptionElement = selectElement.Options.FirstOrDefault(p => p.Text == "Exercise");
                Assert.IsTrue(exerciseOptionElement != null && exerciseOptionElement.Displayed, "Exercise Option available in landing page dropdown");
                Console.WriteLine("Exercise Option available in landing page dropdown");
                Assert.IsTrue(!exerciseOptionElement.Enabled, "Exercise option in landing page dropdown should not be enabled");
                Console.WriteLine("Exercise option is disabled in landing page dropdown");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1811 (Scenario 2) - Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1811 (Scenario 2) Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-1811 Scenario 3 Enable landing page selection for participant", TestName = "Scenario 3: Enable Landing Page selection")]
        [Order(3)]
        public void ValidateLandingPage()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            try
            {
                var landingPageSelectElement = driver.FindElement(By.TagName("select"));
                SelectElement selectElement = new SelectElement(landingPageSelectElement);
                string selectElementText = selectElement.SelectedOption.Text;


                var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
                if (dropdownMenuElement.Displayed)
                {
                    dropdownMenuElement.Click();

                    var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                    if (logoutElement != null && logoutElement.Displayed)
                    {
                        logoutElement.Click();
                        Console.WriteLine("Administrator logged off successfully");
                    }
                    else
                    {
                        Console.Error.WriteLine("Logout link not clickable");
                        Assert.Fail("Logout link not clickable");
                    }

                }
                else
                {
                    Console.Error.WriteLine("Logout Dropdown not visible");
                    Assert.Fail("Logout Dropdown not visible");
                }


                By loadingImage = By.TagName("http-busy");
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                var loginPage = new LoginPage(driver);
                Console.WriteLine("Login Page loaded successfully");
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
                Thread.Sleep(2000);
                loginPage.LoginButton.Click();
                Thread.Sleep(1000);
                var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement1 != null && logoutElement1.Displayed)
                {
                    Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);

                }
                else
                {
                    Console.Error.WriteLine("Login Failed. Failed to load participant view");
                    Assert.Fail("Login Failed. Failed to load participant view");
                }
                Thread.Sleep(2000);

                var activeLinks = driver.FindElements(By.XPath("//ul[contains(@class,'side-nav')]/li/a[contains(@class,'activeAnchor')]"));
                bool linkFound = false;
                foreach (var activeLink in activeLinks)
                {
                    if (string.IsNullOrEmpty(activeLink.Text)) continue;
                    Console.WriteLine(activeLink.Text);
                    Assert.IsTrue(activeLink.Text.Equals(selectElementText));
                    Console.WriteLine("Current Landing Page matches with selection of landing page in admin section");
                    linkFound = true;
                    break;
                }

                if (linkFound)
                {
                    LogoutParticipant();
                }
                else
                {
                    Console.WriteLine("Failed to find landing page link {0}", selectElement);
                    Assert.Fail("Failed to find landing page link {0}", selectElementText);
                }

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1811 (Scenario 3) - Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1811 (Scenario 3) Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-1811 Scenario 4 Enable Exercise Section again", TestName = "Scenario 4: Enable Exercise Section again")]
        [Order(4)]
        public void EnableExerciseSection()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 3 not completed"); return;
            }
            Thread.Sleep(5000);
            LoginAdministrator();
            string exerciseElementPath = "//div[contains(@class,'settingLabel') and contains(.,'Exercise')]/preceding-sibling::md-switch";
            try
            {
                driver.FindElement(By.XPath(exerciseElementPath)).Click();
                Console.WriteLine("Enabled Exercise Option from account setting");
            }
            catch
            {
                Console.WriteLine("Excercise option already unchecked");
                // Element already disabled no actions needed.
            }
 
        }

        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }


        public PIV1811Data GetData()
        {
            PIV1811Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1811.json");
                data = JsonConvert.DeserializeObject<PIV1811Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }


        private void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");
            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }

            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            if (!adminPage.EditClient.Displayed)
            {
                Console.WriteLine("Clients Details not found");
                Assert.Fail("Clients Details not found");

            }
            else
            {
                Console.WriteLine("Client Details page loaded");
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(2000);

        }

        private void LogoutParticipant()
        {
            Thread.Sleep(7000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
            Thread.Sleep(3000);
        }

    }
}
