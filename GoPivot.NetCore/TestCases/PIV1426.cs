﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1426", Category = "Activity")]
    public class PIV1426 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1426Data Data { get; set; }
        [TestCase(Description = "PIV-1426 Administrator - Create checkbox option to hide end Date in Activity configuration wizardAdmin - Create checkbox option to hide end Date in Activity configuration wizard", TestName = "Checkbox option to hide end date")]
        [Order(1)]
        public void Create_CheckBox_Option_To_Hide_End_Date()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAndSelectActivity();

                driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
                Console.WriteLine("Add Activity button clicked");

                Thread.Sleep(5000);
                Data.NewActivity = new AdminPage(driver).CreateNewActivity(Data.NewActivity, errorMessage: out string errorMessage);
                if (Data.NewActivity == null)
                {
                    Assert.Fail(errorMessage);
                }
                Console.WriteLine("Activity created successfully. Name: {0}", Data.NewActivity.Name);

                LogoutAdministrator();
                Thread.Sleep(1000);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                
                Console.WriteLine("PIV-1426 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1426 Scenario 1 Failed. Error: {0}", ex);
            }
        }


        [TestCase(Description = "PIV-1426 Participant – Hide activity end date on the Activity detail page", TestName = "Hide Activity Date in Activity Details Page")]
        [Order(2)]
        public void Validate_Hide_Activity_In_Activity_Detail_Page()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("First Scenario not completed"); return;
                }
                string activityName = string.Empty;
                if (Data?.NewActivity != null)
                {
                    activityName = Data.NewActivity.Name;
                }
                else
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginParticipant();
                Thread.Sleep(8000);
                IWebElement activityTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Activities")));
                activityTab.Click();
                Console.WriteLine("Activities Link clicked");
                Thread.Sleep(5000);
                string activityLinkXPath =
                    string.Format(
                        "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
                        activityName);
                IWebElement activityElement = driver.FindElement(By.XPath(activityLinkXPath));
                Assert.IsTrue(activityElement.Displayed, string.Format("Activity {0} must be visible in activity list", activityName));
                Console.WriteLine("Activity {0} available in activity list", activityName);
                activityElement.Click();
                Thread.Sleep(2000);

                try
                {
                    Assert.IsFalse(driver.FindElement(By.XPath("//div[contains(@class,'endDate')]")).Displayed, "End Date should not be displayed");
                    Console.Error.WriteLine("Error: End Date displayed to participant");
                }
                catch
                {
                    Console.WriteLine("End Date not visible to participant");
                }

                var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
                if (dropdownMenuElement.Displayed)
                {
                    dropdownMenuElement.Click();

                    var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                    if (logoutElement != null && logoutElement.Displayed)
                    {
                        logoutElement.Click();
                        Console.WriteLine("Participant logged off successfully");
                    }
                    else
                    {
                        Console.Error.WriteLine("Logout link not clickable");
                        Assert.Fail("Logout link not clickable");
                    }

                }
                else
                {
                    Console.Error.WriteLine("Logout Dropdown not visible");
                    Assert.Fail("Logout Dropdown not visible");
                }
                Thread.Sleep(1000);
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1426 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1426 Scenario 2 Failed. Error: {0}", ex);

            }
        }

        [TestCase(Description = "PIV-1426 Administrator – Set activity end date on the Activity edit page", TestName = "Set Show Activity Date in Activity Edit Page")]
        [Order(3)]
        public void Change_Activity_To_Show_End_Date()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 2 not completed"); return;
                }
                Thread.Sleep(5000);
                LoginAndSelectActivity();
                string activityName = string.Empty;
                if (Data?.NewActivity != null)
                {
                    activityName = Data.NewActivity.Name;
                }
                else
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                Thread.Sleep(2000);
                string activityXPath =
                    string.Format("//tr[contains(@class,'itemRow')]/td[contains(.,'{0}')]", activityName);
                driver.FindElement(By.XPath(activityXPath)).Click();
                Console.WriteLine("Activity {0} clicked to edit", activityName);
                Thread.Sleep(10000);
                CheckBoxHelper.CheckedCheckBox(By.XPath("//md-checkbox[@ng-model = 'model.hideActivityEndDate']"));

                Console.WriteLine("Hide Activity End Date unchecked");
                Thread.Sleep(3000);
                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();
                Thread.Sleep(3000);
                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();
                Thread.Sleep(4000);
                driver.FindElement(By.XPath("//button[contains(.,'Save')]")).Click();
                Console.WriteLine("Activity {0} updated successfully", activityName);
                Thread.Sleep(4000);
                LogoutAdministrator();
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1426 Scenario 3 Error: {0}", ex.Message);
                Assert.Fail("PIV-1426 Scenario 3 Failed. Error: {0}", ex);
            }
        }


        [TestCase(Description = "PIV-1426 Participant – Show activity end date on the Activity detail page", TestName = "Show Activity Date in Activity Details Page")]
        [Order(4)]
        public void Validate_Show_Activity_In_Activity_Detail_Page()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 3 not completed"); return;
                }

                string activityName = string.Empty;
                if (Data?.NewActivity != null)
                {
                    activityName = Data.NewActivity.Name;
                }
                else
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                Thread.Sleep(3000);
                LoginParticipant();
                IWebElement activityTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Activities")));
                activityTab.Click();
                Console.WriteLine("Activities Link clicked");
                Thread.Sleep(8000);
                string activityLinkXPath =
                    string.Format(
                        "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
                        activityName);
                IWebElement activityElement = driver.FindElement(By.XPath(activityLinkXPath));
                Assert.IsTrue(activityElement.Displayed, string.Format("Activity {0} must be visible in activity list", activityName));
                Console.WriteLine("Activity {0} available in activity list", activityName);
                activityElement.Click();
                Thread.Sleep(1000);
                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'endDate')]")).Displayed, "End Date should be displayed");
                Console.WriteLine("Activity End Date displayed to participant");

                LogoutParticipant();
                Thread.Sleep(1000);
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1426 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1426 Scenario 2 Failed. Error: {0}", ex);
            }
        }

        #region Utilities
        private void LogoutAdministrator()
        {
            Thread.Sleep(4000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private void LoginParticipant()
        {
            Thread.Sleep(3000);
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private static void LoginAndSelectActivity()
        {
            Thread.Sleep(3000);
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);
            var activitiesTab = adminPage.GetProgramTab("Activities");
            if (activitiesTab == null)
            {
                Console.WriteLine("Unable to find activities tab");
                Assert.Fail("Unable to find activities tab");
            }
            else
            {
                Console.WriteLine("Activities Tab loaded");
                activitiesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(6000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

        }
        private void LogoutParticipant()
        {
            Thread.Sleep(3000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private PIV1426Data GetData()
        {
            PIV1426Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1426.json");
                data = JsonConvert.DeserializeObject<PIV1426Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion

    }
}
