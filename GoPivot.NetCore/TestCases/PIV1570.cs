﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.DataEntities;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Linq;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1570", Category = "Promotion")]
    public class PIV1570 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        [Order(1)]
        [TestCase(TestName = "Scenario 1: Activate Promoted Item")]
        public void Validate_Activate_Promoted_Item()
        {
            try
            {
                LoginAdministrator();
                Thread.Sleep(3000);

                IList<IWebElement> promotionElements = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]//md-switch"));
                if (promotionElements.Count == 0)
                {
                    Console.WriteLine("No Elements found to activate any promoted items");
                    Assert.Fail("No Elements found to activate any promoted items");
                }
                IWebElement firstPromotionElement = promotionElements[0];
                string firstPromotionName = driver.FindElement(By.XPath("//div[@ng-controller='programPromotions']/table/tbody/tr[1]/td[3]")).Text;
                string firstPromotionClassName = firstPromotionElement.GetAttribute("class");
                if (firstPromotionClassName.Contains("md-checked"))
                {
                    promotionElements[0].Click();
                    Console.WriteLine("{0} Promotion been deactivated to validate activation", firstPromotionName);
                    Thread.Sleep(3000);
                }

                promotionElements[0].Click();
                Console.WriteLine("{0} Promotion activated", firstPromotionName);
                Thread.Sleep(5000);

                string newFirstPromotionName = driver.FindElement(By.XPath("//div[@ng-controller='programPromotions']/table/tbody/tr[1]/td[3]")).Text;
                promotionElements = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]//md-switch[contains(@class,'md-checked')]"));
                if (promotionElements.Count == 0)
                {
                    Console.WriteLine("{0} Promotion not activated", firstPromotionName);
                    Assert.Fail("{0} Promotion not activated", firstPromotionName);
                }

                Assert.IsTrue(newFirstPromotionName == firstPromotionName, "{0} Promotion must be active", newFirstPromotionName);
                Console.WriteLine("{0} Promotion been activated", newFirstPromotionName);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1570 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1570 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [Order(2)]
        [TestCase(TestName = "Scenario 2: Deactivate Promoted Item")]
        public void Validate_Deactive_Promoted_Item()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 1 not completed"); return;
                }
                var activePromotionElements = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]//md-switch[contains(@class,'md-checked')]"));
                Console.WriteLine("{0} Active Promotions available", activePromotionElements.Count);

                if (activePromotionElements.Count == 0)
                {
                    Console.WriteLine("No Promotions available to deactive");
                    Assert.Warn("No Promotions available to deactivate");
                }

                var firstActivePromotion = activePromotionElements[0];
                firstActivePromotion.Click();
                Console.WriteLine("Promotion selected to deactive");
                Thread.Sleep(3000);
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1570 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1570 Scenario 2 Failed. Error: {0}", ex);
            }
        }

        [Order(3)]
        [TestCase(TestName = "Scenario 3: Edit Promoted Item")]
        public void Validate_Edit_Promoted_Item()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            try
            {
                IList<IWebElement> promotionElements = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]//md-switch"));
                if (promotionElements.Count == 0)
                {
                    Console.WriteLine("No Elements found to activate any promoted items");
                    Assert.Fail("No Elements found to activate any promoted items");
                }
                IWebElement element = driver.FindElement(By.XPath("//div[@ng-controller='programPromotions']/table/tbody/tr[1]/td[3]"));
                Console.WriteLine("{0} Promotion selected to edit", element.Text);
                element.Click();
                Thread.Sleep(3000);

                IWebElement headerElement = driver.FindElement(By.XPath("//h2[contains(@class,'inline')]/span/b"));
                Assert.IsTrue(headerElement.Text == "Edit Item", "Header must be valid");
                Console.WriteLine("Header validated '{0}'", headerElement.Text);

                IWebElement promotionNameElement = driver.FindElement(By.XPath("//input[@ng-model='newPromotion.name']"));
                IWebElement promotionDescriptionElement = driver.FindElement(By.XPath("//textarea[@ng-model='newPromotion.description']"));
                IWebElement imageTextElement = driver.FindElement(By.XPath("//input[@ng-model='newPromotion.textImg']"));
                IWebElement pointValueElement = driver.FindElement(By.XPath("//input[@ng-model='newPromotion.pointValue']"));
                IWebElement saveButtonElement = driver.FindElement(By.XPath("//button[contains(.,'Save')]"));
                IWebElement cancelButtonElement = driver.FindElement(By.XPath("//button[contains(.,'Cancel')]"));
                Assert.IsTrue(promotionNameElement.Displayed, "Promotion Name element must be displayed");
                Assert.IsTrue(promotionDescriptionElement.Displayed, "Promotion Description element must be displayed");
                Assert.IsTrue(imageTextElement.Displayed, "Promotion Image upload elment must be displayed");
                Assert.IsTrue(pointValueElement.Displayed, "Promotion points element must be displayed");
                Console.WriteLine("All Promotion element present while editing promotion item");
                Assert.IsTrue(saveButtonElement.Displayed, "Promotion Save button element must be displayed");
                Console.WriteLine("Save button available");
                Assert.IsTrue(cancelButtonElement.Displayed, "Promotion Cancel button element must be displayed");
                Console.WriteLine("Cancel button available");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1570 Scenario 3 Error: {0}", ex.Message);
                Assert.Fail("PIV-1570 Scenario 3 Failed. Error: {0}", ex);
            }
        }

        [Order(4)]
        [TestCase(TestName = "Scenario 4: Edit Promoted Item > Cancel")]
        public void Validate_Edit_Promoted_Item_Cancel()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 3 not completed"); return;
            }
            try
            {
                IWebElement promotionDescriptionElement = driver.FindElement(By.XPath("//textarea[@ng-model='newPromotion.description']"));
                promotionDescriptionElement.Clear();
                promotionDescriptionElement.SendKeys("Test");
                Thread.Sleep(2000);

                IWebElement cancelButtonElement = driver.FindElement(By.XPath("//button[contains(.,'Cancel')]"));
                cancelButtonElement.Click();
                Console.WriteLine("Cancel button clicked");
                Thread.Sleep(3000);

                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'modal-dialog')]")).Displayed,
                    "Modal Popup Displayed");
                Console.WriteLine("Modal Popup displayed");

                Assert.IsTrue(driver.FindElement(By.XPath("//button[contains(.,'Leave without saving')]")).Displayed, "Leave without button must be visible");
                Console.WriteLine("Button - Leave without saving displayed");
                Assert.IsTrue(driver.FindElement(By.XPath("//button[contains(.,'Stay on page')]")).Displayed, "Stay on page button must be visible");
                Console.WriteLine("Button - Stay on page displayed");

                driver.FindElement(By.XPath("//button[contains(.,'Stay on page')]")).Click();
                Console.WriteLine("Stay on page button clicked");


            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1570 Scenario 4 Error: {0}", ex.Message);
                Assert.Fail("PIV-1570 Scenario 4 Failed. Error: {0}", ex);
            }
        }

        [Order(5)]
        [TestCase(TestName = "Scenario 5: Edit Promoted Item > Save")]
        public void Validate_Edit_Promoted_Item_Save()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 4 not completed"); return;
            }

            try
            {
                IWebElement saveButtonElement = driver.FindElement(By.XPath("//button[contains(.,'Save')]"));
                saveButtonElement.Click();
                Console.WriteLine("Save button clicked");
                Thread.Sleep(3000);

                IList<IWebElement> promotionElements = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]//md-switch"));
                if (promotionElements.Count == 0)
                {
                    Console.WriteLine("Promotion Listing page not loaded");
                    Assert.Fail("Promotion Listing page not loaded");
                }
                Console.WriteLine("Promotion listing page loaded");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1570 Scenario 5 Error: {0}", ex.Message);
                Assert.Fail("PIV-1570 Scenario 5 Failed. Error: {0}", ex);
            }
        }

        public string DeletePromotionName { get; set; }
        [Order(6)]
        [TestCase(TestName = "Scenario 6: Delete Promoted Item - Confirmation Modal")]
        public void Validate_Delete_Promoted_Item_Confirm()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 5 not completed"); return;
            }

            try
            {
                Thread.Sleep(3000);
                IList<IWebElement> promotionElements = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]//md-switch"));
                if (promotionElements.Count == 0)
                {
                    Console.WriteLine("No promoted items found");
                    Assert.Fail("No promoted items found");
                }

                IWebElement firstPromotionElement = driver.FindElement(By.XPath("//div[@ng-controller='programPromotions']/table/tbody/tr[1]/td[3]"));
                string promotionName = firstPromotionElement.Text;
                DeletePromotionName = firstPromotionElement.Text;
                Console.WriteLine("Promotion selected to delete: {0}", promotionName);

                Thread.Sleep(3000);

                var deleteButton =
           driver.FindElement(
               By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]/td/i[@class='icon-trash']"));
                var builder = new Actions(driver);
                builder.MoveToElement(deleteButton).Build().Perform();
                JavaScriptExecutor.ExecuteScript("arguments[0].click();", deleteButton);

                Thread.Sleep(4000);

                Assert.IsTrue(driver.FindElement(By.ClassName("modal-dialog")).Displayed,
                    "Delete Confirmation Model is visible");

                var modelHeading = string.Format("Delete '{0}'?", promotionName);
                Assert.IsTrue(driver.FindElement(By.XPath("//div[@class='modal-header']/h2")).Text
                    .Equals(modelHeading));

                Console.WriteLine("Heading matching in popup {0}", modelHeading);

                Assert.IsTrue(
           driver.FindElement(By.XPath(
                   "//div[@class='modal-footer']/div[@class='pull-right']/button[contains(.,'Cancel')]"))
               .Displayed, "Cancel button should be visible to select");
                Assert.IsTrue(
                    driver.FindElement(By.XPath(
                            "//div[@class='modal-footer']/div[@class='pull-right']/button[contains(.,'Delete')]"))
                        .Displayed, "Delete button should be visible to select");

                Console.WriteLine("Delete / Cancel button available in modal popup");
                Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1570 Scenario 6 Error: {0}", ex.Message);
                Assert.Fail("PIV-1570 Scenario 6 Failed. Error: {0}", ex);
            }
        }

        [Order(7)]
        [TestCase(TestName = "Scenario 7: Delete confirmation modal > Delete")]
        public void Validate_Delete_Promoted_Item_Delete()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 6 not completed"); return;
            }
            try
            {
                driver.FindElement(By.XPath(
                        "//div[@class='modal-footer']/div[@class='pull-right']/button[contains(.,'Delete')]")).Click();
                Console.WriteLine("Delete button clicked");
                Thread.Sleep(3000);

                try
                {
                    IWebElement firstPromotionElement = driver.FindElement(By.XPath("//div[@ng-controller='programPromotions']/table/tbody/tr[1]/td[3]"));
                    Console.WriteLine("First Promotion Element is {0}", firstPromotionElement.Text);
                    Assert.IsFalse(firstPromotionElement.Text.Equals(DeletePromotionName), "Challenge {0} must be deleted and not in the list", DeletePromotionName);
                    Console.WriteLine("Promotion Item deleted successfully");
                }
                catch
                {
                    Console.WriteLine("No promotion items record exists");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1570 Scenario 7 Error: {0}", ex.Message);
                Assert.Fail("PIV-1570 Scenario 7 Failed. Error: {0}", ex);
            }
        }



        private void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");
            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }

            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            if (!adminPage.EditClient.Displayed)
            {
                Console.WriteLine("Clients Details not found");
                Assert.Fail("Clients Details not found");

            }
            else
            {
                Console.WriteLine("Client Details page loaded");
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);

            var promotedItemsTabElement = adminPage.GetProgramTab("Promoted Items");
            if (promotedItemsTabElement == null)
            {
                Console.WriteLine("Unable to find Promoted Items tab");
                Assert.Fail("Unable to find Promoted Items tab");
            }
            else
            {
                Console.WriteLine("Promoted Items Tab loaded");
                promotedItemsTabElement.Click();
            }

            Thread.Sleep(5000);
        }

        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}
