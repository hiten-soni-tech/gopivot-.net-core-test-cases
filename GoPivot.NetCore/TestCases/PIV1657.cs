﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;
using GoPivot.NetCore.ComponentHelper;
namespace GoPivot.NetCore.TestCases
{

    [TestFixture(TestName = "PIV-1657", Category = "Challenge")]
    public class PIV1657 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1657Data Data { get; set; }
        public string GeneratedChallengeName { get; set; }

        [TestCase(TestName = "Pre-requisite - Create new Wellness Challenge")]
        [Order(1)]
        public void Create_New_Wellness_Challenge()
        {
            try
            {

                if (Data == null)
                {
                    Data = GetData();
                }

                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAndSelectChallenge();
                CreateNewChallenge();
                IsSuccess = true;

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1657 Pre-requisite Error: {0}", ex.Message);
                Assert.Fail("PIV-1657 Pre-requisite Failed. Error: {0}", ex);
            }
        }



        [TestCase(TestName = "Scenario 1: Admin edits a challenge that has not yet started")]
        [Order(2)]
        public void Validate_Edit_Challenge()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            string errorMessage = string.Empty;
            try
            {
                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")).Click();
                Thread.Sleep(3000);

                AdminPage adminPage = new AdminPage(driver);
                var challengeCategory = adminPage.GetChallengeCategory("Custom");
                if (challengeCategory != null)
                {
                    challengeCategory.Click();
                    Console.WriteLine("Custom Challenge type selected");
                }
                else
                {
                    Console.WriteLine("Custom Challenge Type not available");
                    Assert.Fail("Custom Challenge Type not available");
                }
                Console.WriteLine("Challenge Type been updated to Custom");
                Thread.Sleep(2000);
                var challengeType = adminPage.GetChallengeType("Custom");
                if (challengeType != null)
                {
                    challengeType.Click();
                    Console.WriteLine("Custom Challenge selected");
                }
                else
                {
                    errorMessage = "Custom Challenge not found";
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);

                string randomNumber = new Random().Next(1000).ToString();
                string challengename = Data.CustomChallenge.ChallengeName.GetDynamicText();
                string startdate = Data.CustomChallenge.StartDate.GetDynamicText();
                string enddate = Data.CustomChallenge.EndDate.GetDynamicText();
                bool success = addNewChallengePage.FillStepForCustom(challengename, startdate, enddate, Data.CustomChallenge.CustomChallengeActionText, Data.CustomChallenge.CustomChallengeGoal.ToString(), Data.CustomChallenge.CustomChallengeUnit, Data.CustomChallenge.CustomChallengeTime, Data.CustomChallenge.GoalText, out errorMessage);
                if (success)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("All Custom Challenge fields updated");
                    addNewChallengePage.Step1_NextButton.Click();
                }
                else
                {
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                Thread.Sleep(3000);


                addNewChallengePage.Step2_EveryOne.Click();
                Thread.Sleep(3000);
                Console.WriteLine("All participants selected");
                addNewChallengePage.Step2_SaveButton.Click();
                Console.WriteLine("Update Challenge button clicked");


                var wait = GetWebDriverWait();
                var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
                if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
                {
                    Console.WriteLine("Challenge {0} updated successfully", challengename);
                    Assert.IsTrue(true);
                }
                else
                {
                    errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1657 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1657 Scenario 1 Failed. Error: {0}", ex);
            }
        }


        private static void LoginAndSelectChallenge()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            Thread.Sleep(5000);
            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);
            var challengesTab = adminPage.GetProgramTab("Challenges");
            if (challengesTab == null)
            {
                Console.WriteLine("Unable to find challenges tab");
                Assert.Fail("Unable to find challenges tab");
            }
            else
            {
                Console.WriteLine("Challenges Tab loaded");
                challengesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(6000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

        }
        private void CreateNewChallenge()
        {
            string errorMessage = string.Empty;
            AdminPage adminPage = new AdminPage(driver);
            Thread.Sleep(5000);
            adminPage.GetAddChallengeButton();
            if (adminPage.AddChallengeButton != null)
            {
                adminPage.AddChallengeButton.Click();
                Console.WriteLine("Add Challenge Button clicked");
                Thread.Sleep(2000);
            }
            else
            {
                Console.Error.WriteLine("Cannot add new challenge");
                Assert.Fail("cannot add new challenge");
            }


            Thread.Sleep(2000);
            var challengeCategory = adminPage.GetChallengeCategory("Wellness");
            if (challengeCategory != null)
            {
                challengeCategory.Click();
                Console.WriteLine("Wellness Challenge type selected");
            }
            else
            {
                Console.Error.WriteLine("Wellness Challenge Type not available");
                Assert.Fail("Wellness Challenge Type not available");
            }

            Thread.Sleep(2000);
            var challengeType = adminPage.GetChallengeType(Data.WellnessChallenge.WellnessChallengeType);
            if (challengeType != null)
            {
                challengeType.Click();
                Console.WriteLine("{0} Challenge Type selected", Data.WellnessChallenge.WellnessChallengeType);
            }
            else
            {
                errorMessage = string.Format("{0} Challenge type not found", Data.WellnessChallenge.WellnessChallengeType);
                Console.Error.WriteLine(errorMessage);
                Assert.Fail(errorMessage);
            }

            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            string challengename = Data.WellnessChallenge.ChallengeName.GetDynamicText();
            string startdate = Data.WellnessChallenge.StartDate.GetDynamicText();
            string endDate = Data.WellnessChallenge.EndDate.GetDynamicText();
            bool success = addNewChallengePage.FillStep1(challengename, startdate, endDate, Data.WellnessChallenge.GoalValue.ToString(), Data.WellnessChallenge.TimeLine, Data.WellnessChallenge.Description, out errorMessage);
            if (success)
            {
                Thread.Sleep(2000);
                Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                addNewChallengePage.Step1_NextButton.Click();
            }
            else
            {
                Console.WriteLine(errorMessage);
                Assert.Fail(errorMessage);
            }

            var participantCount = addNewChallengePage.Step2_ParticipantList;
            if (participantCount != null && participantCount.Count > 0)
            {
                Console.WriteLine("Participants available to select");
            }
            else
            {
                Console.Error.WriteLine("No Participants available");
                Assert.Fail("No Participants available");
            }

            success = addNewChallengePage.AddChallengers(Data.WellnessChallenge.ChallengerName, out errorMessage);
            if (success)
            {
                Console.WriteLine("Participant {0} added to challenge", Data.WellnessChallenge.ChallengerName);
            }
            else
            {
                Console.Error.WriteLine("Failed to add {0} challenger. Error: {1}", Data.WellnessChallenge.ChallengerName, errorMessage);
                Assert.Fail(errorMessage);
            }


            addNewChallengePage.Step2_CreateButton.Click();
            Console.WriteLine("Create Challenge button clicked");

            WebDriverWait wait = GetWebDriverWait();
            var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
            if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
            {
                string message = string.Format("Challenge {0} added successfully", challengename);
                Console.WriteLine(message);
                Assert.IsTrue(true);
            }
            else
            {
                errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                Assert.Fail(errorMessage);
                Console.Error.WriteLine(errorMessage);
            }
        }


        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private PIV1657Data GetData()
        {
            PIV1657Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1657.json");
                data = JsonConvert.DeserializeObject<PIV1657Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
    }
}
