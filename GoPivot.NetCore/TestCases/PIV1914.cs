﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1914", Category = "Activity")]
    public class PIV1914 : BaseStepDefinition
    {
        public PIV1914Data Data { get; set; }

        [TestCase(TestName = "Ensure Points awarded for recurring activity")]
        public void Validate_Points_Awarded_For_Recurring_Activity()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                LoginParticipant();
                Thread.Sleep(3000);
                Data.BeforeCreateActivityPoints = GetPoints();

                Console.WriteLine("Before Creating Activities Points are : {0}", Data.BeforeCreateActivityPoints);
                Thread.Sleep(5000);
                LogoutParticipant();
                Thread.Sleep(3000);
                LoginAndSelectProgram();
                Thread.Sleep(3000);
                AddNewActivity();
                Thread.Sleep(6000);
                LogoutAdministrator();
                Thread.Sleep(5000);
                LoginParticipant();
                Thread.Sleep(5000);
                Data.AfterCreateActivityPoints = GetPoints();
                Console.WriteLine("Current Points are {0}", Data.AfterCreateActivityPoints);
                Assert.IsTrue(Data.AfterCreateActivityPoints > Data.BeforeCreateActivityPoints, "Points should be added after login");
                Console.WriteLine("Points are added after login.");

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1914 Error: {0}", ex.Message);
                Assert.Fail("PIV-1914 Failed. Error: {0}", ex);
            }
        }

        #region Utilities



        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private void LogoutParticipant()
        {
            var dropdownMenuElement = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//div[contains(@class,'profileMenu')]")));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static void LoginAndSelectProgram()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(3000);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }

            Thread.Sleep(3000);
            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(3000);



        }
        private void LogoutAdministrator()
        {
            Thread.Sleep(5000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private int GetPoints()
        {
            int points = 0;
            string pointsText = driver.FindElement(By.XPath("//li[@class='points']/a")).Text;
            pointsText = pointsText.Replace("pts", "").Trim();
            pointsText = pointsText.Replace(",", "").Trim();
            int.TryParse(pointsText, out points);
            return points;
        }


        private void UpdateProgramDates()
        {
            driver.FindElement(By.LinkText("Edit")).Click();
            Console.WriteLine("Edit button clicked");

            Thread.Sleep(7000);
            GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.Id("programDatePicker"))).Click();
            Console.WriteLine("Datepicker clicked");

            Thread.Sleep(2000);
            var startDateElement = driver.FindElement(By.Name("daterangepicker_start"));
            startDateElement.Clear();
            string startDateText = "{startofmonth}".GetDynamicText();
            startDateElement.SendKeys(startDateText);

            var endDateElement = driver.FindElement(By.Name("daterangepicker_end"));
            endDateElement.Clear();
            string endDayText = "{endofnextmonth}".GetDynamicText();
            endDateElement.SendKeys(endDayText);


            driver.FindElement(By.XPath("//button[contains(.,'Apply')]")).Click();
            Thread.Sleep(3000);

            driver.FindElement(By.XPath("//button[contains(.,'Save')]")).Click();
            Thread.Sleep(3000);

            Console.WriteLine("Program Dates been updated to Start from {0} to {1}", startDateText, endDayText);

        }

        private void AddNewActivity()
        {
            AdminPage adminPage = new AdminPage(driver);
            Thread.Sleep(1000);
            var activitiesTab = adminPage.GetProgramTab("Activities");
            if (activitiesTab == null)
            {
                Console.WriteLine("Unable to find activities tab");
                Assert.Fail("Unable to find activities tab");
            }
            else
            {
                Console.WriteLine("Activities Tab loaded");
                activitiesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(7000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

            driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
            Console.WriteLine("Add Activity button clicked");

            Thread.Sleep(5000);
            Data.NewActivity = new AdminPage(driver).CreateNewActivity(Data.NewActivity, errorMessage: out string errorMessage);
            if (Data.NewActivity == null)
            {
                Assert.Fail(errorMessage);
            }
            Console.WriteLine("Activity created successfully. Name: {0}", Data.NewActivity.Name);

            Thread.Sleep(1000);

        }

        private PIV1914Data GetData()
        {
            PIV1914Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1914.json");
                data = JsonConvert.DeserializeObject<PIV1914Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion

    }
}
