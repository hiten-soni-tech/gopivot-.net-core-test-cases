﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.TestCases
{

    [TestFixture(TestName = "PIV-93", Category = "Challenge")]
    public class PIV93 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public string ChallengeNotStartedName { get; set; }
        public string ChallengeAlreadyStartedName { get; set; }
        public DateTime ChallengeNotStartedStartDate { get; set; }
        public PIV93Data Data { get; set; }
        [TestCase(Description = "PIV-93: Validate Challenge Details Tab", TestName = "Pre-requisite: Create Challenges")]
        [Order(1)]
        public void CreateChallenges()
        {
            string errorMessage = string.Empty;
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                var loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
                loginPage.LoginButton.Click();


                Thread.Sleep(1000);

                Console.WriteLine("Administrator Logged in successfully");

                var adminPage = new AdminPage(driver);
                if (adminPage.Clients != null && adminPage.Clients.Displayed)
                {
                    adminPage.Clients.Click();
                    Console.WriteLine("Clients link clicked");
                }
                else
                {
                    Console.WriteLine("Clients Link not found");
                    Assert.Fail("Clients link not found");
                }


                var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
                if (clientElement != null && clientElement.Displayed)
                {
                    Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Username);
                    clientElement.Click();
                }
                else
                {
                    Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Username);
                    Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Username);
                }

                var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
                if (programElement != null && programElement.Displayed)
                {
                    programElement.Click();
                    Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
                }
                else
                {
                    Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
                }

                if (adminPage.ProgramTabs.Count > 0)
                {
                    Console.WriteLine("Program Listing page loaded");

                }
                else
                {
                    Console.WriteLine("Program details page not loaded");
                    Assert.Fail("Failed to load Program details");
                }

                Thread.Sleep(1000);
                var challengeTab = adminPage.GetProgramTab("Challenges");
                if (challengeTab == null)
                {
                    Console.WriteLine("Unable to find challenges tab");
                    Assert.Fail("Unable to find challenges tab");
                }
                else
                {
                    Console.WriteLine("Challenge Tab loaded");
                    challengeTab.Click();
                }

                Thread.Sleep(3000);

                adminPage.GetAddChallengeButton();
                if (adminPage.AddChallengeButton != null)
                {
                    adminPage.AddChallengeButton.Click();
                    Console.WriteLine("Add Challenge Button clicked");
                    Thread.Sleep(2000);
                }
                else
                {
                    Console.WriteLine("Cannot add new challenge");
                    Assert.Fail("cannot add new challenge");
                }

                Thread.Sleep(2000);

                #region Create Challenge Not Started 
                var challengeCategory = adminPage.GetChallengeCategory("Custom");
                if (challengeCategory != null)
                {
                    challengeCategory.Click();
                    Console.WriteLine("Custom Challenge type selected");
                }
                else
                {
                    Console.WriteLine("Custom Challenge Type not available");
                    Assert.Fail("Custom Challenge Type not available");
                }
                Thread.Sleep(2000);
                var challengeType = adminPage.GetChallengeType("Custom");
                if (challengeType != null)
                {
                    challengeType.Click();
                    Console.WriteLine("Custom Challenge selected");
                }
                else
                {
                    errorMessage = "Custom Challenge not found";
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }
                AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
                string randomNumber = new Random().Next(1000).ToString();
                string challengename = Data.ChallengeNotStarted.ChallengeName;
                challengename = challengename.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());
                challengename = challengename.Replace("{randomnumber}", randomNumber);
                ChallengeNotStartedName = challengename;

                string startDate = Data.ChallengeNotStarted.StartDate;
                startDate = startDate.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());
                ChallengeNotStartedStartDate = DateTime.Parse(startDate);
                DateTime reference = DateTime.Now;
                DateTime firstDayThisMonth = new DateTime(reference.Year, reference.Month, 1);
                DateTime firstDayPlusTwoMonths = firstDayThisMonth.AddMonths(2);
                DateTime lastDayNextMonth = firstDayPlusTwoMonths.AddDays(-1);
                DateTime endOfLastDayNextMonth = firstDayPlusTwoMonths.AddTicks(-1);
                string enddate = Data.ChallengeNotStarted.EndDate.Replace("{endofnextmonth}", endOfLastDayNextMonth.ToShortDateString());
                bool success = addNewChallengePage.FillStepForCustom(challengename, startDate, enddate, Data.ChallengeNotStarted.CustomChallengeActionText, Data.ChallengeNotStarted.CustomChallengeGoal.ToString(), Data.ChallengeNotStarted.CustomChallengeUnit, Data.ChallengeNotStarted.CustomChallengeTime, Data.ChallengeNotStarted.GoalText, out errorMessage);
                if (success)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                    addNewChallengePage.Step1_NextButton.Click();
                }
                else
                {
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                Thread.Sleep(1000);
                var participantCount = addNewChallengePage.Step2_ParticipantList;
                if (participantCount != null && participantCount.Count > 0)
                {
                    Console.WriteLine("Participants available to select");
                }
                else
                {
                    Console.WriteLine("No Participants available");
                    Assert.Fail("No Participants available");
                }

                success = addNewChallengePage.AddChallengers(Data.ChallengeNotStarted.ChallengerName, out errorMessage);
                if (success)
                {
                    Console.WriteLine("Participant {0} added to challenge", Data.ChallengeNotStarted.ChallengerName);
                }
                else
                {
                    Console.WriteLine("Failed to add {0} challenger. Error: {1}", Data.ChallengeNotStarted.ChallengerName, errorMessage);
                    Assert.Fail(errorMessage);
                }
                Thread.Sleep(3000);
                addNewChallengePage.Step2_CreateButton.Click();
                Console.WriteLine("Create Challenge button clicked");


                WebDriverWait wait = GetWebDriverWait();
                var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
                if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
                {
                    string message = string.Format("Challenge {0} added successfully", challengename);
                    Console.WriteLine(message);
                    Assert.IsTrue(true);
                }
                else
                {
                    errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                    Console.Error.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                Thread.Sleep(1000);
                adminPage.GetAddChallengeButton();
                adminPage.AddChallengeButton.Click();
                Thread.Sleep(2000);
                #endregion

                Console.WriteLine("-----------------------------------------------------");

                #region Create Challenge - Already Started
                challengeCategory = adminPage.GetChallengeCategory("Custom");
                if (challengeCategory != null)
                {
                    challengeCategory.Click();
                    Console.WriteLine("Custom Challenge type selected");
                }
                else
                {
                    Console.WriteLine("Custom Challenge Type not available");
                    Assert.Fail("Custom Challenge Type not available");
                }
                Thread.Sleep(2000);
                challengeType = adminPage.GetChallengeType("Custom");
                if (challengeType != null)
                {
                    challengeType.Click();
                    Console.WriteLine("Custom Challenge selected");
                }
                else
                {
                    errorMessage = "Custom Challenge not found";
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                var addNewChallengeAlreadyStartedPage = new AddNewChallengePage(driver);
                randomNumber = new Random().Next(1000).ToString();
                challengename = Data.ChallengeAlreadyStarted.ChallengeName;
                challengename = challengename.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
                challengename = challengename.Replace("{randomnumber}", randomNumber);
                ChallengeAlreadyStartedName = challengename;

                startDate = Data.ChallengeAlreadyStarted.StartDate;
                startDate = startDate.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
                enddate = Data.ChallengeNotStarted.EndDate.Replace("{endofnextmonth}", endOfLastDayNextMonth.ToShortDateString());
                success = addNewChallengeAlreadyStartedPage.FillStepForCustom(challengename, startDate, enddate, Data.ChallengeAlreadyStarted.CustomChallengeActionText, Data.ChallengeAlreadyStarted.CustomChallengeGoal.ToString(), Data.ChallengeAlreadyStarted.CustomChallengeUnit, Data.ChallengeAlreadyStarted.CustomChallengeTime, Data.ChallengeAlreadyStarted.GoalText, out errorMessage);
                if (success)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                    addNewChallengeAlreadyStartedPage.Step1_NextButton.Click();
                }
                else
                {
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                Thread.Sleep(1000);
                participantCount = addNewChallengeAlreadyStartedPage.Step2_ParticipantList;
                if (participantCount != null && participantCount.Count > 0)
                {
                    Console.WriteLine("Participants available to select");
                }
                else
                {
                    Console.WriteLine("No Participants available");
                    Assert.Fail("No Participants available");
                }

                success = addNewChallengeAlreadyStartedPage.AddChallengers(Data.ChallengeNotStarted.ChallengerName, out errorMessage);
                if (success)
                {
                    Console.WriteLine("Participant {0} added to challenge", Data.ChallengeAlreadyStarted.ChallengerName);
                }
                else
                {
                    Console.WriteLine("Failed to add {0} challenger. Error: {1}", Data.ChallengeAlreadyStarted.ChallengerName, errorMessage);
                    Assert.Fail(errorMessage);
                }
                Thread.Sleep(3000);
                addNewChallengeAlreadyStartedPage.Step2_CreateButton.Click();
                Console.WriteLine("Create Challenge button clicked");


                wait = GetWebDriverWait();
                addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
                if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
                {
                    string message = string.Format("Challenge {0} added successfully", challengename);
                    Console.WriteLine(message);
                    Assert.IsTrue(true);
                }
                else
                {
                    errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                    Console.Error.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }
                #endregion

                Thread.Sleep(2000);
                var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
                if (dropdownMenuElement.Displayed)
                {
                    dropdownMenuElement.Click();

                    var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                    if (logoutElement != null && logoutElement.Displayed)
                    {
                        logoutElement.Click();
                        Console.WriteLine("Administrator logged off successfully");
                    }
                    else
                    {
                        Console.Error.WriteLine("Logout link not clickable");
                        Assert.Fail("Logout link not clickable");
                    }

                }
                else
                {
                    Console.Error.WriteLine("Logout Dropdown not visible");
                    Assert.Fail("Logout Dropdown not visible");
                }
                Thread.Sleep(1000);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-93 Pre-requisites Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-93 Pre-requisites Failed. Error: {0}", ex);
            }
        }
        [TestCase(Description = "PIV-93 Scenario 1:  View of Details tab after user Joins available - Challenge has not started", TestName = "Challenge Details - Challenge not started")]
        [Order(2)]
        public void ValidateChallengeDetailsForChallengeNotStarted()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 0 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Data = GetData();
                }
                if (string.IsNullOrEmpty(ChallengeNotStartedName))
                {
                    Console.WriteLine("No Challenge created");
                    Assert.Fail("No Challenge created");
                }
                var loginPage = new LoginPage(driver);
                Console.WriteLine("Login Page loaded successfully");
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
                loginPage.LoginButton.Click();
                Thread.Sleep(1000);
                var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement1 != null && logoutElement1.Displayed)
                {
                    Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
                }
                else
                {
                    Console.Error.WriteLine("Login Failed. Failed to load participant view");
                    Assert.Fail("Login Failed. Failed to load participant view");
                }

                IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
                if (challengeTab != null && challengeTab.Displayed)
                {
                    challengeTab.Click();
                    Console.WriteLine("Challenge Tab Clicked");
                }

                By loadingImage = By.TagName("http-busy");
                Thread.Sleep(3000);
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                var availableChallengeXPath = string.Format("//span[@ng-if='!showAvailableChallenges']//p[contains(@class,'title') and contains(.,'{0}')]", ChallengeNotStartedName);
                driver.FindElement(By.XPath(availableChallengeXPath)).Click();
                Console.WriteLine("{0} Challenge found in available challenges", ChallengeNotStartedName);

                Thread.Sleep(1000);

                IWebElement joinButtonElement = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(.,'Join!')]")));
                if (joinButtonElement != null && joinButtonElement.Displayed)
                {
                    Thread.Sleep(1000);
                    GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                    joinButtonElement.Click();
                    Console.WriteLine("Join Button available and clicked to join this challenge");
                }

                IWebElement goalTextElement = driver.FindElement(By.XPath("//div[@class='ng-scope']/h2[contains(.,'Goal')]/following-sibling::p[1]"));
                if (goalTextElement != null && goalTextElement.Displayed)
                {
                    if (goalTextElement.Text.Equals(Data.ChallengeNotStarted.GoalText, StringComparison.InvariantCultureIgnoreCase))
                    {
                        Console.WriteLine("Goal Text {0} matching", Data.ChallengeNotStarted.GoalText);

                    }
                }
                else
                {
                    Console.Error.WriteLine("Cannot find Goal Text");
                    Assert.Fail("failed to load goal text element");
                }

                Thread.Sleep(1000);
                try
                {

                    Assert.IsFalse(driver.FindElement(By.XPath("//a[contains(.,'Join!')]")).Displayed, "Join button should not be available after participant joined already");
                }
                catch
                {
                    // Do nothing
                    Console.WriteLine("Join button not available now");
                }

                Assert.IsTrue(driver.FindElement(By.ClassName("icon-check")).Displayed, "Joined icon should be displayed");
                Console.WriteLine("Joined icon displayed");

                var joinedDivElement = driver.FindElement(By.ClassName("challengeJoinedText"));
                Assert.IsTrue(joinedDivElement.Displayed && joinedDivElement.Text.Contains("Joined"), "Joined text should be displayed");
                Console.WriteLine("Joined text displayed");

                string challengeStartText = string.Format("This challenge will begin on {0}", ChallengeNotStartedStartDate.ToString("MMMM d, yyyy"));
                var challengeStartTextElement = driver.FindElement(By.XPath("//div[@ng-if='!challenge.hasStarted && challenge.hasJoined']/p"));
                string challengeStartTextElementText = challengeStartTextElement.Text;
                Console.WriteLine(challengeStartTextElement.Text);

                Assert.IsTrue(challengeStartTextElement.Text.Contains(challengeStartText), "Challenge Start Text should be matching");
                Console.WriteLine("Challenge Start Text matching with start date of challenge {0}", ChallengeNotStartedStartDate.ToString("MMMM d,yyyy"));
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-93 Scenario 1 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-93 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-93 Scenario 2:  User Joins available challenge from Challenge card - Challenge has started", TestName = "Challenge Details - Challenge Already Started")]
        [Order(3)]
        public void ValidateChallengeDetailsForChallengeAlreadyStarted()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
                if (challengeTab != null && challengeTab.Displayed)
                {
                    challengeTab.Click();
                    Console.WriteLine("Challenge Tab Clicked");
                }

                By loadingImage = By.TagName("http-busy");
                Thread.Sleep(12000);
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));


                var availableChallengeXPath = string.Format("//span[@ng-if='!showAvailableChallenges']//p[contains(@class,'title') and contains(.,'{0}')]", ChallengeAlreadyStartedName);
                driver.FindElement(By.XPath(availableChallengeXPath)).Click();
                Console.WriteLine("{0} Challenge found in available challenges", ChallengeNotStartedName);

                Thread.Sleep(1000);
                Assert.IsTrue(driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Details')]")).Displayed, "Details tab should be visible");
                IWebElement joinButtonElement = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(.,'Join!')]")));
                if (joinButtonElement != null && joinButtonElement.Displayed)
                {
                    Thread.Sleep(1000);
                    GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));
                    joinButtonElement.Click();
                    Console.WriteLine("Join Button available and clicked to join this challenge");
                }
                Thread.Sleep(2000);
                try
                {
                    Assert.IsFalse(driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Details')]")).Displayed, "Details tab should NOT be visible");
                    Assert.IsFalse(driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Participants')]")).Displayed, "Participants tab should NOT be visible");

                }
                catch
                {
                    Console.WriteLine("Details / Participants tabs is not available now");
                }
                Assert.IsTrue(driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Progress')]")).Displayed, "Progress tab should be visible");
                Assert.IsTrue(driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Leaderboard')]")).Displayed, "Leaderboard tab should be visible");
                Console.WriteLine("Progress / Leaderboard Tabs are visible");
            }
            catch (Exception ex)
            {
                IsSuccess = true;
                Console.WriteLine("PIV-93 Scenario 2 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-93 Scenario 2 Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-93 Scenario 3:   Display challenges a participant has joined in the Joined Challenges UI", TestName = "Challenge Listing - Validate Joined Challenges")]
        [Order(4)]
        public void ValidateChallengesinJoinedChallenges()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            try
            {
                Thread.Sleep(3000);
                IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
                if (challengeTab != null && challengeTab.Displayed)
                {
                    challengeTab.Click();
                    Console.WriteLine("Challenge Tab Clicked");
                }

                By loadingImage = By.TagName("http-busy");
                Thread.Sleep(12000);
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                Assert.IsTrue(driver.FindElement(By.XPath(string.Format("//span[@ng-if='!showJoinedChallenges']//p[contains(@class,'title') and contains(.,'{0}')]", ChallengeAlreadyStartedName))).Displayed, string.Format("Challenge {0} should be available in joined list", ChallengeAlreadyStartedName));
                Assert.IsTrue(driver.FindElement(By.XPath(string.Format("//span[@ng-if='!showJoinedChallenges']//p[contains(@class,'title') and contains(.,'{0}')]", ChallengeNotStartedName))).Displayed, string.Format("Challenge {0} should be available in joined list", ChallengeAlreadyStartedName));

                Console.WriteLine("{0} Challenge found in joined challenges", ChallengeNotStartedName);
                Console.WriteLine("{0} Challenge found in joined challenges", ChallengeAlreadyStartedName);

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-93 Scenario 3 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-93 Scenario 3 Failed. Error: {0}", ex);
            }
        }

        private PIV93Data GetData()
        {
            PIV93Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV93.json");
                data = JsonConvert.DeserializeObject<PIV93Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}
