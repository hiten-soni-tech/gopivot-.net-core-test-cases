﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;


namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1948", Category = "Activity")]
    public class PIV1948 : BaseStepDefinition
    {
        public PIV1948Data Data { get; set; }
        [TestCase(TestName = "Ensure activities only award points within program's valid date range")]
        public void Validate_Award_Points_Within_Program_Valid_Date_Range()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAndSelectProgram();
                UpdateProgramDates();
                AddNewChallenge();
                if (!string.IsNullOrEmpty(Data.NewChallengeName))
                {
                    AddNewActivity();
                    LoginParticipant();
                    JoinChallenge();

                }
                else
                {
                    Console.WriteLine("No Challenge available to create new activity");
                    Assert.Fail("Failed to create new challenge for activity");

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1948 Error: {0}", ex.Message);
                Assert.Fail("PIV-1948 Failed. Error: {0}", ex);
            }
        }



        #region Utilities

        public void AddNewChallenge()
        {
            string errorMessage = string.Empty;
            AdminPage adminPage = new AdminPage(driver);
            Thread.Sleep(3000);
            var challengesTab = adminPage.GetProgramTab("Challenges");
            if (challengesTab == null)
            {
                Console.WriteLine("Unable to find challenges tab");
                Assert.Fail("Unable to find challenges tab");
            }
            else
            {
                Console.WriteLine("Challenges Tab loaded");
                challengesTab.Click();
            }
            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(6000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

            adminPage.GetAddChallengeButton();
            if (adminPage.AddChallengeButton != null)
            {
                adminPage.AddChallengeButton.Click();
                Console.WriteLine("Add Challenge Button clicked");
                Thread.Sleep(2000);
            }
            else
            {
                Console.Error.WriteLine("Cannot add new challenge");
                Assert.Fail("cannot add new challenge");
            }
            Thread.Sleep(2000);
            var challengeCategory = adminPage.GetChallengeCategory("Wellness");
            if (challengeCategory != null)
            {
                challengeCategory.Click();
                Console.WriteLine("Wellness Challenge type selected");
            }
            else
            {
                Console.Error.WriteLine("Wellness Challenge Type not available");
                Assert.Fail("Wellness Challenge Type not available");
            }

            Thread.Sleep(2000);
            var challengeType = adminPage.GetChallengeType(Data.WellnessChallenge.WellnessChallengeType);
            if (challengeType != null)
            {
                challengeType.Click();
                Console.WriteLine("{0} Challenge Type selected", Data.WellnessChallenge.WellnessChallengeType);
            }
            else
            {
                errorMessage = string.Format("{0} Challenge type not found", Data.WellnessChallenge.WellnessChallengeType);
                Console.Error.WriteLine(errorMessage);
                Assert.Fail(errorMessage);
            }

            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            string challengename = Data.WellnessChallenge.ChallengeName.GetDynamicText();
            string startdate = Data.WellnessChallenge.StartDate.GetDynamicText();
            string endDate = Data.WellnessChallenge.EndDate.GetDynamicText();
            bool success = addNewChallengePage.FillStep1(challengename, startdate, endDate, Data.WellnessChallenge.GoalValue.ToString(), Data.WellnessChallenge.TimeLine, Data.WellnessChallenge.Description, out errorMessage);
            if (success)
            {
                Thread.Sleep(2000);
                Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                addNewChallengePage.Step1_NextButton.Click();
            }
            else
            {
                Console.WriteLine(errorMessage);
                Assert.Fail(errorMessage);
            }

            var participantCount = addNewChallengePage.Step2_ParticipantList;
            if (participantCount != null && participantCount.Count > 0)
            {
                Console.WriteLine("Participants available to select");
            }
            else
            {
                Console.Error.WriteLine("No Participants available");
                Assert.Fail("No Participants available");
            }

            success = addNewChallengePage.AddChallengers(Data.WellnessChallenge.ChallengerName, out errorMessage);
            if (success)
            {
                Console.WriteLine("Participant {0} added to challenge", Data.WellnessChallenge.ChallengerName);
            }
            else
            {
                Console.Error.WriteLine("Failed to add {0} challenger. Error: {1}", Data.WellnessChallenge.ChallengerName, errorMessage);
                Assert.Fail(errorMessage);
            }


            addNewChallengePage.Step2_CreateButton.Click();
            Console.WriteLine("Create Challenge button clicked");

            Thread.Sleep(3000);
            WebDriverWait wait = GetWebDriverWait();
            var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
            if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
            {
                Data.NewChallengeName = challengename;
                string message = string.Format("Challenge {0} added successfully", challengename);
                Console.WriteLine(message);
                Assert.IsTrue(true);
            }
            else
            {
                errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                Assert.Fail(errorMessage);
                Console.Error.WriteLine(errorMessage);
            }

        }

        private void JoinChallenge()
        {
            Thread.Sleep(5000);

            string pointsText = driver.FindElement(By.XPath("//li[@class='points']/a")).Text;
            pointsText = pointsText.Replace("pts", "").Trim();
            pointsText = pointsText.Replace(",", "").Trim();
            int.TryParse(pointsText, out int points);
            Console.WriteLine("Current Points are: {0}", points);
            Thread.Sleep(3000);
            IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
            if (challengeTab != null && challengeTab.Displayed)
            {
                challengeTab.Click();
                Console.WriteLine("Challenge Tab Clicked");
            }

            Thread.Sleep(1000);

            var challengeList = driver.FindElements(By.XPath("//div[@class='panel challenge ng-scope']"));
            if (challengeList != null && challengeList.Count > 0)
            {
                Console.WriteLine("{0} Challenges available for participant", challengeList.Count);
            }
            else
            {
                Console.Error.WriteLine("Failed to load challenges");
                Assert.Fail("Failed to load challenges");
            }

            var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", Data.NewChallengeName))));
            if (challenge != null && challenge.Displayed)
            {
                challenge.Click();
                Console.WriteLine("{0} available challenge found", Data.NewChallengeName);
            }
            else
            {
                Console.Error.WriteLine("{0} available challenge not found", Data.NewChallengeName);
                Assert.Fail("{0} available challenge not found", Data.NewChallengeName);
            }

            IWebElement participantTabElement = driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Participants')]"));
            if (participantTabElement != null && participantTabElement.Displayed)
            {
                Console.WriteLine("Participant tab is visible");
                Assert.IsTrue(participantTabElement.Displayed);
            }
            else
            {
                Console.Error.WriteLine("Cannot find participants tab");
                Assert.Fail("Cannot find participants tab");
            }
            Thread.Sleep(1000);
            IWebElement joinButtonElement = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(.,'Join!')]")));
            if (joinButtonElement != null && joinButtonElement.Displayed)
            {
                Thread.Sleep(2000);
                joinButtonElement.Click();
                Console.WriteLine("Join Button available and clicked to join this challenge");
            }
            else
            {
                Console.WriteLine("Join button not available or already joined");
            }
            Thread.Sleep(3000);

            pointsText = driver.FindElement(By.XPath("//li[@class='points']/a")).Text;
            pointsText = pointsText.Replace("pts", "").Trim();
            pointsText = pointsText.Replace(",", "").Trim();
            int.TryParse(pointsText, out int newPoints);

            Assert.IsTrue(points == newPoints, "Points must NOT be updated");
            Console.WriteLine("Points not updated");


        }
        private void AddTransactionToActivity()
        {
            Thread.Sleep(3000);

            string pointsText = driver.FindElement(By.XPath("//li[@class='points']/a")).Text;
            pointsText = pointsText.Replace("pts", "").Trim();
            pointsText = pointsText.Replace(",", "").Trim();
            int.TryParse(pointsText, out int points);
            Console.WriteLine("Current Points are: {0}", points);

            string activityName = string.Empty;
            if (Data?.NewActivity != null)
            {
                activityName = Data.NewActivity.Name;
            }
            //  activityName = "PIV-1948 Custom Activity - 682";
            IWebElement activityTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Activities")));
            activityTab.Click();
            Console.WriteLine("Activities Link clicked");
            Thread.Sleep(7000);
            string activityLinkXPath =
                string.Format(
                    "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
                    activityName);
            IWebElement activityElement = driver.FindElement(By.XPath(activityLinkXPath));
            Assert.IsTrue(activityElement.Displayed, string.Format("Activity {0} must be visible in activity list", activityName));
            Console.WriteLine("Activity {0} available in activity list", activityName);
            activityElement.Click();
            Thread.Sleep(2000);

            var milesActivityElement = driver.FindElement(By.Name("activityDecimal"));
            milesActivityElement.Clear();
            milesActivityElement.SendKeys("100");
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//button[contains(.,'Save')]")).Click();
            Thread.Sleep(3000);
            Assert.IsTrue(driver.FindElement(By.XPath("//i[contains(@class,'icon-check')]")).Displayed, "Success message should be displaed");
            Console.WriteLine("Miles logged successfully");

            pointsText = driver.FindElement(By.XPath("//li[@class='points']/a")).Text;
            pointsText = pointsText.Replace("pts", "").Trim();
            pointsText = pointsText.Replace(",", "").Trim();
            int.TryParse(pointsText, out int newPoints);

            Assert.IsTrue(points == newPoints, "Points must NOT be updated");
            Console.WriteLine("Points not updated");

        }
        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            Thread.Sleep(3000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private void LogoutParticipant()
        {
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private void LogoutAdministrator()
        {
            Thread.Sleep(5000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static void LoginAndSelectProgram()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }

            Thread.Sleep(3000);
            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(3000);



        }

        private void UpdateProgramDates()
        {
            driver.FindElement(By.LinkText("Edit")).Click();
            Console.WriteLine("Edit button clicked");

            Thread.Sleep(8000);
            GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.Id("programDatePicker"))).Click();
            Console.WriteLine("Datepicker clicked");

            Thread.Sleep(2000);
            var startDateElement = driver.FindElement(By.Name("daterangepicker_start"));
            startDateElement.Clear();
            string startDateText = "{startofmonth}".GetDynamicText();
            startDateElement.SendKeys(startDateText);

            var endDateElement = driver.FindElement(By.Name("daterangepicker_end"));
            endDateElement.Clear();
            string endDayText = "{previousday}".GetDynamicText();
            endDateElement.SendKeys(endDayText);


            driver.FindElement(By.XPath("//button[contains(.,'Apply')]")).Click();
            Thread.Sleep(3000);

            driver.FindElement(By.XPath("//button[contains(.,'Save')]")).Click();
            Thread.Sleep(3000);

            Console.WriteLine("Program Dates been updated to Start from {0} to {1}", startDateText, endDayText);

        }

        private void AddNewActivity()
        {
            AdminPage adminPage = new AdminPage(driver);
            Thread.Sleep(1000);
            var activitiesTab = adminPage.GetProgramTab("Activities");
            if (activitiesTab == null)
            {
                Console.WriteLine("Unable to find activities tab");
                Assert.Fail("Unable to find activities tab");
            }
            else
            {
                Console.WriteLine("Activities Tab loaded");
                activitiesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(7000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

            driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
            Console.WriteLine("Add Activity button clicked");

            Thread.Sleep(5000);
            Data.NewActivity.ChallengeName = Data.NewChallengeName;
            Data.NewActivity = new AdminPage(driver).CreateNewActivity(Data.NewActivity, errorMessage: out string errorMessage);
            if (Data.NewActivity == null)
            {
                Assert.Fail(errorMessage);
            }
            Console.WriteLine("Activity created successfully. Name: {0}", Data.NewActivity.Name);

            LogoutAdministrator();
            Thread.Sleep(1000);

        }

        private PIV1948Data GetData()
        {
            PIV1948Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1948.json");
                data = JsonConvert.DeserializeObject<PIV1948Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
