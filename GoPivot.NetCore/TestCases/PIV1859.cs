﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;
using GoPivot.NetCore.ComponentHelper;
using System.Collections.Generic;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1859", Category = "Challenge")]
    public class PIV1859 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1859Data Data { get; set; }
        [TestCase(TestName = "Scenario 0: Prerequisite Create New Custom Challenge")]
        [Order(1)]
        public void Prerequisite_Create_New_Challenge()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAdministrator();
                CreateNewChallenge();
                LogoutAdministrator();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("Creating Custom Challenge Failed. Error: {0}", ex);

            }
        }
        [TestCase(TestName = "Scenario 1: Display a Leaderboard of the top 10 participants in the challenge")]
        [Order(2)]
        public void Validate_Display_Leadership_Board_Of_Top_10_Participants()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 0 not completed"); return;
            }
            //Data = GetData();
            if (Data == null)
            {
                Console.WriteLine("No Valid Input Data found");
                Assert.Fail("No Valid Input Data found");
            }
            // Data.GeneratedChallengeName = "PIV-1859 Custom 1/1/2019-24";
            int challengerCount = 0;
            foreach (var challenger in Data.Challengers)
            {
                LoginParticipant(challenger.Username, challenger.Password);
                Thread.Sleep(12000);
                challengerCount++;
                IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
                if (challengeTab != null && challengeTab.Displayed)
                {
                    challengeTab.Click();
                    Console.WriteLine("Challenge Tab Clicked");
                }


                Thread.Sleep(8000);
                var challengeList = driver.FindElements(By.XPath("//div[@class='panel challenge ng-scope']"));
                if (challengeList != null && challengeList.Count > 0)
                {
                    Console.WriteLine("{0} Challenges available for participant", challengeList.Count);
                }
                else
                {
                    Console.Error.WriteLine("Failed to load challenges");
                    Assert.Fail("Failed to load challenges");
                }

                var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", Data.GeneratedChallengeName))));
                if (challenge != null && challenge.Displayed)
                {
                    challenge.Click();
                    Console.WriteLine("{0} available challenge found", Data.GeneratedChallengeName);
                }
                else
                {
                    Console.Error.WriteLine("{0} available challenge not found", Data.GeneratedChallengeName);
                    Assert.Fail("{0} available challenge not found", Data.GeneratedChallengeName);
                }

                try
                {
                    IWebElement joinButtonElement = driver.FindElement(By.XPath("//a[contains(.,'Join!')]"));
                    if (joinButtonElement != null && joinButtonElement.Displayed)
                    {
                        Thread.Sleep(3000);
                        joinButtonElement.Click();
                        Console.WriteLine("Join Button available and clicked to join this challenge");
                    }
                    else
                    {
                        Console.WriteLine("Join button not available or already joined");
                    }
                }
                catch
                {
                    IWebElement logItElement = driver.FindElement(By.XPath("//h2[contains(.,'Log It')]"));
                    if (logItElement != null && logItElement.Displayed)
                    {
                        Console.WriteLine("Participant already joined");
                    }
                }

                if (Data.Challengers.Length == challengerCount)
                {
                    Thread.Sleep(3000);
                    driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Leaderboard')]")).Click();
                    Console.WriteLine("Leaderboard selected for participant : {0}", challenger.Name);


                    Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='leaderboardHeader']/th[contains(.,'Rank')]")).Displayed, "Rank header displayed");
                    Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='leaderboardHeader']/th[contains(.,'Name')]")).Displayed, "Name header displayed");
                    Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='leaderboardHeader']/th[contains(.,'Total Time(s)')]")).Displayed, "Total Times header displayed");
                    Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='leaderboardHeader']/th[contains(.,'Goals Met')]")).Displayed, "Goals Met header displayed");
                    Console.WriteLine("All Headers matching");
                    Console.WriteLine(driver.FindElement(By.XPath("//table[contains(@class,'table')]//tr[2]")).Text);
                    Assert.IsTrue(driver.FindElement(By.XPath("//table[contains(@class,'table')]//tr[2]")).Text.Contains("YOUR RANK"), "Your Rank section must be displayed");
                    Console.WriteLine("Your Rank section displayed successfully");

                    Int32.TryParse(driver.FindElement(By.XPath("//tr[@class='my-rank']//td[1]")).Text, out int rank);
                    Assert.IsTrue(rank > 0, "Rank must be greater than 1");
                    Console.WriteLine("Rank for participant set correctly");

                    Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='my-rank']//td[3]")).Text == "You", "Rank Name be must be 'You'");
                    Console.WriteLine("Rank Name been set correctly");

                    Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='my-rank']//td[4]")).Text == "0.00", "Total Time must be 0.00");
                    Console.WriteLine("Total Time been set correctly");

                    Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='my-rank']//td[5]")).Text == "0", "Goals Met must be 0");
                    Console.WriteLine("Goals met must be 0");

                    Assert.IsTrue(driver.FindElement(By.XPath("//table[contains(@class,'table')]//tr[4]")).Text.Contains("TOP 10"), "Top 10 section must be displayed");
                    Console.WriteLine("Top 10 section displayed successfully");

                    IList<IWebElement> participants = driver.FindElements(By.XPath("//tr[@ng-if='usersLeaderboardList.length']"));
                    Assert.IsTrue(participants.Count <= 10, "Participants count must be less than or equal to 10");
                    Console.WriteLine("Participant count is less than 10. Current Count: {0}", participants.Count);

                    if (Data.Challengers.Length > 10)
                    {
                        Assert.IsTrue(driver.FindElement(By.LinkText("Show All")).Displayed, "Show All Link must be displayed");
                        Console.WriteLine("Show All displayed");
                    }
                }

                LogoutParticipant();
            }
        }
        


        private void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");
            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }

            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            if (!adminPage.EditClient.Displayed)
            {
                Console.WriteLine("Clients Details not found");
                Assert.Fail("Clients Details not found");

            }
            else
            {
                Console.WriteLine("Client Details page loaded");
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);

            var challengeTab = adminPage.GetProgramTab("Challenges");
            if (challengeTab == null)
            {
                Console.WriteLine("Unable to find challenges tab");
                Assert.Fail("Unable to find challenges tab");
            }
            else
            {
                Console.WriteLine("Challenge Tab loaded");
                challengeTab.Click();
            }

            Thread.Sleep(5000);
        }

        private void CreateNewChallenge()
        {
            AdminPage adminPage = new AdminPage(driver);
            adminPage.GetAddChallengeButton();
            if (adminPage.AddChallengeButton != null)
            {
                adminPage.AddChallengeButton.Click();
                Console.WriteLine("Add Challenge Button clicked");
                Thread.Sleep(2000);
            }
            else
            {
                Console.WriteLine("Cannot add new challenge");
                Assert.Fail("cannot add new challenge");
            }

            Thread.Sleep(2000);

            var challengeCategory = adminPage.GetChallengeCategory("Custom");
            if (challengeCategory != null)
            {
                challengeCategory.Click();
                Console.WriteLine("Custom Challenge type selected");
            }
            else
            {
                Console.WriteLine("Custom Challenge Type not available");
                Assert.Fail("Custom Challenge Type not available");
            }

            Thread.Sleep(2000);
            var challengeType = adminPage.GetChallengeType("Custom");
            string errorMessage;
            if (challengeType != null)
            {
                challengeType.Click();
                Console.WriteLine("Custom Challenge selected");
            }
            else
            {
                errorMessage = "Custom Challenge not found";
                Console.WriteLine(errorMessage);
                Assert.Fail(errorMessage);
            }


            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);

            var challengename = Data.Challenge.ChallengeName.GetDynamicText();
            string startdate = Data.Challenge.StartDate.GetDynamicText();
            string endDate = Data.Challenge.EndDate.GetDynamicText();

            Data.GeneratedChallengeName = challengename;
            bool success = addNewChallengePage.FillStepForCustom(challengename, startdate, endDate, Data.Challenge.CustomChallengeActionText, Data.Challenge.CustomChallengeGoal.ToString(), Data.Challenge.CustomChallengeUnit, Data.Challenge.CustomChallengeTime, Data.Challenge.GoalText, out errorMessage);
            if (success)
            {
                Thread.Sleep(2000);
                Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                addNewChallengePage.Step1_NextButton.Click();
            }
            else
            {
                Console.WriteLine(errorMessage);
                Assert.Fail(errorMessage);
            }

            var participantCount = addNewChallengePage.Step2_ParticipantList;
            if (participantCount != null && participantCount.Count > 0)
            {
                Console.WriteLine("Participants available to select");
            }
            else
            {
                Console.WriteLine("No Participants available");
                Assert.Fail("No Participants available");
            }

            foreach (var challenger in Data.Challengers)
            {
                success = addNewChallengePage.AddChallengers(challenger.Name, out errorMessage);
                if (success)
                {
                    Console.WriteLine("Participant {0} added to challenge", challenger.Name);
                }
                else
                {
                    Console.Error.WriteLine("Failed to add {0} challenger. Error: {1}", challenger.Name, errorMessage);
                    Assert.Fail(errorMessage);
                }
            }
            addNewChallengePage.Step2_CreateButton.Click();
            Console.WriteLine("Create Challenge button clicked");


            var wait = GetWebDriverWait();
            var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
            if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
            {
                Console.WriteLine("Challenge {0} added successfully", challengename);
                Assert.IsTrue(true);
            }
            else
            {
                errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                Console.WriteLine(errorMessage);
                Assert.Fail(errorMessage);
            }

        }
        private static WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private static PIV1859Data GetData()
        {
            PIV1859Data data = null;
            try
            {
                var fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1859.json");
                data = JsonConvert.DeserializeObject<PIV1859Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private void LogoutAdministrator()
        {
            Thread.Sleep(4000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private void LoginParticipant(string username, string password)
        {
            Thread.Sleep(3000);
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(username);
            loginPage.PassTextBox.SendKeys(password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }

            Thread.Sleep(3000);
        }

        private void LogoutParticipant()
        {
            Thread.Sleep(3000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
            Thread.Sleep(3000);
        }

    }
}
