﻿
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;
using GoPivot.NetCore.ComponentHelper;
namespace GoPivot.NetCore.TestCases
{

    [TestFixture(TestName = "PIV-1977", Category = "Account")]
    public class PIV1977 : BaseStepDefinition
    {
        [TestCase(TestName = "Validate Biometric Program Settings")]
        public void Validate_Biometric_Program_settings()
        {
            try
            {
                var data = GetData();
                if (data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                var loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
                Thread.Sleep(1000);
                loginPage.LoginButton.Click();

                Thread.Sleep(1000);

                Console.WriteLine("Administrator Logged in successfully");

                var adminPage = new AdminPage(driver);
                if (adminPage.Clients != null && adminPage.Clients.Displayed)
                {
                    adminPage.Clients.Click();
                    Console.WriteLine("Clients link clicked");
                }
                else
                {
                    Console.WriteLine("Clients Link not found");
                    Assert.Fail("Clients link not found");
                }
                Thread.Sleep(2000);
                var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
                if (clientElement != null && clientElement.Displayed)
                {
                    Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                    clientElement.Click();
                }
                else
                {
                    Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                    Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                }


                var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
                if (programElement != null && programElement.Displayed)
                {
                    programElement.Click();
                    Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
                }
                else
                {
                    Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
                }
                Thread.Sleep(2000);
                if (adminPage.ProgramTabs.Count > 0)
                {
                    Console.WriteLine("Program Listing page loaded");

                }
                else
                {
                    Console.WriteLine("Program details page not loaded");
                    Assert.Fail("Failed to load Program details");
                }
                string biometricsElementPath = "//div[contains(@class,'settingLabel') and contains(.,'Biometrics')]/preceding-sibling::md-switch[contains(@class,'md-checked')]";
                try
                {
                    driver.FindElement(By.XPath(biometricsElementPath)).Click();
                    Console.WriteLine("Disabled Biometrics Option from account setting");
                }
                catch
                {
                    // Element already disabled no actions needed.
                    Console.WriteLine("Biometrics option is disabled");
                }
                Thread.Sleep(3000);
                biometricsElementPath = "//div[contains(@class,'settingLabel') and contains(.,'Biometrics')]/preceding-sibling::md-switch";
                driver.FindElement(By.XPath(biometricsElementPath)).Click();
                Thread.Sleep(3000);
                biometricsElementPath = "//div[contains(@class,'settingLabel') and contains(.,'Biometrics')]/preceding-sibling::md-switch[contains(@class,'md-checked')]";
                Assert.IsTrue(driver.FindElement(By.XPath(biometricsElementPath)).Displayed, "Biometrics section must be enabled");
                Console.WriteLine("Biometrics section enabled for participant");


                Assert.IsTrue(driver.FindElement(By.ClassName("modal-dialog")).Displayed, "Biometrics modal popup must be displayed");
                Console.WriteLine("Biometrics Modal popup displayed");

                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                IWebElement instructionsElement = driver.FindElement(By.XPath("//text-angular[@id='instructionsEditor']//div[contains(@id,'taTextElement')]"));
                js.ExecuteScript(string.Format("arguments[0].innerHTML = '<p>{0}</p>'", data.InstructionsText), instructionsElement);
                Thread.Sleep(5000);
                JavaScriptExecutor.ExecuteScript(string.Format("document.getElementsByName('instructionsContent')[0].value = '<p>{0}</p>'", data.InstructionsText));
                Thread.Sleep(5000);
                IWebElement informationElement = driver.FindElement(By.XPath("//text-angular[@id='informationEditor']//div[contains(@id,'taTextElement')]"));
                js.ExecuteScript(string.Format("arguments[0].innerHTML = '<p>{0}</p>'", data.InformationText), informationElement);
                Thread.Sleep(5000);
                JavaScriptExecutor.ExecuteScript(string.Format("document.getElementsByName('informationContent')[0].value = '<p>{0}</p>'", data.InformationText));
                Thread.Sleep(5000);
                driver.FindElement(By.XPath("//input[@value='Save' and @type='button']")).Click();
                Console.WriteLine("Save Button clicked");
                Thread.Sleep(3000);
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1977 Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1977 Failed. Error: {0}", ex);
            }
        }

        public PIV1977Data GetData()
        {
            PIV1977Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1977.json");
                data = JsonConvert.DeserializeObject<PIV1977Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
    }
}
