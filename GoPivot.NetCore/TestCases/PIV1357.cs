﻿using System;
using System.IO;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.DataEntities;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace GoPivot.NetCore.TestCases
{
    [Order(1)]
    [TestFixture(TestName = "PIV-1357", Description = "Something", Category = "Exercise")]
    public class PIV1357 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1357Data Data { get; set; }
        [TestCase(TestName = "Exercise preference data is not present for the participant", Ignore = "Yes", IgnoreReason = "Already set")]
        [Order(1)]
        public void Validate_Exercise_Preference_Data_Not_Present()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                LoginParticipant();
                var exerciseTab = GetWebDriverWait()
               .Until(ExpectedConditions.ElementIsVisible(By.LinkText("Exercise")));
                if (exerciseTab != null && exerciseTab.Displayed)
                {
                    exerciseTab.Click();
                    Console.WriteLine("Exercise Tab Clicked");
                }
                Thread.Sleep(3000);
                driver.FindElement(By.LinkText("Build Your Workout Plan")).Click();
                Console.WriteLine("Built Your Workout Plan clicked");



                Thread.Sleep(2000);

                driver.FindElement(By.XPath("//input[@value='Build Exercise Plan']")).Click();
                Console.WriteLine("Build Exercise Plan clicked");
                Thread.Sleep(2000);

                Assert.IsTrue(driver.FindElement(By.XPath("//form[@name='nutritionForm']")).Displayed, "Form should be displayed");
                Console.WriteLine("Build Exercise Plan form displayed");

                AddNewExercisePlan();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("PIV-1357 Scenario 1 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1357 Scenario 1 Failed. Error: {0}", ex);

            }

        }

        private void AddNewExercisePlan()
        {
            //var heightFeetElement = driver.FindElement(By.Id("heightFeet"));
            //heightFeetElement.Clear();
            //heightFeetElement.SendKeys(Data.BuildExercisePlan.HeightFeet.ToString());

            //var heightInchesElement = driver.FindElement(By.Id("heightInches"));
            //heightInchesElement.Clear();
            //heightInchesElement.SendKeys(Data.BuildExercisePlan.HeightInch.ToString());
            //Console.WriteLine("Height {0}'{1}", Data.BuildExercisePlan.HeightFeet, Data.BuildExercisePlan.HeightInch);
            //var weightElement = driver.FindElement(By.XPath("//input[@ng-model='fitnessPreferences.weight']"));
            //weightElement.Clear();
            //weightElement.SendKeys(Data.BuildExercisePlan.Weight.ToString());
            //Console.WriteLine("Weight: {0}", Data.BuildExercisePlan.Weight);

            var ageElement = driver.FindElement(By.Id("fitnessAge"));
            ageElement.Clear();
            ageElement.SendKeys(Data.BuildExercisePlan.Age.ToString());
            Console.WriteLine("Age: {0}", Data.BuildExercisePlan.Age);

            var exerciseGoalElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'fitnessGoalHeight')]/div[contains(@class,'fitnessDescription') and contains(.,'{0}')]", Data.BuildExercisePlan.ExerciseGoal)));
            exerciseGoalElement.Click();
            Console.WriteLine("Exercise Goal: {0}", Data.BuildExercisePlan.ExerciseGoal);

            string[] daysForExercise = Data.BuildExercisePlan.DaysOfExercise.Split(new char[] { ',' });
            foreach (var day in daysForExercise)
            {
                string elementId = string.Format("{0}Selection", day.ToLower());
                var dayElement = driver.FindElement(By.Id(elementId));
                try
                {
                    var selectedIcon = dayElement.FindElement(By.ClassName("selectedIcon"));
                }
                catch
                {
                    dayElement.Click();
                    Console.WriteLine("{0} Day Clicked for exercise", day);
                    Thread.Sleep(3000);
                }

            }

            var physicalActiveElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'fitnessActiveHeight')]/div[contains(@class,'fitnessDescription') and contains(.,'{0}')]", Data.BuildExercisePlan.PhysicalActive)));
            physicalActiveElement.Click();
            Console.WriteLine("Physical Activeness: {0}", Data.BuildExercisePlan.PhysicalActive);

            var accessToEquipmentElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'fitnessEquipmentHeight')]/div[contains(@class,'fitnessDescription') and contains(.,'{0}')]", Data.BuildExercisePlan.AccessToEquipment)));
            accessToEquipmentElement.Click();
            Console.WriteLine("Access to Equipments: {0}", Data.BuildExercisePlan.AccessToEquipment);


            Thread.Sleep(3000);
            driver.FindElement(By.XPath("//input[@type='button' and @value='Save']")).Click();
            Console.WriteLine("Save button clicked");
            Thread.Sleep(5000);

            var exerciseTab = GetWebDriverWait()
   .Until(ExpectedConditions.ElementIsVisible(By.LinkText("Exercise")));
            if (exerciseTab != null && exerciseTab.Displayed)
            {
                exerciseTab.Click();
                Console.WriteLine("Exercise Tab Clicked");
            }
            Thread.Sleep(3000);
            try
            {
                Assert.IsFalse(driver.FindElement(By.LinkText("Build Your Workout Plan")).Displayed, "Build Your Workout Plan should not be displayed");
            }
            catch
            {
                Console.WriteLine("Build Your Workout plan completed successfully");
            }
        }

        [TestCase(TestName = "Scenario 2: Exercise preference data is present for the participant")]
        [Order(2)]
        public void Validate_Exercise_Preference_Present()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("First Scenario not completed"); return;
                }
                LoginParticipant();
                Thread.Sleep(8000);
                IWebElement exerciseTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Exercise")));
                exerciseTab.Click();
                Console.WriteLine("Exercise Link clicked");
                Thread.Sleep(5000);
                driver.FindElement(By.LinkText("Change Exercise Plan")).Click();
                Console.WriteLine("Change Exercise Plan clicked");
                Thread.Sleep(5000);
                driver.FindElement(By.XPath("//div[contains(@class,'fitnessGoalHeight')][1]")).Click();
                Thread.Sleep(5000);
                Console.WriteLine("First Exercise Goal been set");

                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'ui-notification')]")).Displayed, "Growl Text notification been shown");
                Console.WriteLine("Growl Text notification been shown");
                Console.WriteLine("Exercise data Updated");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.Error.WriteLine("PIV-1357 Scenario 2 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1357 Scenario 2 Failed. Error: {0}", ex);
        
            }
        }

        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.NewParticipant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.NewParticipant.Password);
            loginPage.LoginButton.Click();

            Thread.Sleep(2000);

            var logoutElement =
                GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement != null && logoutElement.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully",
                    ObjectRepository.Setting.Credentials.NewParticipant.Username);
            }
            else
            {
                Assert.Fail("Participant Login Failed");
                return;
            }
        }

        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private PIV1357Data GetData()
        {
            PIV1357Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1357.json");
                data = JsonConvert.DeserializeObject<PIV1357Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
    }
}
