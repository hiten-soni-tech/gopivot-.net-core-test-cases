﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.DataEntities;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Linq;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1569", Category = "Promotion")]
    public class PIV1569 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1569Data Data { get; set; }
        [Order(1)]
        [TestCase(TestName = "Scenario 1: Add Item Form Elements")]
        public void Validate_Add_Item_Form_Elements()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAdministrator();
                Thread.Sleep(3000);

                driver.FindElement(By.XPath("//button[contains(.,'Add Item')]")).Click();
                Console.WriteLine("Add Item button clicked");

                IWebElement headerElement = driver.FindElement(By.XPath("//h2[contains(@class,'inline')]/span/b"));
                Assert.IsTrue(headerElement.Text == "Add Item", "Header must be valid");
                Console.WriteLine("Header validated '{0}'", headerElement.Text);

                Assert.IsTrue(driver.FindElement(By.XPath("//a[@class='pointer'  and contains(.,'Back to Item List')]")).Displayed, "Back to Item List link must be displayed");
                Console.WriteLine("Back to Item List link displayed");

                IWebElement promotionNameElement = driver.FindElement(By.XPath("//input[@ng-model='newPromotion.name']"));
                IWebElement promotionDescriptionElement = driver.FindElement(By.XPath("//textarea[@ng-model='newPromotion.description']"));
                IWebElement imageTextElement = driver.FindElement(By.XPath("//input[@ng-model='newPromotion.textImg']"));
                IWebElement pointValueElement = driver.FindElement(By.XPath("//input[@ng-model='newPromotion.pointValue']"));
                IWebElement saveButtonElement = driver.FindElement(By.XPath("//button[contains(.,'Add')]"));
                IWebElement cancelButtonElement = driver.FindElement(By.XPath("//button[contains(.,'Cancel')]"));
                Assert.IsTrue(promotionNameElement.Displayed, "Promotion Name element must be displayed");
                Assert.IsTrue(promotionDescriptionElement.Displayed, "Promotion Description element must be displayed");
                Assert.IsTrue(imageTextElement.Displayed, "Promotion Image upload elment must be displayed");
                Assert.IsTrue(pointValueElement.Displayed, "Promotion points element must be displayed");
                Console.WriteLine("All Promotion element present while adding new promotion item");
                Assert.IsTrue(saveButtonElement.Displayed, "Promotion Add button element must be displayed");
                Console.WriteLine("Add button available");
                Assert.IsTrue(cancelButtonElement.Displayed, "Promotion Cancel button element must be displayed");
                Console.WriteLine("Cancel button available");
                IsSuccess = true;

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1569 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1569 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [Order(2)]
        [TestCase(TestName = "Scenario 2: Upload Item image")]
        public void Validate_Upload_Item_Image()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {

                IWebElement imageUploadElement = driver.FindElement(By.Id("promoteImageFile"));
                string fileName = TestContext.CurrentContext.TestDirectory + "\\files\\" + Data.ImageName;
                imageUploadElement.SendKeys(fileName);
                Thread.Sleep(5000);

                IWebElement imagePreviewElement = driver.FindElement(By.XPath("//div[@id='preview']/img"));
                Assert.IsTrue(imagePreviewElement.GetAttribute("src").Contains("data:image/png"), "Uploaded image must be previewed");
                Console.WriteLine("Image Preview is visible");

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1569 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1569 Scenario 2 Failed. Error: {0}", ex);
            }
        }

        [Order(3)]
        [TestCase(TestName = "Scenario 3: Upload image error messaging")]
        public void Validate_Upload_Item_Error_Messaging()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            Assert.Pass("Feature not tested because file uploading limit not decided");
        }
        [Order(4)]
        [TestCase(TestName = "Scenario 4: Add Item > Cancel (No unsaved changes)")]
        public void Validate_Add_Item_Cancel_No_Unsaved_Changes()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 3 not completed"); return;
            }
            try
            {
                driver.FindElement(By.XPath("//a[@class='pointer'  and contains(.,'Back to Item List')]")).Click();
                Console.WriteLine("Back to Item List clicked");
                Thread.Sleep(3000);

                driver.FindElement(By.XPath("//button[contains(.,'Add Item')]")).Click();
                Console.WriteLine("Add Item button clicked");
                Thread.Sleep(2000);
                IWebElement cancelButtonElement = driver.FindElement(By.XPath("//button[contains(.,'Cancel')]"));
                cancelButtonElement.Click();
                Console.WriteLine("Cancel button clicked");
                Thread.Sleep(3000);

                Assert.IsTrue(driver.FindElement(By.XPath("//button[contains(.,'Add Item')]")).Displayed, "Add Item button must be displayed after cancellation");
                Console.WriteLine("Add Item button displayed again which means cancel worked successfully without modal popup");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1569 Scenario 4 Error: {0}", ex.Message);
                Assert.Fail("PIV-1569 Scenario 4 Failed. Error: {0}", ex);
            }
        }

        [Order(5)]
        [TestCase(TestName = "Scenario 5: Add Item > Cancel (Unsaved changes)")]
        public void Validate_Add_Item_Cancel_Unsaved_Changes()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 4 not completed"); return;
            }
            try
            {
                driver.FindElement(By.XPath("//button[contains(.,'Add Item')]")).Click();
                Console.WriteLine("Add Item button clicked");
                IWebElement promotionNameElement = driver.FindElement(By.XPath("//input[@ng-model='newPromotion.name']"));
                string promotionName = Data.Name.GetDynamicText();
                promotionNameElement.SendKeys(promotionName);
                Thread.Sleep(2000);
                IWebElement cancelButtonElement = driver.FindElement(By.XPath("//button[contains(.,'Cancel')]"));
                cancelButtonElement.Click();
                Console.WriteLine("Cancel button clicked");
                Thread.Sleep(3000);
                Assert.IsTrue(driver.FindElement(By.XPath("//button[contains(.,'Leave without saving')]")).Displayed, "Leave without button must be visible");
                Console.WriteLine("Button - Leave without saving displayed");
                Assert.IsTrue(driver.FindElement(By.XPath("//button[contains(.,'Stay on page')]")).Displayed, "Stay on page button must be visible");
                Console.WriteLine("Button - Stay on page displayed");

                driver.FindElement(By.XPath("//button[contains(.,'Stay on page')]")).Click();
                Console.WriteLine("Stay on page button clicked");


            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1569 Scenario 5 Error: {0}", ex.Message);
                Assert.Fail("PIV-1569 Scenario 5 Failed. Error: {0}", ex);
            }
        }
        [Order(6)]
        [TestCase(TestName = "Scenario 6: Add Item > Add")]
        public void Validate_Add_Item_Add()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 5 not completed"); return;
            }
            try
            {
                IWebElement promotionNameElement = driver.FindElement(By.XPath("//input[@ng-model='newPromotion.name']"));
                IWebElement promotionDescriptionElement = driver.FindElement(By.XPath("//textarea[@ng-model='newPromotion.description']"));
                IWebElement pointValueElement = driver.FindElement(By.XPath("//input[@ng-model='newPromotion.pointValue']"));
                IWebElement saveButtonElement = driver.FindElement(By.XPath("//button[contains(.,'Add')]"));
                string promotionName = Data.Name.GetDynamicText();
                promotionNameElement.Clear();
                promotionNameElement.SendKeys(promotionName);
                promotionDescriptionElement.SendKeys(Data.Description.GetDynamicText());
                IWebElement imageUploadElement = driver.FindElement(By.Id("promoteImageFile"));
                string fileName = TestContext.CurrentContext.TestDirectory + "\\files\\" + Data.ImageName;
                imageUploadElement.SendKeys(fileName);
                pointValueElement.Clear();
                pointValueElement.SendKeys(Data.Points.ToString());
                Thread.Sleep(5000);

                saveButtonElement.Click();
                Thread.Sleep(4000);

                IWebElement firstPromotionElement = driver.FindElement(By.XPath("//div[@ng-controller='programPromotions']/table/tbody/tr[1]/td[3]"));
                Assert.IsTrue(firstPromotionElement.Text.Equals(promotionName), "Promotion Name must be {0}", promotionName);
                Console.WriteLine("Promotion '{0}' added successfully", promotionName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1569 Scenario 6 Error: {0}", ex.Message);
                Assert.Fail("PIV-1569 Scenario 6 Failed. Error: {0}", ex);
            }
        }


        private void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");
            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }

            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            if (!adminPage.EditClient.Displayed)
            {
                Console.WriteLine("Clients Details not found");
                Assert.Fail("Clients Details not found");

            }
            else
            {
                Console.WriteLine("Client Details page loaded");
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);

            var promotedItemsTabElement = adminPage.GetProgramTab("Promoted Items");
            if (promotedItemsTabElement == null)
            {
                Console.WriteLine("Unable to find Promoted Items tab");
                Assert.Fail("Unable to find Promoted Items tab");
            }
            else
            {
                Console.WriteLine("Promoted Items Tab loaded");
                promotedItemsTabElement.Click();
            }

            Thread.Sleep(5000);
        }

        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private static PIV1569Data GetData()
        {
            PIV1569Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1569.json");
                data = JsonConvert.DeserializeObject<PIV1569Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
    }
}
