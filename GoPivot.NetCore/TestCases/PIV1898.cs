﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.DataEntities;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Linq;
using GoPivot.NetCore.ComponentHelper;


namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1898", Category = "WhiteLabel")]
    public class PIV1898 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        [Order(1)]
        [TestCase(TestName = "Scenario 1: Display Remove action trigger for uploaded assets")]
        public void Validate_Display_Remove_Action_For_Uploaded_Assets()
        {
            try
            {
                LoginAdministrator();

                #region Program Guide
                bool programGuideFound = false;
                try
                {
                    programGuideFound = driver.FindElement(By.XPath("//div[@ng-show='program.programGuidePDF']//a")).Displayed;

                }
                catch
                {
                    Console.WriteLine("No Program Guide Found");
                }

                if (programGuideFound)
                {
                    Console.WriteLine("Program guide file available");
                    Assert.IsTrue(driver.FindElement(By.XPath("//a[@ng-if='program.programGuidePDF']")).Displayed, "Remove button for Program Guid PDF must be displayed");
                    Console.WriteLine("Delete link for Program Guide PDF available");
                }
                else
                {
                    Console.WriteLine("Program file not available");
                }
                #endregion

                #region User Guide
                bool userGuideFound = false;
                try
                {
                    userGuideFound = driver.FindElement(By.XPath("//div[@ng-show='program.userGuidePDF']//a")).Displayed;

                }
                catch
                {
                    Console.WriteLine("No user guide Found");
                }

                if (userGuideFound)
                {
                    Console.WriteLine("user guide file available");
                    Assert.IsTrue(driver.FindElement(By.XPath("//a[@ng-if='program.userGuidePDF']")).Displayed, "Remove button for user guide file must be displayed");
                    Console.WriteLine("Delete link for user Guide PDF available");
                }
                else
                {
                    Console.WriteLine("User Guide file not available");
                }
                #endregion

                #region Custom Logo
                bool logoFound = false;
                try
                {
                    logoFound = driver.FindElement(By.XPath("//div[@ng-show='program.customLogoName']//a")).Displayed;

                }
                catch
                {
                    Console.WriteLine("No logo Found");
                }

                if (logoFound)
                {
                    Console.WriteLine("logo file available");
                    Assert.IsTrue(driver.FindElement(By.XPath("//a[@ng-if='program.customLogoName']")).Displayed, "Remove button for logo file must be displayed");
                    Console.WriteLine("Delete link for logo file available");
                }
                else
                {
                    Console.WriteLine("Logo file not available");
                }
                #endregion

                #region Favicon
                bool faviconFound = false;
                try
                {
                    faviconFound = driver.FindElement(By.XPath("//div[@ng-show='program.favicon']//a")).Displayed;

                }
                catch
                {
                    Console.WriteLine("No favicon Found");
                }

                if (faviconFound)
                {
                    Console.WriteLine("favicon file available");
                    Assert.IsTrue(driver.FindElement(By.XPath("//a[@ng-if='program.favicon']")).Displayed, "Remove button for favicon file must be displayed");
                    Console.WriteLine("Delete link for favicon file available");
                }
                else
                {
                    Console.WriteLine("Favicon file not available");
                }
                #endregion

                #region Custom CSS
                bool customCssFound = false;
                try
                {
                    customCssFound = driver.FindElement(By.XPath("//div[@ng-show='program.customCssName']//a")).Displayed;

                }
                catch
                {
                    Console.WriteLine("No css Found");
                }

                if (customCssFound)
                {
                    Console.WriteLine("css file available");
                    Assert.IsTrue(driver.FindElement(By.XPath("//a[@ng-if='program.customCssName']")).Displayed, "Remove button for css file must be displayed");
                    Console.WriteLine("Delete link for css file available");
                }
                else
                {
                    Console.WriteLine("CSS file not available");
                }
                #endregion

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1898 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1898 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [Order(2)]
        [TestCase(TestName = "Scenario 2: Delete asset")]
        public void Validate_Delete_Asset()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                Console.WriteLine("Scenario 2 case will try to remove only favicon for the system");

                #region Favicon
                bool faviconFound = false;
                try
                {
                    faviconFound = driver.FindElement(By.XPath("//div[@ng-show='program.favicon']//a")).Displayed;

                }
                catch
                {
                    Console.WriteLine("No favicon Found");
                }

                if (faviconFound)
                {
                    IWebElement removeFaviconElement = driver.FindElement(By.XPath("//a[contains(@class,'removeButton') and @ng-if='program.favicon']"));
                    removeFaviconElement.Click();
                    Console.WriteLine("Favion Delete button clicked");
                    Thread.Sleep(3000);
                    IWebElement deleteMessageElement = driver.FindElement(By.XPath("//alert-label[@show-success='faviconSuccess']//div[contains(@class,'alert-success')]"));
                    Assert.IsTrue(deleteMessageElement.Text.Contains("success"), "Favicon Removal success message should be appreared");
                    Console.WriteLine("Favicon deleted successfully");
                }
                else
                {
                    Console.WriteLine("Cannot perform any delete action. Favicon file not available.");
                }
                #endregion
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1898 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1898 Scenario 2 Failed. Error: {0}", ex);
            }

        }
        private void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");
            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }

            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            if (!adminPage.EditClient.Displayed)
            {
                Console.WriteLine("Clients Details not found");
                Assert.Fail("Clients Details not found");

            }
            else
            {
                Console.WriteLine("Client Details page loaded");
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            Thread.Sleep(2000);
            driver.FindElement(By.XPath("//div[contains(@class,'settingLabel') and contains(.,'White Label')]")).Click();
            Console.WriteLine("White Label section clicked");
            Thread.Sleep(3000);
            Assert.IsTrue(driver.FindElement(By.ClassName("modal-dialog")).Displayed, "White label modal popup must be displayed");
            Console.WriteLine("White label Modal popup displayed");

            Thread.Sleep(2000);
        }

        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }


    }
}
