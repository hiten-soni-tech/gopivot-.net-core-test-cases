﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;


namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1947", Category = "Activity")]
    public class PIV1947 : BaseStepDefinition
    {
        public PIV1947Data Data { get; set; }
        [TestCase(TestName = "Ensure activities only award points within activities's valid date range")]
        public void Validate_Award_Points_Within_Activity_Valid_Date_Range()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAndSelectProgram();
                UpdateProgramDates();
                AddNewActivity();
                LoginParticipant();
                AddTransactionToActivity();

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1947 Error: {0}", ex.Message);
                Assert.Fail("PIV-1947 Failed. Error: {0}", ex);
            }
        }



        #region Utilities


        private void AddTransactionToActivity()
        {
            Thread.Sleep(5000);

            string pointsText = driver.FindElement(By.XPath("//li[@class='points']/a")).Text;
            pointsText = pointsText.Replace("pts", "").Trim();
            pointsText = pointsText.Replace(",", "").Trim();
            int.TryParse(pointsText, out int points);
            Console.WriteLine("Current Points are: {0}", points);

            string activityName = string.Empty;
            if (Data?.NewActivity != null)
            {
                activityName = Data.NewActivity.Name;
            }
            //activityName = "PIV-1947 Custom Activity - 653";
            IWebElement activityTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Activities")));
            activityTab.Click();
            Console.WriteLine("Activities Link clicked");
            Thread.Sleep(7000);
            string activityLinkXPath =
                string.Format(
                    "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
                    activityName);
            IWebElement activityElement = driver.FindElement(By.XPath(activityLinkXPath));
            Assert.IsTrue(activityElement.Displayed, string.Format("Activity {0} must be visible in activity list", activityName));
            Console.WriteLine("Activity {0} available in activity list", activityName);
            activityElement.Click();
            Thread.Sleep(2000);

            var dateElement = driver.FindElement(By.CssSelector(".md-datepicker-input"));
            dateElement.Clear();
            dateElement.SendKeys(DateTime.Now.AddDays(-1).ToShortDateString());
            Thread.Sleep(3000);
            var milesActivityElement = driver.FindElement(By.Name("activityDecimal"));
            milesActivityElement.Clear();
            milesActivityElement.SendKeys("100");
            Thread.Sleep(1000);
            var button = driver.FindElement(By.XPath("//button[contains(.,'Save')]"));
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].click()", button);
            Console.WriteLine("{0} Goal added for date {1}", 100, DateTime.Now.AddDays(-1).ToShortDateString());
            Thread.Sleep(3000);
            Assert.IsTrue(driver.FindElement(By.XPath("//i[contains(@class,'icon-check')]")).Displayed, "Success message should be displaed");
            Console.WriteLine("Miles logged successfully");

            pointsText = driver.FindElement(By.XPath("//li[@class='points']/a")).Text;
            pointsText = pointsText.Replace("pts", "").Trim();
            pointsText = pointsText.Replace(",", "").Trim();
            int.TryParse(pointsText, out int newPoints);

            Assert.IsTrue(points == newPoints, "Points must NOT be updated");
            Console.WriteLine("Points not updated");

        }
        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            Thread.Sleep(5000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private void LogoutParticipant()
        {
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private void LogoutAdministrator()
        {
            Thread.Sleep(5000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static void LoginAndSelectProgram()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(3000);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }

            Thread.Sleep(3000);
            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(3000);



        }

        private void UpdateProgramDates()
        {
            driver.FindElement(By.LinkText("Edit")).Click();
            Console.WriteLine("Edit button clicked");

            Thread.Sleep(10000);
            GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.Id("programDatePicker"))).Click();
            Console.WriteLine("Datepicker clicked");

            Thread.Sleep(2000);
            var startDateElement = driver.FindElement(By.Name("daterangepicker_start"));
            startDateElement.Clear();
            string startDateText = "{startofmonth}".GetDynamicText();
            startDateElement.SendKeys(startDateText);

            var endDateElement = driver.FindElement(By.Name("daterangepicker_end"));
            endDateElement.Clear();
            string endDayText = "{endofnextmonth}".GetDynamicText();
            endDateElement.SendKeys(endDayText);


            driver.FindElement(By.XPath("//button[contains(.,'Apply')]")).Click();
            Thread.Sleep(3000);

            driver.FindElement(By.XPath("//button[contains(.,'Save')]")).Click();
            Thread.Sleep(3000);

            Console.WriteLine("Program Dates been updated to Start from {0} to {1}", startDateText, endDayText);

        }

        private void AddNewActivity()
        {
            AdminPage adminPage = new AdminPage(driver);
            Thread.Sleep(1000);
            var activitiesTab = adminPage.GetProgramTab("Activities");
            if (activitiesTab == null)
            {
                Console.WriteLine("Unable to find activities tab");
                Assert.Fail("Unable to find activities tab");
            }
            else
            {
                Console.WriteLine("Activities Tab loaded");
                activitiesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(7000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

            driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
            Console.WriteLine("Add Activity button clicked");

            Thread.Sleep(5000);
            Data.NewActivity = new AdminPage(driver).CreateNewActivity(Data.NewActivity, errorMessage: out string errorMessage);
            if (Data.NewActivity == null)
            {
                Assert.Fail(errorMessage);
            }
            Console.WriteLine("Activity created successfully. Name: {0}", Data.NewActivity.Name);

            LogoutAdministrator();
            Thread.Sleep(1000);

        }

        private PIV1947Data GetData()
        {
            PIV1947Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1947.json");
                data = JsonConvert.DeserializeObject<PIV1947Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
