﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1613", Category = "Exercise")]
    public class PIV1613 : BaseStepDefinition
    {
        [TestCase(Description = "PIV-1613 Replace FitIn video image placeholder with the embedded video code", TestName = "Replace FitIn video image placeholder with the embedded video code")]
        public void Validate_Fit_In_Video()
        {
            try
            {
                LoginParticipant();
                Thread.Sleep(8000);
                IWebElement exerciseTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Exercise")));
                exerciseTab.Click();
                Console.WriteLine("Exercise Link clicked");
                Thread.Sleep(3000);

                driver.FindElement(By.LinkText("Take a FitIn")).Click();
                Console.WriteLine("Take a fit in button clicked");
                Thread.Sleep(2000);

                IWebElement webElement = driver.FindElement(By.XPath("//div[@class='activityVideo']/iFrame"));
                string iFrameSource = webElement.GetAttribute("src");
                Console.WriteLine("IFrame Source is: {0}", iFrameSource);
                Assert.IsTrue(iFrameSource.Contains("youtube"),"Youtube video must be loaded");
                webElement.Click();

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1613 Error: {0}", ex.Message);
                Assert.Fail("PIV-1613 Failed. Error: {0}", ex);
            }
      
        }
        #region Utilities

        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }


        private void LogoutParticipant()
        {
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

       

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
