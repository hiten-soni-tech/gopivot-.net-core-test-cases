﻿
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1704", Category = "Challenge")]
    public class PIV1704 : BaseStepDefinition
    {
        public string GeneratedChallengeName { get; set; }
        public bool IsSuccess { get; set; }
        [TestCase(Description = "Create new Custom One time Challenge having future date to start", TestName = "Scenario 1: Create Custom Challenge with Future Date for Everyone")]
        public void CreateNewCustomChallenge()
        {
            try
            {
                var data = GetData();
                if (data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                var loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
                loginPage.LoginButton.Click();

                Thread.Sleep(1000);

                Console.WriteLine("Administrator Logged in successfully");

                var adminPage = new AdminPage(driver);
                if (adminPage.Clients != null && adminPage.Clients.Displayed)
                {
                    adminPage.Clients.Click();
                    Console.WriteLine("Clients link clicked");
                }
                else
                {
                    Console.WriteLine("Clients Link not found");
                    Assert.Fail("Clients link not found");
                }

                var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
                if (clientElement != null && clientElement.Displayed)
                {
                    Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                    clientElement.Click();
                }
                else
                {
                    Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                    Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                }

                if (!adminPage.EditClient.Displayed)
                {
                    Console.WriteLine("Clients Details not found");
                    Assert.Fail("Clients Details not found");

                }
                else
                {
                    Console.WriteLine("Client Details page loaded");
                }

                var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
                if (programElement != null && programElement.Displayed)
                {
                    programElement.Click();
                    Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
                }
                else
                {
                    Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
                }

                if (adminPage.ProgramTabs.Count > 0)
                {
                    Console.WriteLine("Program Listing page loaded");

                }
                else
                {
                    Console.WriteLine("Program details page not loaded");
                    Assert.Fail("Failed to load Program details");
                }

                Thread.Sleep(1000);

                var challengeTab = adminPage.GetProgramTab("Challenges");
                if (challengeTab == null)
                {
                    Console.WriteLine("Unable to find challenges tab");
                    Assert.Fail("Unable to find challenges tab");
                }
                else
                {
                    Console.WriteLine("Challenge Tab loaded");
                    challengeTab.Click();
                }

                Thread.Sleep(5000);

                adminPage.GetAddChallengeButton();
                if (adminPage.AddChallengeButton != null)
                {
                    adminPage.AddChallengeButton.Click();
                    Console.WriteLine("Add Challenge Button clicked");
                    Thread.Sleep(2000);
                }
                else
                {
                    Console.WriteLine("Cannot add new challenge");
                    Assert.Fail("cannot add new challenge");
                }

                Thread.Sleep(2000);

                var challengeCategory = adminPage.GetChallengeCategory("Custom");
                if (challengeCategory != null)
                {
                    challengeCategory.Click();
                    Console.WriteLine("Custom Challenge type selected");
                }
                else
                {
                    Console.WriteLine("Custom Challenge Type not available");
                    Assert.Fail("Custom Challenge Type not available");
                }

                Thread.Sleep(2000);
                var challengeType = adminPage.GetChallengeType("Custom");
                string errorMessage;
                if (challengeType != null)
                {
                    challengeType.Click();
                    Console.WriteLine("Custom Challenge selected");
                }
                else
                {
                    errorMessage = "Custom Challenge not found";
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }


                AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);

                string randomNumber = new Random().Next(1000).ToString();
                var challengename = data.Challenge.ChallengeName;
                string startdate = data.Challenge.StartDate;
                challengename = challengename.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
                challengename = challengename.Replace("{currentday}", DateTime.Now.ToShortDateString());
                challengename = challengename.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
                challengename = challengename.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());
                challengename = challengename.Replace("{randomnumber}", randomNumber);
                GeneratedChallengeName = challengename;

                //     TestContext. .Add("ChallengeRandomNumber", randomNumber);
                startdate = startdate.Replace("{currentdate}", DateTime.Now.ToShortDateString());
                startdate = startdate.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
                startdate = startdate.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
                startdate = startdate.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());

                DateTime reference = DateTime.Now;
                DateTime firstDayThisMonth = new DateTime(reference.Year, reference.Month, 1);
                DateTime firstDayPlusTwoMonths = firstDayThisMonth.AddMonths(2);
                DateTime endOfLastDayNextMonth = firstDayPlusTwoMonths.AddTicks(-1);
                string enddate = data.Challenge.EndDate.Replace("{endofnextmonth}", endOfLastDayNextMonth.ToShortDateString());

                bool success = addNewChallengePage.FillStepForCustom(challengename, startdate, enddate, data.Challenge.CustomChallengeActionText, data.Challenge.CustomChallengeGoal.ToString(), data.Challenge.CustomChallengeUnit, data.Challenge.CustomChallengeTime, data.Challenge.GoalText, out errorMessage);
                if (success)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                    addNewChallengePage.Step1_NextButton.Click();
                }
                else
                {
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                var participantCount = addNewChallengePage.Step2_ParticipantList;
                if (participantCount != null && participantCount.Count > 0)
                {
                    Console.WriteLine("Participants available to select");
                }
                else
                {
                    Console.WriteLine("No Participants available");
                    Assert.Fail("No Participants available");
                }

                addNewChallengePage.Step2_EveryOne.Click();
                Thread.Sleep(3000);
                addNewChallengePage.Step2_CreateButton.Click();
                Console.WriteLine("Create Challenge button clicked");


                var wait = GetWebDriverWait();
                var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
                if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
                {
                    Console.WriteLine("Challenge {0} added successfully", challengename);
                    Assert.IsTrue(true);
                }
                else
                {
                    errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("Creating Custom Challenge Failed. Error: {0}", ex);

            }
        }

        [TestCase(Description = "Participant Tab Details", TestName = "Scenario 2: Participant Tab")]
        public void ParticipantTab()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                if (string.IsNullOrEmpty(GeneratedChallengeName))
                {
                    Console.Error.WriteLine("Challenge not available");
                    Assert.Fail("Challenge not available");
                    return;
                }

                var data = GetData();
                if (data == null)
                {
                    Console.Error.WriteLine("Data not available");
                    Assert.Fail("Data not available");
                    return;
                }
                Thread.Sleep(3000);
                var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
                if (dropdownMenuElement.Displayed)
                {
                    dropdownMenuElement.Click();

                    var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                    if (logoutElement != null && logoutElement.Displayed)
                    {
                        logoutElement.Click();
                        Console.WriteLine("Administrator logged off successfully");
                    }
                    else
                    {
                        Console.Error.WriteLine("Logout link not clickable");
                        Assert.Fail("Logout link not clickable");
                    }

                }
                else
                {
                    Console.Error.WriteLine("Logout Dropdown not visible");
                    Assert.Fail("Logout Dropdown not visible");
                }


                By loadingImage = By.TagName("http-busy");
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                var loginPage = new LoginPage(driver);
                Console.WriteLine("Login Page loaded successfully");
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
                Thread.Sleep(2000);
                loginPage.LoginButton.Click();
                Thread.Sleep(1000);
                var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement1 != null && logoutElement1.Displayed)
                {
                    Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);

                }
                else
                {
                    Console.Error.WriteLine("Login Failed. Failed to load participant view");
                    Assert.Fail("Login Failed. Failed to load participant view");
                }

                IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
                if (challengeTab != null && challengeTab.Displayed)
                {
                    challengeTab.Click();
                    Console.WriteLine("Challenge Tab Clicked");
                }

                Thread.Sleep(1000);
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                var challengeList = driver.FindElements(By.XPath("//div[@class='panel challenge ng-scope']"));
                if (challengeList != null && challengeList.Count > 0)
                {
                    Console.WriteLine("{0} Challenges available for participant", challengeList.Count);
                }
                else
                {
                    Console.Error.WriteLine("Failed to load challenges");
                    Assert.Fail("Failed to load challenges");
                }

                var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", GeneratedChallengeName))));
                if (challenge != null && challenge.Displayed)
                {
                    challenge.Click();
                    Console.WriteLine("{0} available challenge found", GeneratedChallengeName);
                }
                else
                {
                    Console.Error.WriteLine("{0} available challenge not found", GeneratedChallengeName);
                    Assert.Fail("{0} available challenge not found", GeneratedChallengeName);
                }

                IWebElement participantTabElement = driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Participants')]"));
                if (participantTabElement != null && participantTabElement.Displayed)
                {
                    Console.WriteLine("Participant tab is visible");
                    Assert.IsTrue(participantTabElement.Displayed);
                }
                else
                {
                    Console.Error.WriteLine("Cannot find participants tab");
                    Assert.Fail("Cannot find participants tab");
                }
                Thread.Sleep(1000);
                IWebElement detailsTabElement = driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Details')]"));
                if (detailsTabElement != null && detailsTabElement.Displayed)
                {
                    detailsTabElement.Click();
                    Thread.Sleep(1000);
                    try
                    {
                        IWebElement joinButtonElement = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(.,'Join!')]")));
                        if (joinButtonElement != null && joinButtonElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));
                            joinButtonElement.Click();
                            Console.WriteLine("Join Button available and clicked to join this challenge");
                        }
                        else
                        {
                            Console.WriteLine("Join button not available or already joined");
                        }
                    }
                    catch
                    {
                        IWebElement joinedTextElement = driver.FindElement(By.ClassName("challengeJoinedText"));
                        if (joinedTextElement != null && joinedTextElement.Displayed)
                        {
                            Console.WriteLine("Participant already joined");
                        }
                    }

                }
                else
                {
                    Console.Error.WriteLine("Cannot find details tab");
                    Assert.Fail("Cannot find details tab");
                }

                if (participantTabElement.Displayed)
                {
                    participantTabElement.Click();
                    var challengerItemElement = driver.FindElement(By.XPath(string.Format("//div[@class='user-detail']/span[contains(.,'{0}')]", data.Challenge.ChallengerName)));
                    if (challengerItemElement != null && challengerItemElement.Displayed)
                    {

                        Console.Write("{0} Challenger available in list", data.Challenge.ChallengerName);
                        Assert.IsTrue(challengerItemElement.Displayed);
                    }
                    else
                    {
                        Console.Error.WriteLine("{0} Challenger NOT available in list", data.Challenge.ChallengerName);
                        Assert.Fail("Failed to retrieve participant {0}", data.Challenge.ChallengerName);
                    }
                }
                else
                {
                    Console.Error.WriteLine("Cannot find participants tab");
                    Assert.Fail("Cannot find participants tab");
                }

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Failed to view participant tab. Error: {0}", ex.Message);
                Assert.Fail("Failed to view participant tab. Error: {0}", ex);
            }

        }

        private static WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private static PIV1704Data GetData()
        {
            PIV1704Data data = null;
            try
            {
                var fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1704.json");
                data = JsonConvert.DeserializeObject<PIV1704Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
    }
}
