﻿
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-33", Category = "Nutrition")]
    public class PIV33 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        private LoginPage loginPage;
        private bool loggedIn = false;
        [TestCase(Description = "PIV 33 -  Display 7 days at the top of the Nutrition page for the user to select for recipes", TestName = "Display 7 Days on Nutrition Page")]
        [Order(1)]
        public void VerifyWeekLinks()
        {
            string errorMessage = string.Empty;
            try
            {

                loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
                loginPage.LoginButton.Click();

                Thread.Sleep(2000);

                var logoutElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
                    loggedIn = true;
                }
                else
                {
                    Assert.Fail("Participant Login Failed");
                    return;
                }

                IWebElement nutritionTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }
                Thread.Sleep(1000);

                BuildRecipePlanIfNotExists();

                string[] dayofWeeks = Enum.GetNames(typeof(DayOfWeek));
                int daysFound = 0;
                foreach (string day in dayofWeeks)
                {
                    var dayElement = driver.FindElement(By.XPath(string.Format("//ul[contains(@class,'nutritionTabs')]/li/a[contains(.,'{0}')]", day)));
                    if (dayElement != null && dayElement.Displayed)
                    {
                        Console.WriteLine("{0} Link available", day);
                        daysFound++;
                    }
                    else
                    {
                        Console.Error.WriteLine("{0} Link NOT available", day);
                    }
                }
                Assert.IsTrue(daysFound == dayofWeeks.Length, "All Days are found in Nutrition Tab");
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.Error.WriteLine("PIV-33 Scenario-1 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-33 Scenario-1 Failed. Error: {0}", ex);

            }
        }


        [TestCase(Description = "PIV 33 -  Display a section for Daily Totals on each day selected", TestName = "Display goals on each day selected")]
        [Order(2)]
        public void VerifyGoalLabels()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            string errorMessage = string.Empty;
            try
            {
                if (!loggedIn)
                {
                    Assert.Fail("PIV-33 Scenario 1 Test Case Failed");
                }



                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'nutritionTotalsSection')]/div[contains(@class,'nutritionHeader') and contains(.,'Calorie Goal')]")).Displayed, "Calorie Goal Label Displayed");
                Console.WriteLine("Calorie Goal Label Available");
                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'nutritionTotalsSection')]/div[contains(@class,'nutritionHeader') and contains(.,'Recipes Total')]")).Displayed, "Recipe Total Label Displayed");
                Console.WriteLine("Recipes Total Label Available");
                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'nutritionTotalsSection')]/div[contains(@class,'nutritionHeader') and contains(.,'Difference')]")).Displayed, "Difference Label Displayed");
                Console.WriteLine("Difference Label Available");
                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'nutritionTotalsSection')]/div[contains(@class,'nutritionHeader') and contains(.,'Carbs')]")).Displayed, "Carbs Label Displayed");
                Console.WriteLine("Carbs Label Available");
                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'nutritionTotalsSection')]/div[contains(@class,'nutritionHeader') and contains(.,'Protein')]")).Displayed, "Protein Label Displayed");
                Console.WriteLine("Protein Label Available");
                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'nutritionTotalsSection')]/div[contains(@class,'nutritionHeader') and contains(.,'Fat')]")).Displayed, "Fat Label Displayed");
                Console.WriteLine("Fat Label Available");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.Error.WriteLine("PIV-33 Scenario-2 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-33 Scenario-2 Failed. Error: {0}", ex);

            }
        }

        [TestCase(Description = "PIV 33 -  Display a container for three meals on each day selected", TestName = "Display Meals Container")]
        [Order(3)]
        public void VerifyMealsContainer()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            string errorMessage = string.Empty;
            try
            {
                if (!loggedIn)
                {
                    Assert.Fail("PIV-33 Scenario 2 Test Case Failed or not executed");
                }

                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Breakfast')]")).Displayed, "Breakfast Container Displayed");
                Console.WriteLine("Breakfast container available");
                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Lunch')]")).Displayed, "Lunch Container Displayed");
                Console.WriteLine("Lunch container available");
                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Dinner')]")).Displayed, "Dinner Container Displayed");
                Console.WriteLine("Dinner container available");

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.Error.WriteLine("PIV-33 Scenario-2 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-33 Scenario-2 Failed. Error: {0}", ex);

            }
        }


        private void BuildRecipePlanIfNotExists()
        {
            try
            {
                driver.FindElement(By.LinkText("Build Your Recipe Plan")).Click();
                Console.WriteLine("Build Your Recipe Plan clicked");

                driver.FindElement(By.XPath("//input[@ng-model='model.desiredWeight']")).SendKeys("100");
                driver.FindElement(By.XPath("//input[@type='button' and @value = 'Save']")).Click();
                Console.WriteLine("Save Button clicked");

                Thread.Sleep(3000);

                var nutritionTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }
                Thread.Sleep(3000);
            }
            catch
            {
                Console.WriteLine("Button: Build Your Recipe Plan not found");
                Console.WriteLine("Recipe Plan already available for this user");
                // Do nothing
            }
        }

        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}
