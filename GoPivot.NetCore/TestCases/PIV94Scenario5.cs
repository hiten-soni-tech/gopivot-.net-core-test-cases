﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-94 Scenario 5", Category = "Challenge")]
    public class PIV94Scenario5 : BaseStepDefinition
    {
        private LoginPage loginPage;
        private string GeneratedChallengeName;
        public bool IsSuccess { get; set; }
        [TestCase(Description = "Create new Custom Onetime Challenge", TestName = "Create new Custom Onetime Challenge")]
        public void CreateNewCustomOneTimeChallenge()
        {

            string errorMessage = string.Empty;
            try
            {
                var data = GetData();
                if (data == null)
                {
                    Assert.Fail("No Valid Input Data found");
                    Console.WriteLine("No Valid Input Data found");
                }
                loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
                Thread.Sleep(2000);
                loginPage.LoginButton.Click();

                Thread.Sleep(1000);

                Console.WriteLine("Administrator Logged in successfully");

                var adminPage = new AdminPage(driver);
                if (adminPage.Clients != null && adminPage.Clients.Displayed)
                {
                    adminPage.Clients.Click();
                    Console.WriteLine("Clients link clicked");
                }
                else
                {
                    Console.WriteLine("Clients Link not found");
                    Assert.Fail("Clients link not found");
                }

                var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
                if (clientElement != null && clientElement.Displayed)
                {
                    Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                    clientElement.Click();
                }
                else
                {
                    Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                    Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                }

                var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
                if (programElement != null && programElement.Displayed)
                {
                    programElement.Click();
                    Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
                }
                else
                {
                    Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
                }

                if (adminPage.ProgramTabs.Count > 0)
                {
                    Console.WriteLine("Program Listing page loaded");

                }
                else
                {
                    Console.WriteLine("Program details page not loaded");
                    Assert.Fail("Failed to load Program details");
                }

                Thread.Sleep(1000);

                var challengeTab = adminPage.GetProgramTab("Challenges");
                if (challengeTab == null)
                {
                    Console.WriteLine("Unable to find challenges tab");
                    Assert.Fail("Unable to find challenges tab");
                }
                else
                {
                    Console.WriteLine("Challenge Tab loaded");
                    challengeTab.Click();
                }

                Thread.Sleep(5000);

                adminPage.GetAddChallengeButton();
                if (adminPage.AddChallengeButton != null)
                {
                    adminPage.AddChallengeButton.Click();
                    Console.WriteLine("Add Challenge Button clicked");
                    Thread.Sleep(2000);
                }
                else
                {
                    Console.WriteLine("Cannot add new challenge");
                    Assert.Fail("cannot add new challenge");
                }

                Thread.Sleep(2000);

                var challengeCategory = adminPage.GetChallengeCategory("Custom");
                if (challengeCategory != null)
                {
                    challengeCategory.Click();
                    Console.WriteLine("Custom Challenge type selected");
                }
                else
                {
                    Console.WriteLine("Custom Challenge Type not available");
                    Assert.Fail("Custom Challenge Type not available");
                }

                Thread.Sleep(2000);
                var challengeType = adminPage.GetChallengeType("Custom");
                if (challengeType != null)
                {
                    challengeType.Click();
                    Console.WriteLine("Custom Challenge selected");
                }
                else
                {
                    errorMessage = "Custom Challenge not found";
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }
                AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);

                string randomNumber = new Random().Next(1000).ToString();
                string challengename = data.Challenge.ChallengeName;
                string startdate = data.Challenge.StartDate;
                challengename = challengename.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
                challengename = challengename.Replace("{currentday}", DateTime.Now.ToShortDateString());
                challengename = challengename.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
                challengename = challengename.Replace("{randomnumber}", randomNumber);
                GeneratedChallengeName = challengename;
                startdate = startdate.Replace("{currentday}", DateTime.Now.ToShortDateString());
                startdate = startdate.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
                startdate = startdate.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());

                bool success = addNewChallengePage.FillStepForCustom(challengename, startdate, data.Challenge.EndDate, data.Challenge.CustomChallengeActionText, data.Challenge.CustomChallengeGoal.ToString(), data.Challenge.CustomChallengeUnit, data.Challenge.CustomChallengeTime, data.Challenge.GoalText, out errorMessage);
                if (success)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                    addNewChallengePage.Step1_NextButton.Click();
                }
                else
                {
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                Thread.Sleep(1000);
                var participantCount = addNewChallengePage.Step2_ParticipantList;
                if (participantCount != null && participantCount.Count > 0)
                {
                    Console.WriteLine("Participants available to select");
                }
                else
                {
                    Console.WriteLine("No Participants available");
                    Assert.Fail("No Participants available");
                }


                success = addNewChallengePage.AddChallengers(data.Challenge.ChallengerName, out errorMessage);
                if (success)
                {
                    Console.WriteLine("Participant {0} added to challenge", data.Challenge.ChallengerName);
                }
                else
                {
                    Console.WriteLine("Failed to add {0} challenger. Error: {1}", data.Challenge.ChallengerName, errorMessage);
                    Assert.Fail(errorMessage);
                }
                Thread.Sleep(3000);
                addNewChallengePage.Step2_CreateButton.Click();
                Console.WriteLine("Create Challenge button clicked");


                WebDriverWait wait = GetWebDriverWait();
                var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
                if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
                {
                    string message = string.Format("Challenge {0} added successfully", challengename);
                    Console.WriteLine(message);
                    Assert.IsTrue(true);
                }
                else
                {
                    errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                    Console.Error.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("Creating Custom Onetime Challenge Failed. Error: {0}", ex);

            }
        }

        [TestCase(Description = "Join Onetime Challenge, Add Goals and Validate Goals added", TestName = "Custom Challenge One Time - Goals Validation")]
        public void ValidateGoals()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Failed to create challenge. Scenario cannot be validated."); return;
            }

            if (string.IsNullOrEmpty(GeneratedChallengeName))
            {
                Console.Error.WriteLine("Challenge not available");
                Assert.Fail("Challenge not available");
                return;
            }

            var data = GetData();
            if (data == null)
            {
                Console.Error.WriteLine("Data not available");
                Assert.Fail("Data not available");
                return;
            }
            Thread.Sleep(2000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }

            Thread.Sleep(1000);
            By loadingImage = By.TagName("http-busy");
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

            loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);

            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }

            IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
            if (challengeTab != null && challengeTab.Displayed)
            {
                challengeTab.Click();
                Console.WriteLine("Challenge Tab Clicked");
            }

            Thread.Sleep(3000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

            var challengeList = driver.FindElements(By.XPath("//div[@class='panel challenge ng-scope']"));
            if (challengeList != null && challengeList.Count > 0)
            {
                Console.WriteLine("{0} Challenges available for participant", challengeList.Count);
            }
            else
            {
                Console.Error.WriteLine("Failed to load challenges");
                Assert.Fail("Failed to load challenges");
            }

            var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", GeneratedChallengeName))));
            if (challenge != null && challenge.Displayed)
            {
                challenge.Click();
                Console.WriteLine("{0} available challenge found", GeneratedChallengeName);
            }
            else
            {
                Console.Error.WriteLine("{0} available challenge not found", GeneratedChallengeName);
                Assert.Fail("{0} available challenge not found", GeneratedChallengeName);
            }



            IWebElement goalTextElement = driver.FindElement(By.XPath("//div[@class='ng-scope']/h2[contains(.,'Goal')]/following-sibling::p[1]"));
            if (goalTextElement != null && goalTextElement.Displayed)
            {
                if (goalTextElement.Text.Equals(data.Challenge.GoalText, StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine("Goal Text {0} matching", data.Challenge.GoalText);

                }
            }
            else
            {
                Console.Error.WriteLine("Cannot find Goal Text");
                Assert.Fail("failed to load goal text element");
            }


            IWebElement joinButtonElement = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(.,'Join!')]")));
            if (joinButtonElement != null && joinButtonElement.Displayed)
            {
                Thread.Sleep(1000);
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                joinButtonElement.Click();
                Console.WriteLine("Join Button available and clicked to join this challenge");
            }

            Thread.Sleep(1000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

            IWebElement numberOfGoalTextElement = driver.FindElement(By.XPath("//div[@class='numericGoalMetNumber']/div"));
            if (numberOfGoalTextElement != null && numberOfGoalTextElement.Displayed)
            {
                Console.WriteLine("Goals value been set to {0}", numberOfGoalTextElement.Text);
                if (numberOfGoalTextElement.Text == "Yes")
                {
                    Console.WriteLine("Goal already met");
                }
            }
            else
            {
                Console.Error.WriteLine("Goal Achieve Text Element not available");
                Assert.Fail("Goal Achieve Text Element not available");
            }

            string challengestartdate = data.Challenge.StartDate;
            challengestartdate = challengestartdate.Replace("{currentdate}", DateTime.Now.ToShortDateString());
            challengestartdate = challengestartdate.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
            challengestartdate = challengestartdate.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());

            DateTime challengeDate = DateTime.Parse(challengestartdate);
            IWebElement logDateElement = null;
            driver.FindElements(By.CssSelector(".md-datepicker-input")).FirstOrDefault().Clear();
            try
            {

                driver.FindElements(By.CssSelector(".md-datepicker-input")).FirstOrDefault().Click();
                logDateElement = driver.FindElement(By.XPath(string.Format("//span[@class='md-calendar-date-selection-indicator' and contains(.,'{0}')]", challengeDate.Day)));
                logDateElement.Click();
            }
            catch
            {
                logDateElement = driver.FindElements(By.CssSelector(".md-datepicker-input")).FirstOrDefault();
                logDateElement.SendKeys(challengeDate.ToShortDateString());
            }
            Thread.Sleep(1000);
            driver.FindElement(By.Id("numericValue")).Clear();
            driver.FindElement(By.Id("numericValue")).SendKeys(data.Challenge.CustomChallengeGoal.ToString());

            var button = driver.FindElement(By.XPath("/html/body/div/div/div[2]/div/div[2]/div/div[2]/ng-include/div/div[1]/div/div/div/form/div[4]/button"));
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].click()", button);
            Console.WriteLine("{0} Goal added for date {1}", data.Challenge.CustomChallengeGoal, challengeDate.ToShortDateString());
            Thread.Sleep(1000);

            numberOfGoalTextElement = driver.FindElement(By.XPath("//div[@class='numericGoalMetNumber']/div"));
            if (numberOfGoalTextElement != null && numberOfGoalTextElement.Displayed && numberOfGoalTextElement.Text == "Yes")
            {
                Console.WriteLine("Goal met successfully");
                Assert.IsTrue(numberOfGoalTextElement.Text == "Yes");
            }
            else
            {
                Console.Error.WriteLine("Goal Achieve Text Element not available");
                Assert.Fail("Goal Achieve Text Element not available");
            }
        }


        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private DateTime GetStartOfWeek(DateTime input)
        {
            // Using +6 here leaves Monday as 0, Tuesday as 1 etc.
            int dayOfWeek = (((int)input.DayOfWeek) + 6) % 7;
            return input.Date.AddDays(-dayOfWeek);
        }

        private int GetWeeks(DateTime start, DateTime end)
        {
            start = GetStartOfWeek(start);
            end = GetStartOfWeek(end);
            int days = (int)(end - start).TotalDays;
            return (days / 7) + 1; // Adding 1 to be inclusive
        }

        private PIV94Scenario5Data GetData()
        {
            PIV94Scenario5Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV94Scenario5.json");
                data = JsonConvert.DeserializeObject<PIV94Scenario5Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
    }
}
