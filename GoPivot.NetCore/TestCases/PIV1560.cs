﻿
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using System.Collections.Generic;
using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1560", Category = "Nutrition")]
    public class PIV1560 : BaseStepDefinition
    {
        [TestCase(Description = "PIV 1560 -  As a user, I want to access the full recipe form it's source so I can learn more about the recipe.", TestName = "Access Full Recipe Form")]
        public void ValidateFullRecipeLinks()
        {
            try
            {


                var loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
                loginPage.LoginButton.Click();

                Thread.Sleep(2000);

                var logoutElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);

                }
                else
                {
                    Assert.Fail("Participant Login Failed");
                    return;
                }
                Thread.Sleep(5000);
                var nutritionTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }
                Thread.Sleep(1000);

                BuildRecipePlanIfNotExists();
                Thread.Sleep(3000);
                IList<IWebElement> recipeButtonElements = driver.FindElements(By.XPath("//div[contains(@class,'recipeButton')]/a"));
                Assert.IsTrue(recipeButtonElements.Count == 3, "View recipe links available for all meals");

                foreach (var element in recipeButtonElements)
                {
                    var url = element.GetAttribute("href");
                    var result = Uri.TryCreate(url, UriKind.Absolute, out var uriResult)
                                  && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                    if (!string.IsNullOrEmpty(url))
                    {
                        Assert.IsTrue(result, "URL must be valid");
                        Assert.IsTrue(element.GetAttribute("target") == "_blank", "URL should have _blank target");
                        Console.WriteLine("{0} is valid URL contains _blank target", element.GetAttribute("href"));

                    }
                    else
                    {
                        Console.Error.WriteLine("No URL exists for link {0}", element.Text);
                        Assert.Fail("No URL exists for link {0}", element.Text);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("PIV-1560 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1560 Failed. Error: {0}", ex);
            }
        }

        private void BuildRecipePlanIfNotExists()
        {
            try
            {
                driver.FindElement(By.LinkText("Build Your Recipe Plan")).Click();
                Console.WriteLine("Build Your Recipe Plan clicked");

                driver.FindElement(By.XPath("//input[@ng-model='model.desiredWeight']")).SendKeys("100");
                driver.FindElement(By.XPath("//input[@type='button' and @value = 'Save']")).Click();
                Console.WriteLine("Save Button clicked");

                Thread.Sleep(3000);

                var nutritionTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }
                Thread.Sleep(3000);
            }
            catch
            {
                Console.WriteLine("Button: Build Your Recipe Plan not found");
                Console.WriteLine("Recipe Plan already available for this user");
                // Do nothing
            }
        }

        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}
