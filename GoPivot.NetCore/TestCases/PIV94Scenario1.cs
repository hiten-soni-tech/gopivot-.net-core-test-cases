﻿
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-94 Scenario 1", Category = "Challenge")]
    public class PIV94Scenario1 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        private LoginPage loginPage;
        private string GeneratedChallengeName;
        [TestCase(Description = "Create new Wellness Challenge", TestName = "Create Wellness Challenge")]
        public void CreateNewWellnessChallenge()
        {
            string errorMessage = string.Empty;
            try
            {
                var data = GetData();
                if (data == null)
                {
                    Assert.Fail("No Valid Input Data found");
                    Console.WriteLine("No Valid Input Data found");
                }
                loginPage = new LoginPage(driver);
                Console.WriteLine("Login Page loaded successfully");


                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
                loginPage.LoginButton.Click();

                Console.WriteLine("Administrator {0} Logged in successfully", ObjectRepository.Setting.Credentials.Administrator.Username);
                Console.WriteLine("Admin View Loaded");

                Thread.Sleep(3000);
                var adminPage = new AdminPage(driver);
                if (adminPage.Clients != null && adminPage.Clients.Displayed)
                {
                    adminPage.Clients.Click();
                    Console.WriteLine("Clients link clicked");
                }
                else
                {
                    Console.Error.WriteLine("Clients Link not found");
                    Assert.Fail("Clients link not found");
                }

                Thread.Sleep(3000);
                var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
                if (clientElement != null && clientElement.Displayed)
                {
                    Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                    clientElement.Click();
                }
                else
                {
                    Console.Error.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                    Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                }

                if (!adminPage.EditClient.Displayed)
                {
                    Console.Error.WriteLine("Clients Details not found");
                    Assert.Fail("Clients Details not found");

                }
                else
                {
                    Console.WriteLine("Client Details page loaded");
                }

                var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
                if (programElement != null && programElement.Displayed)
                {
                    programElement.Click();
                    Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
                }
                else
                {
                    Console.Error.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
                }

                if (adminPage.ProgramTabs.Count > 0)
                {
                    Console.WriteLine("Program Listing page loaded");

                }
                else
                {
                    Console.Error.WriteLine("Program details page not loaded");
                    Assert.Fail("Failed to load Program details");
                }

                Thread.Sleep(1000);

                var challengeTab = adminPage.GetProgramTab("Challenges");
                if (challengeTab == null)
                {
                    Console.Error.WriteLine("Unable to find challenges tab");
                    Assert.Fail("Unable to find challenges tab");
                }
                else
                {
                    Console.WriteLine("Challenge Tab loaded");
                    challengeTab.Click();
                }

                Thread.Sleep(5000);
                adminPage.GetAddChallengeButton();
                if (adminPage.AddChallengeButton != null)
                {
                    adminPage.AddChallengeButton.Click();
                    Console.WriteLine("Add Challenge Button clicked");
                    Thread.Sleep(2000);
                }
                else
                {
                    Console.Error.WriteLine("Cannot add new challenge");
                    Assert.Fail("cannot add new challenge");
                }


                Thread.Sleep(2000);
                var challengeCategory = adminPage.GetChallengeCategory("Wellness");
                if (challengeCategory != null)
                {
                    challengeCategory.Click();
                    Console.WriteLine("Wellness Challenge type selected");
                }
                else
                {
                    Console.Error.WriteLine("Wellness Challenge Type not available");
                    Assert.Fail("Wellness Challenge Type not available");
                }

                Thread.Sleep(2000);
                var challengeType = adminPage.GetChallengeType(data.Administrator.WellnessChallengeType);
                if (challengeType != null)
                {
                    challengeType.Click();
                    Console.WriteLine("{0} Challenge Type selected", data.Administrator.WellnessChallengeType);
                }
                else
                {
                    errorMessage = string.Format("{0} Challenge type not found", data.Administrator.WellnessChallengeType);
                    Console.Error.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
                string challengename = data.Administrator.ChallengeName;
                string startdate = data.Administrator.StartDate;
                string randomNumber = new Random().Next(1000).ToString();
                challengename = challengename.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
                challengename = challengename.Replace("{currentday}", DateTime.Now.ToShortDateString());
                challengename = challengename.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
                challengename = challengename.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());
                challengename = challengename.Replace("{randomnumber}", randomNumber);

                startdate = startdate.Replace("{currentday}", DateTime.Now.ToShortDateString());
                startdate = startdate.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
                startdate = startdate.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
                startdate = startdate.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());

                GeneratedChallengeName = challengename;
                bool success = addNewChallengePage.FillStep1(challengename, startdate, data.Administrator.EndDate, data.Administrator.GoalValue.ToString(), data.Administrator.Timeline, data.Administrator.Description, out errorMessage);
                if (success)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                    addNewChallengePage.Step1_NextButton.Click();
                }
                else
                {
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                var participantCount = addNewChallengePage.Step2_ParticipantList;
                if (participantCount != null && participantCount.Count > 0)
                {
                    Console.WriteLine("Participants available to select");
                }
                else
                {
                    Console.Error.WriteLine("No Participants available");
                    Assert.Fail("No Participants available");
                }

                success = addNewChallengePage.AddChallengers(data.Administrator.ChallengerName, out errorMessage);
                if (success)
                {
                    Console.WriteLine("Participant {0} added to challenge", data.Administrator.ChallengerName);
                }
                else
                {
                    Console.Error.WriteLine("Failed to add {0} challenger. Error: {1}", data.Administrator.ChallengerName, errorMessage);
                    Assert.Fail(errorMessage);
                }


                addNewChallengePage.Step2_CreateButton.Click();
                Console.WriteLine("Create Challenge button clicked");

                WebDriverWait wait = GetWebDriverWait();
                var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
                if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
                {
                    string message = string.Format("Challenge {0} added successfully", challengename);
                    Console.WriteLine(message);
                    Assert.IsTrue(true);
                }
                else
                {
                    errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                    Assert.Fail(errorMessage);
                    Console.Error.WriteLine(errorMessage);
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("Creating Wellness Challenge Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "Join the Challenge and Display Goal", TestName = "Display Goal")]
        public void DisplayGoal()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Failed to create challenge. Scenario cannot be validated."); return;
            }

            try
            {
                if (string.IsNullOrEmpty(GeneratedChallengeName))
                {
                    Console.Error.WriteLine("Challenge not available");
                    Assert.Fail("Challenge not available");
                }

                var data = GetData();
                if (data == null)
                {
                    Console.Error.WriteLine("Data not available");
                    Assert.Fail("Data not available");
                }
                Thread.Sleep(2000);

                By loadingImage = By.TagName("http-busy");
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
                if (dropdownMenuElement.Displayed)
                {
                    dropdownMenuElement.Click();

                    var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                    if (logoutElement != null && logoutElement.Displayed)
                    {
                        logoutElement.Click();
                        Console.WriteLine("Administrator logged off successfully");
                    }
                    else
                    {
                        Console.Error.WriteLine("Logout link not clickable");
                        Assert.Fail("Logout link not clickable");
                    }

                }
                else
                {
                    Console.Error.WriteLine("Logout Dropdown not visible");
                    Assert.Fail("Logout Dropdown not visible");
                }


                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));
                Thread.Sleep(1000);
                loginPage = new LoginPage(driver);
                Console.WriteLine("Login Page loaded successfully");
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
                Thread.Sleep(2000);
                loginPage.LoginButton.Click();
                Thread.Sleep(3000);
                var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement1 != null && logoutElement1.Displayed)
                {
                    Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);

                }
                else
                {
                    Console.Error.WriteLine("Login Failed. Failed to load participant view");
                    Assert.Fail("Login Failed. Failed to load participant view");
                }

                IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
                if (challengeTab != null && challengeTab.Displayed)
                {
                    challengeTab.Click();
                    Console.WriteLine("Challenge Tab Clicked");
                }

                Thread.Sleep(3000);
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                var challengeList = driver.FindElements(By.XPath("//div[@class='panel challenge ng-scope']"));
                if (challengeList != null && challengeList.Count > 0)
                {
                    Console.WriteLine("{0} Challenges available for participant", challengeList.Count);
                }
                else
                {
                    Console.Error.WriteLine("Failed to load challenges");
                    Assert.Fail("Failed to load challenges");
                }

                var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", GeneratedChallengeName))));
                if (challenge != null && challenge.Displayed)
                {
                    challenge.Click();
                    Console.WriteLine("{0} available challenge found", GeneratedChallengeName);
                }
                else
                {
                    Console.Error.WriteLine("{0} available challenge not found", GeneratedChallengeName);
                    Assert.Fail("{0} available challenge not found", GeneratedChallengeName);
                }

                var goalTextElement = driver.FindElement(By.XPath("//div[@class='ng-scope']/h2[contains(.,'Goal')]/following-sibling::p[1]"));
                if (goalTextElement != null && goalTextElement.Displayed && goalTextElement.Text.ToLower().Contains(data.Participant.GoalDescription.ToLower()))
                {
                    Console.WriteLine("Goal {0} matching", data.Participant.GoalDescription);
                    Assert.IsTrue(goalTextElement.Text.ToLower().Contains(data.Participant.GoalDescription.ToLower()));

                }
                else
                {
                    Console.Error.WriteLine("Failed to load goal details");
                    Assert.Fail("Failed to validate goal. Error: Element not found or text not matching");

                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.Error.WriteLine("Failed to view goal details. Error: {0}", ex.Message);
                Assert.Fail("Failed to view goal details. Error: {0}", ex);
            }

        }

        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private DateTime GetStartOfWeek(DateTime input)
        {
            // Using +6 here leaves Monday as 0, Tuesday as 1 etc.
            int dayOfWeek = (((int)input.DayOfWeek) + 6) % 7;
            return input.Date.AddDays(-dayOfWeek);
        }

        private int GetWeeks(DateTime start, DateTime end)
        {
            start = GetStartOfWeek(start);
            end = GetStartOfWeek(end);
            int days = (int)(end - start).TotalDays;
            return (days / 7) + 1; // Adding 1 to be inclusive
        }

        private PIV94Scenario1Data GetData()
        {
            PIV94Scenario1Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV94Scenario1.json");  
                data = JsonConvert.DeserializeObject<PIV94Scenario1Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
    }
}
