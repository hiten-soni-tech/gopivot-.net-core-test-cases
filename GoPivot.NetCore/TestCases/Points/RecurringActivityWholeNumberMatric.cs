﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;
using GoPivot.NetCore.DataEntities.Points;

namespace GoPivot.NetCore.TestCases.Points
{
    [TestFixture(TestName = "Recurring Activity Whole Number Matric", Category = "Activity")]
    public class RecurringActivityWholeNumberMatric : BaseStepDefinition
    {
        [TestCase(Description = "Recurring Activity / Whole Number Metric", TestName = "Recurring Activity / Whole Number Metric")]
        public void Validate_Recurring_Activity_Whole_Number_Matric()
        {
            try
            {
                var Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAndSelectActivity();
                Thread.Sleep(3000);
                driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
                Console.WriteLine("Add Activity button clicked");

                Thread.Sleep(5000);
                Data.NewActivity = new AdminPage(driver).CreateNewActivity(Data.NewActivity, errorMessage: out string errorMessage);
                if (Data.NewActivity == null)
                {
                    Assert.Fail(errorMessage);
                }
                Console.WriteLine("Activity created successfully. Name: {0}", Data.NewActivity.Name);

                LogoutAdministrator();
                Thread.Sleep(1000);
                string activityName = Data.NewActivity.Name;
                //string activityName = "Daily Steps - 561";
                LoginParticipant();
                IWebElement activityTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Activities")));
                activityTab.Click();
                Console.WriteLine("Activities Link clicked");
                Thread.Sleep(4000);
                string activityLinkXPath =
                    string.Format(
                        "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
                        activityName);
                IWebElement activityElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(activityLinkXPath)));
                Assert.IsTrue(activityElement.Displayed, string.Format("Activity {0} must be visible in activity list", activityName));
                activityElement.Click();

                string pointsText = driver.FindElement(By.XPath("//li[@class='points']/a")).Text;
                pointsText = pointsText.Replace("pts", "").Trim();
                pointsText = pointsText.Replace(",", "").Trim();
                int.TryParse(pointsText, out int points);
                Console.WriteLine("Current Points are: {0}", points);
                driver.FindElement(By.Name("activityWholeNumber")).Clear();
                driver.FindElement(By.Name("activityWholeNumber")).SendKeys(Data.InvalidActivitySteps.ToString());

                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//button[contains(.,'Save')]")).Click();
                Thread.Sleep(2000);

                Assert.IsTrue(driver.FindElement(By.ClassName("icon-check")).Displayed, "Check Icon should be displayed after adding steps {0}", Data.InvalidActivitySteps);
                Console.WriteLine("Steps {0} added successfully", Data.InvalidActivitySteps.ToString());

                Thread.Sleep(2000);

                pointsText = driver.FindElement(By.XPath("//li[@class='points']/a")).Text;
                pointsText = pointsText.Replace("pts", "").Trim();
                pointsText = pointsText.Replace(",", "").Trim();
                int.TryParse(pointsText, out int newInvalidPoints);
                Console.WriteLine("New Points are {0}", newInvalidPoints);
                Assert.IsTrue(points == newInvalidPoints, "No Changes in points as Steps are not matching to add points");
                Console.WriteLine("Old Points and New Points are matching");

                driver.Navigate().Refresh();

                driver.FindElement(By.Name("activityWholeNumber")).Clear();
                driver.FindElement(By.Name("activityWholeNumber")).SendKeys(Data.ValidActivitySteps.ToString());

                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//button[contains(.,'Save')]")).Click();
                Thread.Sleep(2000);

                Assert.IsTrue(driver.FindElement(By.ClassName("icon-check")).Displayed, "Check Icon should be displayed after adding steps {0}", Data.ValidActivitySteps);
                Console.WriteLine("Steps {0} added successfully", Data.ValidActivitySteps.ToString());

                Thread.Sleep(2000);

                pointsText = driver.FindElement(By.XPath("//li[@class='points']/a")).Text;
                pointsText = pointsText.Replace("pts", "").Trim();
                pointsText = pointsText.Replace(",", "").Trim();
                int.TryParse(pointsText, out int newValidPoints);

                int requiredPoints = points + Data.NewActivity.MeasurePointValue;
                Assert.IsTrue(requiredPoints == newValidPoints, "Points should be increased after updating valid steps");
                Console.WriteLine("New points added into Points total. Current Value {0}", requiredPoints);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Recurring Activity Whole Number Matric Failed. Error: {0}", ex.Message);
                Assert.Fail("Recurring Activity Whole Number Matric Failed. Error: {0}", ex);
            }
        }
        #region Utilities 


        private RecurringActivityWholeNumberMatricData GetData()
        {
            RecurringActivityWholeNumberMatricData data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//Points//RecurringActivityWholeNumberMatric.json");
                data = JsonConvert.DeserializeObject<RecurringActivityWholeNumberMatricData>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private static void LoginAndSelectActivity()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);
            var activitiesTab = adminPage.GetProgramTab("Activities");
            if (activitiesTab == null)
            {
                Console.WriteLine("Unable to find activities tab");
                Assert.Fail("Unable to find activities tab");
            }
            else
            {
                Console.WriteLine("Activities Tab loaded");
                activitiesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(6000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

        }
        private void LogoutParticipant()
        {
            Thread.Sleep(3000);
            var dropdownMenuElement = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//div[contains(@class,'profileMenu')]")));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }
        private void LogoutAdministrator()
        {
            Thread.Sleep(3000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                Thread.Sleep(3000);
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }


        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
