﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.Collections.Generic;
using System.Linq;
using GoPivot.NetCore.DataEntities;
using System.IO;
using Newtonsoft.Json;
using OpenQA.Selenium.Interactions;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-159", Category = "Assessment")]
    public class PIV159 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV159Data Data { get; set; }
        [TestCase(TestName = "Scenario 1: Add Assessments option to Program navigation")]
        [Order(1)]
        public void Validate_Add_Assessment_Option_To_Program_Navigation()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAdministrator();
                AdminPage adminPage = new AdminPage(driver);
                var assessmentTab = adminPage.GetProgramTab(Data.TabName);
                if (assessmentTab == null)
                {
                    Console.WriteLine("Unable to find Assessment tab");
                    Assert.Fail("Unable to find Assessment tab");
                }
                Console.WriteLine($"{Data.TabName} available in program details");
                assessmentTab.Click();
                Thread.Sleep(3000);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-159 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-159 Scenario 1 Failed. Error: {0}", ex);
            }

        }

        [TestCase(TestName = "Scenario 2: View Assessments list")]
        [Order(2)]
        public void Validate_View_Assessment_List()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
            
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                var pageTitleElement = driver.FindElement(By.XPath("//div[contains(@class,'programAssessments')]/h2"));
                Assert.IsTrue(pageTitleElement.Text == Data.PageTitle, "Page Title must be {0}", Data.PageTitle);
                Console.WriteLine("Page Title is valid: {0}", Data.PageTitle);

                var searchTextElement = driver.FindElement(By.XPath($"//input[@placeholder = '{Data.SearchBoxLabel}']"));
                Assert.IsTrue(searchTextElement.Displayed, "Search input box must be available");
                Console.WriteLine("Search Input box available");

                var showCompletedCheckboxElement = driver.FindElement(By.XPath("//md-checkbox[@ng-model='query.showCompleted']"));
                Assert.IsTrue(showCompletedCheckboxElement.Displayed, "Show Completed checkbox must be displayed");
                Console.WriteLine("Show Completed checkbox displayed");

                var showCompletedCheckboxLabelElement = driver.FindElement(By.XPath("//md-checkbox[@ng-model='query.showCompleted']/div[@class='md-label']"));
                Assert.IsTrue(showCompletedCheckboxLabelElement.Text == Data.ShowCompletedLabel, "Show Completed label must be matching with {0}", Data.ShowCompletedLabel);
                Console.WriteLine("Show Completed label matching with {0}", Data.ShowCompletedLabel);

                Assert.IsTrue(driver.FindElement(By.XPath($"//button[contains(.,'{Data.AddButtonText}')]")).Displayed, "{0} Button displayed", Data.AddButtonText);

                // Columns Validation

                Assert.IsTrue(driver.FindElement(By.XPath($"//table[contains(@class,'adminTable')]/thead/tr/th[contains(.,'{Data.TableColumns.StartDateLabel}')]")).Displayed, "Table {0} column must be displayed", Data.TableColumns.StartDateLabel);
                Console.WriteLine("Table contains {0} column", Data.TableColumns.StartDateLabel);
                Assert.IsTrue(driver.FindElement(By.XPath($"//table[contains(@class,'adminTable')]/thead/tr/th[contains(.,'{Data.TableColumns.EndDateLabel}')]")).Displayed, "Table {0} column must be displayed", Data.TableColumns.EndDateLabel);
                Console.WriteLine("Table contains {0} column", Data.TableColumns.EndDateLabel);
                Assert.IsTrue(driver.FindElement(By.XPath($"//table[contains(@class,'adminTable')]/thead/tr/th[contains(.,'{Data.TableColumns.IdLabel}')]")).Displayed, "Table {0} column must be displayed", Data.TableColumns.IdLabel);
                Console.WriteLine("Table contains {0} column", Data.TableColumns.IdLabel);
                Assert.IsTrue(driver.FindElement(By.XPath($"//table[contains(@class,'adminTable')]/thead/tr/th[contains(.,'{Data.TableColumns.NameLabel}')]")).Displayed, "Table {0} column must be displayed", Data.TableColumns.NameLabel);
                Console.WriteLine("Table contains {0} column", Data.TableColumns.NameLabel);

                var tableData = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr"));
                Console.WriteLine("Total {0} records available in table", tableData.Count);

                Assert.IsTrue(tableData.Count <= Data.Pagination, "Row count must be less than {0}", Data.Pagination);
                Console.Write("Table Row count is less than {0}", Data.Pagination);

                var totalAssessmentCount = Int32.Parse(driver.FindElement(By.XPath("//div[contains(@class,'programAssessments')]/div[@ng-show='searchResults.data.length > 0']/strong[2]")).Text);
                if (totalAssessmentCount > Data.Pagination)
                {
                    Console.Write("Total assessment count is greater than pagination count: {0}", totalAssessmentCount);
                    Assert.IsTrue(driver.FindElement(By.XPath("//ul[@class='pager']")).Displayed, "Pager must be displayed");
                }
                else
                {
                    try
                    {
                        Assert.IsFalse(driver.FindElement(By.XPath("//ul[@class='pager']")).Displayed, "Pager must NOT be displayed");

                    }
                    catch
                    {
                        Console.WriteLine("No Pagination pager been displayed");
                    }
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-159 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-159 Scenario 2 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 3: Item hover actions")]
        [Order(3)]
        public void Validate_Item_Hover_Actions()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            try
            {
            
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                var tableData = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr"));
                Console.WriteLine("Total {0} records available in table", tableData.Count);

                if (tableData.Count == 0)
                {
                    Console.WriteLine("No records available to hover on content");

                }
                else
                {
                    var firstElement =
                 driver.FindElement(
                     By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]"));
                    var builder = new Actions(driver);
                    builder.MoveToElement(firstElement).Build().Perform();

                    Assert.IsTrue(driver.FindElement(
                         By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]/td/i[@class='icon-refresh']")).Displayed, "Refresh button must be visible");
                    Console.WriteLine("Refresh button is available");


                    Assert.IsTrue(driver.FindElement(
                         By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]/td/i[contains(@class,'icon-docs')]")).Displayed, "Copy button must be visible");
                    Console.WriteLine("Copy button is available");


                    Assert.IsTrue(driver.FindElement(
                         By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]/td/i[contains(@class,'icon-trash')]")).Displayed, "Delete button must be visible");
                    Console.WriteLine("Delete button is available");
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-159 Scenario 3 Error: {0}", ex.Message);
                Assert.Fail("PIV-159 Scenario 3 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 4: Show Completed")]
        [Order(4)]
        public void Validate_Show_Completed()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 3 not completed"); return;
            }
            try
            {
                
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                Console.WriteLine("Feature not working");

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-159 Scenario 4 Error: {0}", ex.Message);
                Assert.Fail("PIV-159 Scenario 4 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 5: Search Assessments (Results Exist)")]
        [Order(5)]
        public void Validate_Search_Assessment_With_Result()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 4 not completed"); return;
            }
            try
            {
            
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                var tableData = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr"));
                Console.WriteLine("Total {0} records available in table", tableData.Count);

                if (tableData.Count == 0)
                {
                    Console.WriteLine("No records available to search any assessments");

                }
                else
                {

                    string firstAssessment = driver.FindElement(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr[1]/td[3]")).Text;
                    string searchText = firstAssessment.Substring(0, 4);
                    driver.FindElement(By.XPath($"//input[@placeholder = '{Data.SearchBoxLabel}']")).SendKeys(searchText);
                    Console.WriteLine("Search Value is {0}", searchText);
                    Thread.Sleep(3000);

                    var newTableData = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr/td[3]"));
                    if (newTableData.Count == 0)
                    {
                        Assert.Fail("No record found for search text {0}", searchText);
                    }
                    foreach (var data in newTableData)
                    {
                        if (data.Text.Contains(searchText))
                        {
                            Console.WriteLine("Assessment Name {0} contains search value {1}", data.Text, searchText);
                        }
                        else
                        {
                            Assert.Fail("Assessment name {0} does not contain search value {1}", data.Text, searchText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-159 Scenario 5 Error: {0}", ex.Message);
                Assert.Fail("PIV-159 Scenario 5 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 6: Search Assessments (No Results Exist)")]
        [Order(6)]
        public void Validate_Search_Assessment_Without_Result()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 5 not completed"); return;
            }
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                string searchText = Guid.NewGuid().ToString().Substring(0, 8);
                driver.FindElement(By.XPath($"//input[@placeholder = '{Data.SearchBoxLabel}']")).Clear();
                driver.FindElement(By.XPath($"//input[@placeholder = '{Data.SearchBoxLabel}']")).SendKeys(searchText);
                Console.WriteLine("Search Value is {0}", searchText);
                Thread.Sleep(3000);

                var newTableData = driver.FindElements(By.XPath("//table[contains(@class,'adminTable')]/tbody/tr/td[3]"));
                Assert.IsTrue(newTableData.Count == 0, "No search result should be available");

                var nomatchElement = driver.FindElement(By.XPath("//div[@ng-show='searchResults.data.length === 0']/p"));
                Assert.IsTrue(nomatchElement.Text.Equals(Data.NoSearchResultLabel), "No Search result text should be matching");
                Console.WriteLine("No search result text is visible and matching");
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-159 Scenario 6 Error: {0}", ex.Message);
                Assert.Fail("PIV-159 Scenario 6 Failed. Error: {0}", ex);
            }
        }

        #region Utilities
        private void LogoutAdministrator()
        {
            Thread.Sleep(5000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);

        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private PIV159Data GetData()
        {
            PIV159Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV159.json");
                data = JsonConvert.DeserializeObject<PIV159Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
        #endregion
    }
}
