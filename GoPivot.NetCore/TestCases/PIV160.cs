﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.Collections.Generic;
using System.Linq;
using GoPivot.NetCore.DataEntities;
using System.IO;
using Newtonsoft.Json;
namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-160", Category = "Assessment")]
    public class PIV160 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV160Data Data { get; set; }
        [TestCase(TestName = "Scenario 1: Assessment Wizard - Choose Assessment Type")]
        [Order(1)]
        public void Validate_Assessment_Wizard_Choose_Assessment_Type()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAdministrator();
                AdminPage adminPage = new AdminPage(driver);
                var assessmentTab = adminPage.GetProgramTab("Assessment");
                if (assessmentTab == null)
                {
                    Console.WriteLine("Unable to find Assessment tab");
                    Assert.Fail("Unable to find Assessment tab");
                }
                else
                {
                    Console.WriteLine("Assessment Tab loaded");
                    assessmentTab.Click();
                }

                Thread.Sleep(3000);
                driver.FindElement(By.XPath("//button[contains(.,'Add Assessment')]")).Click();
                Console.WriteLine("Add Assessment button clicked");
                Thread.Sleep(2000);


                Assert.IsTrue(driver.FindElement(By.XPath($"//h3[contains(.,'{Data.PageHeader}')]")).Displayed, "Page header must be displayed");
                Console.WriteLine("Page Header displayed");

                Assert.IsTrue(driver.FindElement(By.LinkText(Data.ReturnToAssessmentLinkText)).Displayed, "Return to assessment list link must bed displayed");
                Console.Write("Return to assessment list displayed");

                var step1Element = driver.FindElement(By.XPath($"//wizard-step-display//div[@class='bottomRow']/div[1]"));
                Assert.IsTrue(step1Element.Text.Equals(Data.AssessmentSteps.Step1.BreadcrumbText), "Step 1 text must be matching to {0}", Data.AssessmentSteps.Step1.BreadcrumbText);
                Console.WriteLine("Step 1 text matching to {0}", Data.AssessmentSteps.Step1.BreadcrumbText);


                var step2Element = driver.FindElement(By.XPath($"//wizard-step-display//div[@class='bottomRow']/div[2]"));
                Assert.IsTrue(step2Element.Text.Equals(Data.AssessmentSteps.Step2.BreadcrumbText), "Step 2 text must be matching to {0}", Data.AssessmentSteps.Step2.BreadcrumbText);
                Console.WriteLine("Step 2 text matching to {0}", Data.AssessmentSteps.Step2.BreadcrumbText);


                var step3Element = driver.FindElement(By.XPath($"//wizard-step-display//div[@class='bottomRow']/div[3]"));
                Assert.IsTrue(step3Element.Text.Equals(Data.AssessmentSteps.Step3.BreadcrumbText), "Step 3 text must be matching to {0}", Data.AssessmentSteps.Step3.BreadcrumbText);
                Console.WriteLine("Step 3 text matching to {0}", Data.AssessmentSteps.Step3.BreadcrumbText);
                IsSuccess = true;

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-160 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-160 Scenario 1 Failed. Error: {0}", ex);
            }

        }

        [TestCase(TestName = "Scenario 2: View Assessment Types")]
        [Order(2)]
        public void Validate_View_Assessment_Types()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                Assert.IsTrue(driver.FindElement(By.XPath($"//form/ng-include/div[contains(.,'{Data.AssessmentSteps.Step1.PageSummary}')]")).Displayed, "Page Summary {0} must be displayed", Data.AssessmentSteps.Step1.PageSummary);
                Console.WriteLine($"Page Summary {Data.AssessmentSteps.Step1.PageSummary} displayed");

                Assert.IsTrue(
                    driver.FindElement
                        (
                           By.XPath($"//div[contains(@class,'assessmentTemplate')]/div[contains(@class,'templateAssessmentName') and contains(.,'{Data.AssessmentSteps.Step1.Option1Text}')]")
                        ).Displayed,
                        "Option 1 text must be {0}",
                        Data.AssessmentSteps.Step1.Option1Text);
                Console.WriteLine("Option 1 text matching with {0}", Data.AssessmentSteps.Step1.Option1Text);
                Assert.IsTrue(
                  driver.FindElement
                   (
                      By.XPath($"//div[contains(@class,'assessmentTemplate')]/div[contains(@class,'templateDescription') and contains(.,'{Data.AssessmentSteps.Step1.Option1Summary}')]")
                   ).Displayed,
                   "Option 1 Description text must be {0}",
                   Data.AssessmentSteps.Step1.Option1Summary);

                Console.WriteLine("Option 1 description matching with {0}", Data.AssessmentSteps.Step1.Option1Summary);
                Assert.IsTrue(
                 driver.FindElement
                     (
                        By.XPath($"//div[contains(@class,'assessmentTemplate')]/div[contains(@class,'templateAssessmentName') and contains(.,'{Data.AssessmentSteps.Step1.Option2Text}')]")
                     ).Displayed,
                     "Option 2 text must be {0}",
                     Data.AssessmentSteps.Step1.Option2Text);
                Console.WriteLine("Option 2 text matching with {0}", Data.AssessmentSteps.Step1.Option2Text);

                Assert.IsTrue(
                   driver.FindElement
                   (
                      By.XPath($"//div[contains(@class,'assessmentTemplate')]/div[contains(@class,'templateDescription') and contains(.,'{Data.AssessmentSteps.Step1.Option2Summary}')]")
                   ).Displayed,
                   "Option 2 Description text must be {0}",
                   Data.AssessmentSteps.Step1.Option2Summary);
                Console.WriteLine("Option 2 description matching with {0}", Data.AssessmentSteps.Step1.Option2Summary);

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-160 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-160 Scenario 2 Failed. Error: {0}", ex);
            }

        }

        [TestCase(TestName = "Scenario 3: Return to Assessments List from wizard")]
        [Order(3)]
        public void Validate_Return_To_Assessment_List_Wizard()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 3 not completed"); return;
            }
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                driver.FindElement
                (
                   By.XPath($"//div[contains(@class,'assessmentTemplate')]/div[contains(@class,'templateAssessmentName') and contains(.,'{Data.AssessmentSteps.Step1.Option1Text}')]")
                ).Click();
                Console.WriteLine("Assessment {0} clicked", Data.AssessmentSteps.Step1.Option1Text);

                driver.FindElement(By.LinkText(Data.ReturnToAssessmentLinkText)).Click();
                Console.WriteLine($"{Data.ReturnToAssessmentLinkText} link clicked");

                Thread.Sleep(2000);

                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'modal-dialog')]")).Displayed,
                    "Modal Popup Displayed");
                Console.WriteLine("Modal Popup displayed");

                Assert.IsTrue(driver.FindElement(By.XPath("//button[contains(.,'Leave without saving')]")).Displayed, "Leave without button must be visible");
                Console.WriteLine("Button - Leave without saving displayed");
                Assert.IsTrue(driver.FindElement(By.XPath("//button[contains(.,'Stay on page')]")).Displayed, "Stay on page button must be visible");
                Console.WriteLine("Button - Stay on page displayed");


            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-160 Scenario 3 Error: {0}", ex.Message);
                Assert.Fail("PIV-160 Scenario 3 Failed. Error: {0}", ex);
            }

        }

        [TestCase(TestName = "Scenario 4: Return > Stay on Page")]
        [Order(4)]
        public void Validate_Return_Stay_On_Page()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 3 not completed"); return;
            }
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                driver.FindElement(By.XPath("//button[contains(.,'Stay on page')]")).Click();
                Console.WriteLine("Button - Stay on page clicked");

                try
                {
                    Assert.IsFalse(driver.FindElement(By.XPath("//div[contains(@class,'modal-dialog')]")).Displayed,
                    "Modal Popup should not be displayed");
                }
                catch
                {
                    Console.WriteLine("Modal popup is not visible");
                }
                Assert.IsTrue(driver.FindElement(By.XPath($"//form/ng-include/div[contains(.,'{Data.AssessmentSteps.Step1.PageSummary}')]")).Displayed, "Page Summary {0} must be displayed", Data.AssessmentSteps.Step1.PageSummary);
                Console.WriteLine($"Page Summary {Data.AssessmentSteps.Step1.PageSummary} displayed");

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-160 Scenario 4 Error: {0}", ex.Message);
                Assert.Fail("PIV-160 Scenario 4 Failed. Error: {0}", ex);
            }

        }

        [TestCase(TestName = "Scenario 5: Return > Leave without saving")]
        [Order(5)]
        public void Validate_Return_Leave_Without_Saving()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 4 not completed"); return;
            }

            try
            {
                string currentUrl = driver.Url;
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                driver.FindElement(By.LinkText(Data.ReturnToAssessmentLinkText)).Click();
                Console.WriteLine($"{Data.ReturnToAssessmentLinkText} link clicked");

                Thread.Sleep(2000);

                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'modal-dialog')]")).Displayed,
                    "Modal Popup Displayed");
                Console.WriteLine("Modal Popup displayed");

                driver.FindElement(By.XPath("//button[contains(.,'Leave without saving')]")).Click();
                Console.WriteLine("Leave without saving button clicked");

                Thread.Sleep(3000);
                string newUrl = driver.Url;
                Assert.IsFalse(newUrl == currentUrl, "Page should be redirected to listing page");
                Console.Write("Page redirected to listing page");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-160 Scenario 5 Error: {0}", ex.Message);
                Assert.Fail("PIV-160 Scenario 5 Failed. Error: {0}", ex);
            }

        }

        #region Utilities
        private void LogoutAdministrator()
        {
            Thread.Sleep(5000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);

        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private PIV160Data GetData()
        {
            PIV160Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV160.json");
                data = JsonConvert.DeserializeObject<PIV160Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
        #endregion
    }
}
