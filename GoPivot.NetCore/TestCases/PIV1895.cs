﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.Collections.Generic;
using System.Linq;
using GoPivot.NetCore.DataEntities;
using System.IO;
using Newtonsoft.Json;
namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1895", Category = "Challenge")]
    public class PIV1895 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        [TestCase(Description = "PIV-1895 Scenario 1: New Send to All checkbox", TestName = "Scenario 1: New Send to All checkbox")]
        [Order(1)]
        public void Validate_New_Send_To_All_Checkbox()
        {
            try
            {
                LoginAdministrator();
                AdminPage adminPage = new AdminPage(driver);
                var usersTab = adminPage.GetProgramTab("Users");
                if (usersTab == null)
                {
                    Console.WriteLine("Unable to find Users tab");
                    Assert.Fail("Unable to find Users tab");
                }
                else
                {
                    Console.WriteLine("Users Tab loaded");
                    usersTab.Click();
                }
                Thread.Sleep(3000);

                driver.FindElement(By.Id("programUsersNewMessage")).Click();
                Thread.Sleep(2000);
                Console.WriteLine("New Message button clicked");

                Assert.IsTrue(driver.FindElement(By.ClassName("modal-content")).Displayed, "New message modal displayed");
                Console.WriteLine("New Message modal displayed");

                IWebElement checkboxElement = driver.FindElement(By.XPath("//md-checkbox[@ng-model='sendToAllParticipants']"));
                Assert.IsTrue(checkboxElement.Displayed, "Send to all active participants Checkbox participant available");
                Console.WriteLine("Checkbox for send to all active participants available");

                IWebElement checkboxLabelElement = driver.FindElement(By.XPath("//md-checkbox[@ng-model='sendToAllParticipants']/div[@class='md-label']"));
                Assert.IsTrue(checkboxLabelElement.Displayed, "Send to all active participants checkbox label should be displayed");
                Assert.IsTrue(checkboxLabelElement.Text == "Send to all active participants", "Send to all active participants text matching");
                Console.WriteLine("Send to all active participants checkbox label text matching");
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1895 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1895 Scenario 1 Failed. Error: {0}", ex);
            }

        }

        [TestCase(Description = "PIV-1895 Scenario 2: User selects Send to All checkbox", TestName = "Scenario 2: User selects Send to All checkbox")]
        [Order(2)]
        public void Validate_User_Selects_All_Checkbox()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                IWebElement checkboxElement = driver.FindElement(By.XPath("//md-checkbox[@ng-model='sendToAllParticipants']"));
                checkboxElement.Click();
                Console.WriteLine("Send to all active participants checked");
                Thread.Sleep(5000);

                IWebElement inputElement = driver.FindElement(By.XPath("//tags-input[@id='messageTo']//input"));
                Assert.IsTrue(inputElement.GetAttribute("disabled") == "true", "Message To Input Element must be disabled");
                Console.WriteLine("Message To Input element is disabled");

                IWebElement allActiveElement = driver.FindElement(By.XPath("//tags-input[@id='messageTo']//div[@class='tags']/ul/li[contains(@class,'tag-item') and contains(.,'All Active')]"));
                Assert.IsTrue(allActiveElement.Displayed, "Message To textbox should contains 'All Active'");
                Console.WriteLine("Message To textbox contains 'All active' element");

                IWebElement removeActiveElement = driver.FindElement(By.XPath("//tags-input[@id='messageTo']//div[@class='tags']/ul/li[contains(@class,'tag-item')]//a[contains(@class,'remove-button')]"));
                Assert.IsTrue(removeActiveElement.Displayed, "All Active checkbox must contain remove button");
                Console.WriteLine("All Active checkbox contains remove button");
                Thread.Sleep(3000);
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1895 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1895 Scenario 2 Failed. Error: {0}", ex);
            }
        }


        [TestCase(Description = "PIV-1895 Scenario 3: User removes All Active or Unchecks Send to All checkbox", TestName = "Scenario 3: User removes All Active or Unchecks Send to All checkbox")]
        [Order(3)]
        public void Validate_User_Removes_Active_Checkbox()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            try
            {
                IWebElement checkboxElement = driver.FindElement(By.XPath("//md-checkbox[@ng-model='sendToAllParticipants']"));
                checkboxElement.Click();
                Console.WriteLine("Send to all active participants unchecked");
                Thread.Sleep(2000);

                try
                {
                    IWebElement allActiveElement = driver.FindElement(By.XPath("//tags-input[@id='messageTo']//div[@class='tags']/ul/li[contains(@class,'tag-item') and  contains(.,'All Active')]"));
                    Assert.IsFalse(allActiveElement.Displayed, "Message To textbox should NOT contains 'All Active'");
                }
                catch
                {
                    Console.WriteLine("Message To textbox does NOT contain 'All active' element");
                }

                IWebElement inputElement = driver.FindElement(By.XPath("//tags-input[@id='messageTo']//input"));
                Assert.IsTrue(string.IsNullOrEmpty(inputElement.GetAttribute("disabled")), "Message To Input Element must be enabled");
                Console.WriteLine("Message To Input element is enabled");

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1895 Scenario 3 Error: {0}", ex.Message);
                Assert.Fail("PIV-1895 Scenario 3 Failed. Error: {0}", ex);
            }
        }

        #region Utilities
        private void LogoutAdministrator()
        {
            Thread.Sleep(5000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);

        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
