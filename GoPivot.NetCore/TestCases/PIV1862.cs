﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.Collections.Generic;
using System.Linq;
using GoPivot.NetCore.DataEntities;
using System.IO;
using Newtonsoft.Json;
using OpenQA.Selenium.Interactions;
namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1862", Category = "Assessment")]
    public class PIV1862 : BaseStepDefinition
    {
        public PIV1862Data Data { get; set; }
        public bool IsSuccess { get; set; }
        [TestCase(TestName = "Scenario 1: Display Assessments Detail page")]
        [Order(1)]
        public void Validate_Display_Assessment_Detail_Page()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAdministrator();
                AdminPage adminPage = new AdminPage(driver);
                var assessmentTab = adminPage.GetProgramTab("Assessments");
                if (assessmentTab == null)
                {
                    Console.WriteLine("Unable to find Assessment tab");
                    Assert.Fail("Unable to find Assessment tab");
                }
                assessmentTab.Click();
                Thread.Sleep(3000);

                driver.FindElement(By.XPath("//button[contains(.,'Add Assessment')]")).Click();
                Console.WriteLine("Add Assessment button clicked");

                Thread.Sleep(3000);

                driver.FindElement
                        (
                           By.XPath($"//div[contains(@class,'assessmentTemplate')]/div[contains(@class,'templateAssessmentName') and contains(.,'{Data.ChallengeTemplate}')]")
                        ).Click();

                Console.WriteLine("Assesment Template: {0} selected", Data.ChallengeTemplate);
                Thread.Sleep(1000);
                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();
                Thread.Sleep(2000);
                Assert.IsTrue(
                    driver.FindElement(By.XPath("//wizard-step-display//div[@class='bottomRow']/div[contains(@class,'selected')]")).Text == "Details", "Details step must be active");
                Console.WriteLine("Detaills tab been activated");


                Assert.IsTrue(driver.FindElement(By.Id("assessmentName")).Displayed, "Assessment Name textbox must be displayed");
                Assert.IsTrue(driver.FindElement(By.Name("startDate")).Displayed, "Start Date textbox must be displayed");
                Assert.IsTrue(driver.FindElement(By.Name("endDate")).Displayed, "End Date textbox must be displayed");
                Assert.IsTrue(driver.FindElement(By.Id("disclaimer")).Displayed, "Disclaimer textbox must be displayed");
                Console.WriteLine("All fields available");

                var nextButton = driver.FindElement(By.XPath("//button[contains(.,'Next')]"));
                string disabledAttribute = nextButton.GetAttribute("disabled");
                Assert.IsTrue(disabledAttribute == "true", "Next button must be disabled");

                Console.WriteLine("Next button is disabled");
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1862 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1862 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 2: Assessment Name")]
        [Order(2)]
        public void Validate_Assessment_Name()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                var assessessmentNameLabelElement = driver.FindElement(By.XPath("//label[@for='assessmentName']"));
                string placeholderText = assessessmentNameLabelElement.Text;
                Assert.IsTrue(placeholderText == "Assessment Name *", "Assesment name placeholder been set");


                var assessmentNameTextElement = driver.FindElement(By.Id("assessmentName"));
                int maxLength = Convert.ToInt32(assessmentNameTextElement.GetAttribute("maxlength"));
                Assert.IsTrue(maxLength == Data.AssessmentNameTextBoxLength, "Assessment Name text limit must be {0}", Data.AssessmentNameTextBoxLength);
                Console.WriteLine("Assessment Name Textbox has limit of {0}", maxLength);

                assessmentNameTextElement.SendKeys("Assessment Test");

                var nextButton = driver.FindElement(By.XPath("//button[contains(.,'Next')]"));
                string disabledAttribute = nextButton.GetAttribute("disabled");
                Assert.IsTrue(disabledAttribute == "true", "Next button must be disabled");
                Console.WriteLine("Next Button is disabled");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1862 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1862 Scenario 2 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 3: Start/End Date/Time")]
        [Order(3)]
        public void Validate_Start_End_Time()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                var startDateLabel = driver.FindElement(By.Id("startDateLabel"));
                Assert.IsTrue(startDateLabel.Text == Data.StartDateLabelText, "Start Date label must be matching");
                Console.WriteLine("Start Date Label matching");


                var endDateLabel = driver.FindElement(By.XPath($"//label[contains(.,'{Data.EndDateLabelText}')]"));
                Assert.IsTrue(endDateLabel.Displayed, "End Date label must be matching");
                Console.WriteLine("End Date Label matching");

                var startDateTextElement = driver.FindElement(By.XPath("//md-datepicker[@ng-model='model.startDate']//input"));
                var endDateTextElement = driver.FindElement(By.XPath("//md-datepicker[@ng-model='model.endDate']//input"));
                Assert.IsTrue(string.IsNullOrEmpty(startDateTextElement.Text), "Start Date text must be empty");
                Console.WriteLine("Start Date textbox is empty");
                Assert.IsTrue(string.IsNullOrEmpty(endDateTextElement.Text), "End Date text must be empty");
                Console.WriteLine("End Date textbox is empty");

                startDateTextElement.SendKeys(DateTime.Now.ToShortDateString());
                Thread.Sleep(2000);
                endDateTextElement.SendKeys(DateTime.Now.AddDays(5).ToShortDateString());
                Thread.Sleep(2000);

                var nextButton = driver.FindElement(By.XPath("//button[contains(.,'Next')]"));
                string disabledAttribute = nextButton.GetAttribute("disabled");

                Assert.IsTrue(string.IsNullOrEmpty(disabledAttribute) || disabledAttribute == "false", "Next button must be enabled");
                Console.WriteLine("Next Button is enabled now");

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1862 Scenario 3 Error: {0}", ex.Message);
                Assert.Fail("PIV-1862 Scenario 3 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 4: Assessment Disclaimer")]
        [Order(4)]
        public void Validate_Assessment_Disclaimer()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 3 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                var disclaimerLabelElement = driver.FindElement(By.XPath("//label[@for='disclaimer']"));
                string placeholderText = disclaimerLabelElement.Text;
                Assert.IsTrue(placeholderText == Data.DisclaimerPlaceholderText, "Disclaimer placeholder been set");


                var disclaimerTextElement = driver.FindElement(By.Id("disclaimer"));
                int maxLength = Convert.ToInt32(disclaimerTextElement.GetAttribute("maxlength"));
                Assert.IsTrue(maxLength == Data.DisclaimerTextAreaLength, "Disclaimer text limit must be {0}", Data.DisclaimerTextAreaLength);
                Console.WriteLine("Disclaimer Textbox has limit of {0}", maxLength);

                disclaimerTextElement.SendKeys("This is testing");
                var nextButton = driver.FindElement(By.XPath("//button[contains(.,'Next')]"));
                string disabledAttribute = nextButton.GetAttribute("disabled");

                Assert.IsTrue(string.IsNullOrEmpty(disabledAttribute) || disabledAttribute == "false", "Next button must be enabled");
                Console.WriteLine("Next Button is enabled now");

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1862 Scenario 4 Error: {0}", ex.Message);
                Assert.Fail("PIV-1862 Scenario 4 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 5: Back > Pick Assessment")]
        [Order(5)]
        public void Validate_Back_Pick_Assessment()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 4 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                driver.FindElement(By.XPath("//button[contains(.,'Back')]")).Click();
                Console.WriteLine("Back button clicked");

                Assert.IsTrue(
                driver.FindElement
                        (
                           By.XPath($"//div[contains(@class,'assessmentTemplate')]/div[contains(@class,'templateAssessmentName') and contains(.,'{Data.ChallengeTemplate}')]")
                        ).Enabled, "Templates must be displayed");

                Console.WriteLine("Pick Assessment templates been displayed");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1862 Scenario 5 Error: {0}", ex.Message);
                Assert.Fail("PIV-1862 Scenario 5 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 6: Next > Configure")]
        [Order(6)]
        public void Validate_Next_Configure()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 5 not completed"); return;
            }
            try
            {


                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();
                Console.WriteLine("Next button clicked");

                Thread.Sleep(1000);
                Assert.IsTrue(
                 driver.FindElement(By.XPath("//wizard-step-display//div[@class='bottomRow']/div[contains(@class,'selected')]")).Text ==Data.AssessmentSteps.Step2.BreadcrumbText, "Details step must be active");
                Console.WriteLine("Detaills tab been activated");

                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();
                Console.WriteLine("Next button clicked");

                Assert.IsTrue(
                 driver.FindElement(By.XPath("//wizard-step-display//div[@class='bottomRow']/div[contains(@class,'selected')]")).Text == Data.AssessmentSteps.Step3.BreadcrumbText, "Configure step must be active");
                Console.WriteLine("Configure tab been activated");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1862 Scenario 6  Error: {0}", ex.Message);
                Assert.Fail("PIV-1862 Scenario 6 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 7: Return to Assessment List from wizard")]
        [Order(7)]
        public void Validate_Return_To_Assessment_List()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 6 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//button[contains(.,'Back')]")).Click();
                Console.WriteLine("Back button clicked");


                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//button[contains(.,'Back')]")).Click();
                Console.WriteLine("Back button clicked");

                Thread.Sleep(2000);
                driver.FindElement(By.LinkText(Data.ReturnToAssessmentLinkText)).Click();
                Console.WriteLine($"{Data.ReturnToAssessmentLinkText} link clicked");

                Thread.Sleep(2000);

                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'modal-dialog')]")).Displayed,
                    "Modal Popup Displayed");
                Console.WriteLine("Modal Popup displayed");

                Assert.IsTrue(driver.FindElement(By.XPath("//button[contains(.,'Leave without saving')]")).Displayed, "Leave without button must be visible");
                Console.WriteLine("Button - Leave without saving displayed");
                Assert.IsTrue(driver.FindElement(By.XPath("//button[contains(.,'Stay on page')]")).Displayed, "Stay on page button must be visible");
                Console.WriteLine("Button - Stay on page displayed");

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1862 Scenario 7 Error: {0}", ex.Message);
                Assert.Fail("PIV-1862 Scenario 7 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 8: Return > Stay on Page")]
        [Order(8)]
        public void Validate_Return_Stay_On_Page()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 7 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                 
                driver.FindElement(By.XPath("//button[contains(.,'Stay on page')]")).Click();
                Console.WriteLine("Button - Stay on page clicked");

                try
                {
                    Assert.IsFalse(driver.FindElement(By.XPath("//div[contains(@class,'modal-dialog')]")).Displayed,
                    "Modal Popup should not be displayed");
                }
                catch
                {
                    Console.WriteLine("Modal popup is not visible");
                }
                Assert.IsTrue(driver.FindElement(By.XPath($"//form/ng-include/div[contains(.,'{Data.AssessmentSteps.Step1.PageSummary}')]")).Displayed, "Page Summary {0} must be displayed", Data.AssessmentSteps.Step1.PageSummary);
                Console.WriteLine($"Page Summary {Data.AssessmentSteps.Step1.PageSummary} displayed");

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1862 Scenario 8 Error: {0}", ex.Message);
                Assert.Fail("PIV-1862 Scenario 8 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 9: Return > Leave without saving")]
        [Order(9)]
        public void Validate_Return_Leave_Without_Saving()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 8 not completed"); return;
            }
            try
            {
                string currentUrl = driver.Url;
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                driver.FindElement(By.LinkText(Data.ReturnToAssessmentLinkText)).Click();
                Console.WriteLine($"{Data.ReturnToAssessmentLinkText} link clicked");

                Thread.Sleep(2000);

                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'modal-dialog')]")).Displayed,
                    "Modal Popup Displayed");
                Console.WriteLine("Modal Popup displayed");

                driver.FindElement(By.XPath("//button[contains(.,'Leave without saving')]")).Click();
                Console.WriteLine("Leave without saving button clicked");

                Thread.Sleep(3000);
                string newUrl = driver.Url;
                Assert.IsFalse(newUrl == currentUrl, "Page should be redirected to listing page");
                Console.Write("Page redirected to listing page");
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1862 Scenario 9 Error: {0}", ex.Message);
                Assert.Fail("PIV-1862 Scenario 9 Failed. Error: {0}", ex);
            }
        }


        #region Utilities
        private void LogoutAdministrator()
        {
            Thread.Sleep(5000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);

        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private PIV1862Data GetData()
        {
            PIV1862Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1862.json");
                data = JsonConvert.DeserializeObject<PIV1862Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
        #endregion
    }
}
