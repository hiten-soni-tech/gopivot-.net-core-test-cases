﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1973", Category = "Activity")]
    public class PIV1973 : BaseStepDefinition
    {
        [TestCase(TestName = "Validate Program Activity DACC and Activity Point Source Column")]
        public void Validate_Program_Activity_Labels()
        {
            try
            {
                LoginAndSelectActivity();

                driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
                Console.WriteLine("Add Activity button clicked");

                Thread.Sleep(5000);
                GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.Id("name"))).SendKeys("PIV-1973");
                ComboBoxHelper.SelectElement(By.XPath("//select[@ng-model = 'model.activitySectionId']"),
            "Get Started");
                ComboBoxHelper.SelectElement(By.XPath("//select[@ng-model = 'model.activityCategoryId']"), "Learning");
                Thread.Sleep(5000);
                driver.FindElement(By.Id("activeDateRange")).Click();
                Thread.Sleep(2000);
                var element = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[contains(@class,'dropup') and contains(@style,'display: block')]/div[contains(@class,'ranges')]/ul/li[contains(.,'{0}')]", "6 Months"))));
                element.Click();
                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();
                Thread.Sleep(2000);

                string programActivityDACC = driver.FindElement(By.XPath("//div[@class='activity-dacc']/ul/li[1]")).Text;
                string activityPointSource = driver.FindElement(By.XPath("//div[@class='activity-dacc']/ul/li[2]")).Text;

                Assert.IsTrue(programActivityDACC.Contains("Program Activity DACC:"), "Program Activity DACC Label must be valid");
                Assert.IsTrue(activityPointSource.Contains("Activity Point Source:"), "Program Activity DACC Label must be valid");

                string[] programActivityDaccSplit = programActivityDACC.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                string[] activityPointSourceSplit = activityPointSource.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                if (programActivityDACC.Length <= 1)
                {
                    Assert.Warn("No Program Activity DACC Defined.");
                }

                if (activityPointSourceSplit.Length <= 1)
                {
                    Assert.Warn("No Activity Point Source Defined.");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1973 Error: {0}", ex.Message);
                Assert.Fail("PIV-1973 Failed. Error: {0}", ex);
            }
        }

        private static void LoginAndSelectActivity()
        {
            Thread.Sleep(3000);
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);
            var activitiesTab = adminPage.GetProgramTab("Activities");
            if (activitiesTab == null)
            {
                Console.WriteLine("Unable to find activities tab");
                Assert.Fail("Unable to find activities tab");
            }
            else
            {
                Console.WriteLine("Activities Tab loaded");
                activitiesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(6000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

        }
        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}
