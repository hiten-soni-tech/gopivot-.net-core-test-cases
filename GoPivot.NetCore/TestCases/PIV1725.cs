﻿
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1725 View Challenge Progress Tab (Post-join, Post-start) - Numeric Metrics", Category = "Challenge")]
    public class PIV1725 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        private PIV1725Data Data { get; set; }
        [Order(1)]
        [TestCase(TestName = "Scenario 0: Create New Custom Challenge", Description = "PIV-1725 Create New Challenge")]
        public void PreRequisite_Create_New_Challenge()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                LoginAdministrator();
                AdminPage adminPage = new AdminPage(driver);
                var challengeTab = adminPage.GetProgramTab("Challenges");
                if (challengeTab == null)
                {
                    Console.WriteLine("Unable to find challenges tab");
                    Assert.Fail("Unable to find challenges tab");
                }
                else
                {
                    Console.WriteLine("Challenge Tab loaded");
                    challengeTab.Click();
                }

                Thread.Sleep(5000);

                adminPage.GetAddChallengeButton();
                if (adminPage.AddChallengeButton != null)
                {
                    adminPage.AddChallengeButton.Click();
                    Console.WriteLine("Add Challenge Button clicked");
                    Thread.Sleep(2000);
                }
                else
                {
                    Console.WriteLine("Cannot add new challenge");
                    Assert.Fail("cannot add new challenge");
                }

                Thread.Sleep(2000);

                var challengeCategory = adminPage.GetChallengeCategory("Custom");
                if (challengeCategory != null)
                {
                    challengeCategory.Click();
                    Console.WriteLine("Custom Challenge type selected");
                }
                else
                {
                    Console.WriteLine("Custom Challenge Type not available");
                    Assert.Fail("Custom Challenge Type not available");
                }

                Thread.Sleep(2000);
                var challengeType = adminPage.GetChallengeType("Custom");
                string errorMessage;
                if (challengeType != null)
                {
                    challengeType.Click();
                    Console.WriteLine("Custom Challenge selected");
                }
                else
                {
                    errorMessage = "Custom Challenge not found";
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }


                AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);

                string randomNumber = new Random().Next(1000).ToString();
                string challengename = Data.Challenge.ChallengeName.GetDynamicText();
                string startdate = Data.Challenge.StartDate.GetDynamicText();
                string enddate = Data.Challenge.EndDate.GetDynamicText();
                Data.GeneratedChallengeName = challengename;



                bool success = addNewChallengePage.FillStepForCustom(challengename, startdate, enddate, Data.Challenge.CustomChallengeActionText, Data.Challenge.CustomChallengeGoal.ToString(), Data.Challenge.CustomChallengeUnit, Data.Challenge.CustomChallengeTime, Data.Challenge.GoalText, out errorMessage);
                if (success)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                    addNewChallengePage.Step1_NextButton.Click();
                }
                else
                {
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                var participantCount = addNewChallengePage.Step2_ParticipantList;
                if (participantCount != null && participantCount.Count > 0)
                {
                    Console.WriteLine("Participants available to select");
                }
                else
                {
                    Console.WriteLine("No Participants available");
                    Assert.Fail("No Participants available");
                }


                success = addNewChallengePage.AddChallengers(Data.Challenge.ChallengerName, out errorMessage);
                if (success)
                {
                    Console.WriteLine("Participant {0} added to challenge", Data.Challenge.ChallengerName);
                }
                else
                {
                    Console.Error.WriteLine("Failed to add {0} challenger. Error: {1}", Data.Challenge.ChallengerName, errorMessage);
                    Assert.Fail(errorMessage);
                }
                addNewChallengePage.Step2_CreateButton.Click();
                Console.WriteLine("Create Challenge button clicked");
                WebDriverWait wait = GetWebDriverWait();
                var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
                if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
                {
                    Console.WriteLine("Challenge {0} added successfully", challengename);
                    Assert.IsTrue(true);
                }
                else
                {
                    errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }
                Thread.Sleep(3000);
                LogoutAdministrator();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("Creating Custom Challenge Failed. Error: {0}", ex);

            }
        }

        [TestCase(TestName = "Scenario 1: Display goal")]
        [Order(2)]
        public void Display_Goal()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginParticipant();
                Thread.Sleep(5000);
                SelectChallenge();
                By loadingImage = By.TagName("http-busy");
                try
                {
                    IWebElement joinButtonElement = driver.FindElement(By.XPath("//a[contains(.,'Join!')]"));
                    if (joinButtonElement != null && joinButtonElement.Displayed)
                    {
                        Thread.Sleep(1000);
                        GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));
                        joinButtonElement.Click();
                        Console.WriteLine("Join Button available and clicked to join this challenge");
                    }
                    else
                    {
                        Console.WriteLine("Join button not available or already joined");
                    }
                }
                catch
                {
                    Console.WriteLine("Participant already joined");
                }
                Thread.Sleep(3000);
                SelectChallenge();
                Thread.Sleep(3000);

                IWebElement progressTabElement = driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Progress')]"));
                Assert.IsTrue(progressTabElement.Displayed, "Progress Tab must be displayed");
                Console.WriteLine("Progress tab displayed");

                Assert.IsTrue(driver.FindElement(By.XPath("//h2[@class='challengeDetailContentHeader']")).Text == "Goal", "Goal Header must be matching");
                Console.WriteLine("Goal Header matching");

                var goalTextElement = driver.FindElement(By.XPath("//div[@class='ng-scope']/h2[contains(.,'Goal')]/following-sibling::p[1]"));
                if (goalTextElement != null && goalTextElement.Displayed && goalTextElement.Text.ToLower().Contains(Data.Challenge.GoalText.ToLower()))
                {
                    Console.WriteLine("Goal {0} matching", Data.Challenge.GoalText);
                    Assert.IsTrue(goalTextElement.Text.ToLower().Contains(Data.Challenge.GoalText.ToLower()), "Goal must be matching");
                }
                else
                {
                    Console.Error.WriteLine("Failed to load goal details");
                    Assert.Fail("Failed to validate goal. Error: Element not found or text not matching");
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1725 Scenario 2 - Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1725 Scenario 2 - Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 2: Display log button")]
        [Order(3)]
        public void Display_Log_It_Button()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                Assert.IsTrue(driver.FindElement(By.XPath("//button[contains(.,'Log It!')]")).Displayed, "Log It! Button must be displayed");
                Console.WriteLine("Log It! Button displayed");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1725 Scenario 3 - Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1725 Scenario 3 - Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 3: Display progress stats - Whole Number or Decimal Metric Associated")]
        [Order(4)]
        public void Display_Progress_Stats()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            try
            {
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                IWebElement goalTextBoxElement = driver.FindElement(By.Id("numericValue"));
                goalTextBoxElement.Clear();
                goalTextBoxElement.SendKeys(Data.Challenge.CustomChallengeGoal.ToString());
                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//button[contains(.,'Log It!')]")).Click();
                Thread.Sleep(4000);


                Assert.IsTrue(driver.FindElement(By.XPath("//div[@class = 'numericGoalMetNumber']/div")).Text == "Yes", "Goals met must be Yes");
                Console.WriteLine("Goals met text is Yes");

                Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'numericGoalMetUnit')]")).Text == "Total Time(s)", "Goal Unit must be Total Time(s)");
                Console.WriteLine("Goal Unit is Total Time(s)");

                Int32.TryParse(driver.FindElement(By.XPath("//div[@ng-if = 'challenge.flagNotPercent || challenge.flagOneTimePercent']/div[contains(@class,'numericGoalMetNumber')]")).Text, out int totalTime);
                Assert.IsTrue(totalTime > 0, "Total Time must be greater than 0");
                Console.WriteLine("Total Time is greater than 0 i.e. {0}", totalTime);
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1725 Scenario 4 - Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1725 Scenario 4 - Failed. Error: {0}", ex);
            }
        }
        
        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private void SelectChallenge()
        {
            IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
            if (challengeTab != null && challengeTab.Displayed)
            {
                challengeTab.Click();
                Console.WriteLine("Challenge Tab Clicked");
            }
            By loadingImage = By.TagName("http-busy");
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));
            Thread.Sleep(2000);

            var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", Data.GeneratedChallengeName))));
            if (challenge != null && challenge.Displayed)
            {
                challenge.Click();
                Console.WriteLine("{0} available challenge found", Data.GeneratedChallengeName);
            }
            else
            {
                Console.Error.WriteLine("{0} available challenge not found", Data.GeneratedChallengeName);
                Assert.Fail("{0} available challenge not found", Data.GeneratedChallengeName);
            }

        }

        private PIV1725Data GetData()
        {
            PIV1725Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1725.json");
                data = JsonConvert.DeserializeObject<PIV1725Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");
            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }

            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            if (!adminPage.EditClient.Displayed)
            {
                Console.WriteLine("Clients Details not found");
                Assert.Fail("Clients Details not found");

            }
            else
            {
                Console.WriteLine("Client Details page loaded");
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(2000);

        }

        private void LogoutAdministrator()
        {
            Thread.Sleep(3000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            Thread.Sleep(3000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

    }
}
