﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.BaseClasses;
using System.Linq;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1596", Category = "Challenge")]
    public class PIV1596 : BaseStepDefinition
    {
        [TestCase(Description = "PIV-1596 Send challenge invitations to the team participants", TestName = "Send challenge invitations to the team participants")]
        public void Validate_Challenge_Invitation()
        {

            string errorMessage = string.Empty;
            try
            {
                var data = GetData();
                if (data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }


                #region Create new Challenge 
                LoginAndSelectChallenge(); ;
                AdminPage adminPage = new AdminPage(driver);
                var challengeCategory = adminPage.GetChallengeCategory("Custom");
                if (challengeCategory != null)
                {
                    challengeCategory.Click();
                    Console.WriteLine("Custom Challenge type selected");
                }
                else
                {
                    Console.WriteLine("Custom Challenge Type not available");
                    Assert.Fail("Custom Challenge Type not available");
                }
                Thread.Sleep(2000);
                var challengeType = adminPage.GetChallengeType("Custom");
                if (challengeType != null)
                {
                    challengeType.Click();
                    Console.WriteLine("Custom Challenge selected");
                }
                else
                {
                    errorMessage = "Custom Challenge not found";
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }
                AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
                string randomNumber = new Random().Next(1000).ToString();
                string challengename = data.NewChallenge.ChallengeName;
                challengename = challengename.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());
                challengename = challengename.Replace("{randomnumber}", randomNumber);

                string startDate = data.NewChallenge.StartDate;
                startDate = startDate.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());
                DateTime reference = DateTime.Now;
                DateTime firstDayThisMonth = new DateTime(reference.Year, reference.Month, 1);
                DateTime firstDayPlusTwoMonths = firstDayThisMonth.AddMonths(2);
                DateTime lastDayNextMonth = firstDayPlusTwoMonths.AddDays(-1);
                DateTime endOfLastDayNextMonth = firstDayPlusTwoMonths.AddTicks(-1);
                string enddate = data.NewChallenge.EndDate.Replace("{endofnextmonth}", endOfLastDayNextMonth.ToShortDateString());
                bool success = addNewChallengePage.FillStepForCustom(challengename, startDate, enddate, data.NewChallenge.CustomChallengeActionText, data.NewChallenge.CustomChallengeGoal.ToString(), data.NewChallenge.CustomChallengeUnit, data.NewChallenge.CustomChallengeTime, data.NewChallenge.GoalText, out errorMessage);
                if (success)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                    addNewChallengePage.Step1_NextButton.Click();
                }
                else
                {
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                Thread.Sleep(1000);
                var participantCount = addNewChallengePage.Step2_ParticipantList;
                if (participantCount != null && participantCount.Count > 0)
                {
                    Console.WriteLine("Participants available to select");
                }
                else
                {
                    Console.WriteLine("No Participants available");
                    Assert.Fail("No Participants available");
                }

                success = addNewChallengePage.AddChallengers(data.NewChallenge.ChallengerName, out errorMessage);
                if (success)
                {
                    Console.WriteLine("Participant {0} added to challenge", data.NewChallenge.ChallengerName);
                }
                else
                {
                    Console.WriteLine("Failed to add {0} challenger. Error: {1}", data.NewChallenge.ChallengerName, errorMessage);
                    Assert.Fail(errorMessage);
                }
                Thread.Sleep(3000);
                addNewChallengePage.Step2_CreateButton.Click();
                Console.WriteLine("Create Challenge button clicked");


                WebDriverWait wait = GetWebDriverWait();
                var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
                if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
                {
                    string message = string.Format("Challenge {0} added successfully", challengename);
                    Console.WriteLine(message);
                    Assert.IsTrue(true);
                }
                else
                {
                    errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                    Console.Error.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }
                Thread.Sleep(2000);
                LogoutAdministrator();
                Thread.Sleep(2000);
                #endregion

                LoginParticipant();
                Thread.Sleep(6000);
               // string challengename = " Custom 12/1/2018-388";
                driver.FindElement(By.XPath("//div[contains(@class,'desktop')]//ul[contains(@class,'side-nav')]/li[contains(.,'Inbox')]")).Click();

                Thread.Sleep(2000);
                string subjectText = data.InboxContent.Subject.Replace("{challengename}", challengename);
                Console.WriteLine(subjectText);
                IWebElement subjectElement = driver.FindElement(By.XPath("//p[contains(@class,'subject')]"));
                Console.WriteLine(subjectElement.Text);
                Assert.IsTrue(subjectText.Equals(subjectElement.Text), "Inbox Subjects are matching");
                Console.WriteLine("Inbox subject is matching {0}", subjectText);

                Assert.IsTrue(driver.FindElement(By.XPath("/html/body/div/div/div[2]/div[2]/div/div[2]/div[2]/div/div/div/ng-include[2]/div/div[2]/set-link-target/div/div/a/span")).Displayed, "View Challenge Details button must be displayed");
                Console.WriteLine("View Challenge Details buttton displayed");

                Console.WriteLine("Pass: Inbox contains invitation challenge for challenge {0}", challengename);

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1596 Pre-requisites Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1596 Pre-requisites Failed. Error: {0}", ex);
            }
        }

        #region Utilities
        private void LogoutAdministrator()
        {
            Thread.Sleep(3000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private static void LoginAndSelectChallenge()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(1000);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);
            var challengeTab = adminPage.GetProgramTab("Challenges");
            if (challengeTab == null)
            {
                Console.WriteLine("Unable to find challenges tab");
                Assert.Fail("Unable to find challenges tab");
            }
            else
            {
                Console.WriteLine("Challenges Tab loaded");
                challengeTab.Click();
            }
            Thread.Sleep(3000);
            adminPage.GetAddChallengeButton();
            if (adminPage.AddChallengeButton != null)
            {
                adminPage.AddChallengeButton.Click();
                Console.WriteLine("Add Challenge Button clicked");
                Thread.Sleep(2000);
            }
            else
            {
                Console.WriteLine("Cannot add new challenge");
                Assert.Fail("cannot add new challenge");
            }

            Thread.Sleep(2000);
        }
        private void LogoutParticipant()
        {
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private PIV1596Data GetData()
        {
            PIV1596Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1596.json");
                data = JsonConvert.DeserializeObject<PIV1596Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }


}
