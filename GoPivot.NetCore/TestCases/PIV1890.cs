﻿
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;
using GoPivot.NetCore.ComponentHelper;
using System.Collections.Generic;
namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1890", Category = "Program Guide")]
    public class PIV1890 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        [Order(1)]
        [TestCase(TestName = "Scenario 1: A user guide is available", Description = "Scenario 1: A user guide is available")]
        public void Validate_User_Guide_Is_Available()
        {
            try
            {
                LoginParticipant();
                Thread.Sleep(8000);
                IWebElement programGuideTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Program Guide")));
                programGuideTab.Click();
                Thread.Sleep(3000);

                Assert.IsTrue(
                    driver.FindElement(By.XPath("//h2[@ng-if='programGuide.userGuidePDF']")).Text.Equals("Program User's Guide"), "Program User's Guide header must be avaialable");
                Console.WriteLine("Program User's Guide Header available");

                Assert.IsTrue(
    driver.FindElement(By.XPath("//p[@ng-if='programGuide.userGuidePDF']")).Text.Equals("View or download your program User's Guide here."), "View or download your program User's Guide here section must be avaialable");
                Console.WriteLine("View or download your program User's Guide here link available");

                IWebElement herelink = driver.FindElement(By.LinkText("here"));
                Assert.IsTrue(
                herelink.Displayed, "Here link must be displayed");
                Console.WriteLine("Here link must be displayed");
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1890 Scenario 1 Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1890 Scenario 1 Test case failed. Error: {0}", ex);

            }
        }

        [Order(2)]
        [TestCase(TestName = "Scenario 2: User clicks the User's Guide link")]
        public void Validate_Clicks_User_Guide_Link()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {

                IWebElement element = driver.FindElement(By.LinkText("here"));

                var url = element.GetAttribute("href");
                var result = Uri.TryCreate(url, UriKind.Absolute, out var uriResult)
                              && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                if (!string.IsNullOrEmpty(url))
                {
                    Assert.IsTrue(result, "URL must be valid");
                    Assert.IsTrue(element.GetAttribute("target") == "_blank", "URL should have _blank target");
                    Console.WriteLine("{0} is valid URL contains _blank target", url);

                    if (url.Contains("blob"))
                    {
                        Console.WriteLine("User guide loaded from blob container");
                    }
                    else
                    {
                        Console.WriteLine("User guide not loaded from blob container");
                        Assert.Warn("User guide not loaded from blob container");
                    }
                }
                else
                {
                    Console.Error.WriteLine("No URL exists for link {0}", element.Text);
                    Assert.Fail("No URL exists for link {0}", element.Text);
                }

                LogoutParticipant();
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1890 Scenario 2 Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1890 Scenario 2 Test case failed. Error: {0}", ex);

            }
        }

        [Order(3)]
        [TestCase(TestName = "Scenario 3: A user's guide is not available", Ignore = "true", IgnoreReason = "Feature cannot be tested for this user")]
        public void Validate_User_Guide_Not_Available()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
        }

        private static WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private void LoginParticipant()
        {
            Thread.Sleep(3000);
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private void LogoutParticipant()
        {
            Thread.Sleep(8000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
            Thread.Sleep(3000);
        }
    }
}
