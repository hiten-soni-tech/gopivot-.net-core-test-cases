﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;
using GoPivot.NetCore.ComponentHelper;
namespace GoPivot.NetCore.TestCases
{

    [TestFixture(TestName = "PIV-1703", Category = "Challenge")]
    public class PIV1703 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1703Data Data { get; set; }

        [TestCase(TestName = "Pre-requisite - Create new Wellness Challenge")]
        [Order(1)]
        public void Create_New_Wellness_Challenge()
        {
            try
            {

                if (Data == null)
                {
                    Data = GetData();
                }

                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAndSelectChallenge();
                CreateNewChallenge();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1703 Pre-requisite Error: {0}", ex.Message);
                Assert.Fail("PIV-1703 Pre-requisite Failed. Error: {0}", ex);
            }
        }



        [TestCase(TestName = "Scenario 1: Admin edits a challenge that has already started")]
        [Order(2)]
        public void Validate_Edit_Challenge()
        {
            if (!IsSuccess)
            {
                Assert.Warn("First Scenario not completed"); return;
            }
            string errorMessage = string.Empty;
            try
            {
                
                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")).Click();
                Thread.Sleep(3000);


                IList<IWebElement> challengeCategories = driver.FindElements(By.XPath("//button[contains(@class,'categoryButton')]"));
                foreach (var category in challengeCategories)
                {
                    string disabledAttribute = category.GetAttribute("disabled");
                    Console.WriteLine("{0} Challenge Category has Disabled: {1}", category.Text, disabledAttribute);
                    Assert.IsTrue(disabledAttribute == "true", "{0} Challenge Category must be disabled", category.Text);
                }
                Console.WriteLine("All Challenge Categories are disabled");


                IList<IWebElement> challengeTemplates = driver.FindElements(By.XPath("//div[contains(@class,'challengeTemplate')]"));
                foreach (var template in challengeTemplates)
                {
                    string disabledAttribute = template.GetAttribute("disabled");
                    Console.WriteLine("{0} Challenge Template has Disabled: {1}", template.Text, disabledAttribute);
                    Assert.IsTrue(disabledAttribute == "true", "{0} Challenge Template must be disabled", template.Text);
                }
                Console.WriteLine("All Challenge Templates are disabled");

                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();
                Thread.Sleep(3000);
                Console.WriteLine("Next button clicked");


                AddNewChallengePage addChallengePage = new AddNewChallengePage(driver);
                string challengename = Data.CustomChallenge.ChallengeName.GetDynamicText();
                string startdate = Data.CustomChallenge.StartDate.GetDynamicText();
                string enddate = Data.CustomChallenge.EndDate.GetDynamicText();

                Assert.IsTrue(driver.FindElement(By.XPath("//md-datepicker[@ng-model='model.startDateTime']")).GetAttribute("disabled") == "true", "Start Date datepicker must be disabled");
                Console.WriteLine("Start Date Datepicker is disabled");

                string endDateDisabled = driver.FindElement(By.XPath("//md-datepicker[@ng-model='model.endDateTime']")).GetAttribute("disabled");
                Assert.IsTrue(string.IsNullOrEmpty(endDateDisabled) || endDateDisabled == "false", "End Date datepicker element must be enabled");
                Console.WriteLine("End Date Datepicker is enabled");


                Assert.IsTrue(driver.FindElement(By.XPath("//input[@ng-model='model.threshold']")).GetAttribute("disabled") == "true", "Threshhold element must be disabled");
                Console.WriteLine("Threshhold element is disabled");

                Assert.IsTrue(driver.FindElement(By.XPath("//select[@ng-model='model.frequency']")).GetAttribute("disabled") == "true", "Threshhold Frequency element must be disabled");
                Console.WriteLine("Threshhold Frequency element is disabled");

                Assert.IsTrue(driver.FindElement(By.Id("goal")).GetAttribute("disabled") == "true", "Goal element must be disabled");
                Console.WriteLine("Goal element is disabled");

                addChallengePage.ChallengeName.Clear();
                addChallengePage.ChallengeName.SendKeys(challengename);
                Console.WriteLine("Challenge name updated to {0}", challengename);

                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();
                Thread.Sleep(3000);
                Console.WriteLine("Next button clicked");


                IList<IWebElement> participantTypeElements = driver.FindElements(By.XPath("//button[contains(@class,'categoryButton')]"));
                foreach (var participantType in participantTypeElements)
                {
                    string disabledAttribute = participantType.GetAttribute("disabled");
                    Console.WriteLine("{0} Participant Type has Disabled: {1}", participantType.Text, disabledAttribute);
                    Assert.IsTrue(disabledAttribute == "true", "{0} Participant Type must be disabled", participantType.Text);
                }
                Console.WriteLine("All Participant Types are disabled");

                addChallengePage.Step2_SaveButton.Click();
                Console.WriteLine("Save Button clicked");

                Thread.Sleep(3000);

                var newChallengeText = driver.FindElement(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")).Text;
                Assert.IsTrue(newChallengeText == challengename, "Challenge name must be {0}", challengename);
                Console.WriteLine("Challenge name updated successfully");

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1703 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1703 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [TestCase(TestName = "Scenario 2: Validation error when End Date/Time is prior to the current date/time")]
        [Order(2)]
        public void Validate_Validation_Error_For_End_Date()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            string errorMessage = string.Empty;
            try
            {
              
                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")).Click();
                Thread.Sleep(3000);

                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();
                Thread.Sleep(3000);
                Console.WriteLine("Next button clicked");


                string enddate = DateTime.Now.AddDays(-1).ToShortDateString();
                var endDateElement = driver.FindElement(By.XPath("//md-datepicker[@name='endDate']/div[@class='md-datepicker-input-container']/input"));

                endDateElement.Clear();
                endDateElement.SendKeys(enddate);


                var errorMessageElement = driver.FindElement(By.XPath("//div[@ng-messages='form.endDate.$error' and contains(@class,'ng-active')]"));
                Assert.IsTrue(errorMessageElement.Displayed, "Error Message regarding end date must be displayed");
                Console.WriteLine("Error Message for end date validated");

                var nextButton = driver.FindElement(By.XPath("//button[contains(.,'Next')]"));
                Assert.IsTrue(nextButton.GetAttribute("disabled") == "true", "Next button must be disabled");
                Console.WriteLine("Next button is disabled");

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1703 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1703 Scenario 1 Failed. Error: {0}", ex);
            }
        }


        private static void LoginAndSelectChallenge()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);
            var challengesTab = adminPage.GetProgramTab("Challenges");
            if (challengesTab == null)
            {
                Console.WriteLine("Unable to find challenges tab");
                Assert.Fail("Unable to find challenges tab");
            }
            else
            {
                Console.WriteLine("Challenges Tab loaded");
                challengesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(6000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

        }
        private void CreateNewChallenge()
        {
            string errorMessage = string.Empty;
            AdminPage adminPage = new AdminPage(driver);
            Thread.Sleep(5000);
            adminPage.GetAddChallengeButton();
            if (adminPage.AddChallengeButton != null)
            {
                adminPage.AddChallengeButton.Click();
                Console.WriteLine("Add Challenge Button clicked");
                Thread.Sleep(2000);
            }
            else
            {
                Console.Error.WriteLine("Cannot add new challenge");
                Assert.Fail("cannot add new challenge");
            }


            Thread.Sleep(2000);
            var challengeCategory = adminPage.GetChallengeCategory("Wellness");
            if (challengeCategory != null)
            {
                challengeCategory.Click();
                Console.WriteLine("Wellness Challenge type selected");
            }
            else
            {
                Console.Error.WriteLine("Wellness Challenge Type not available");
                Assert.Fail("Wellness Challenge Type not available");
            }

            Thread.Sleep(2000);
            var challengeType = adminPage.GetChallengeType(Data.WellnessChallenge.WellnessChallengeType);
            if (challengeType != null)
            {
                challengeType.Click();
                Console.WriteLine("{0} Challenge Type selected", Data.WellnessChallenge.WellnessChallengeType);
            }
            else
            {
                errorMessage = string.Format("{0} Challenge type not found", Data.WellnessChallenge.WellnessChallengeType);
                Console.Error.WriteLine(errorMessage);
                Assert.Fail(errorMessage);
            }

            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            string challengename = Data.WellnessChallenge.ChallengeName.GetDynamicText();
            string startdate = Data.WellnessChallenge.StartDate.GetDynamicText();
            string endDate = Data.WellnessChallenge.EndDate.GetDynamicText();
            bool success = addNewChallengePage.FillStep1(challengename, startdate, endDate, Data.WellnessChallenge.GoalValue.ToString(), Data.WellnessChallenge.TimeLine, Data.WellnessChallenge.Description, out errorMessage);
            if (success)
            {
                Thread.Sleep(2000);
                Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                addNewChallengePage.Step1_NextButton.Click();
            }
            else
            {
                Console.WriteLine(errorMessage);
                Assert.Fail(errorMessage);
            }

            var participantCount = addNewChallengePage.Step2_ParticipantList;
            if (participantCount != null && participantCount.Count > 0)
            {
                Console.WriteLine("Participants available to select");
            }
            else
            {
                Console.Error.WriteLine("No Participants available");
                Assert.Fail("No Participants available");
            }

            success = addNewChallengePage.AddChallengers(Data.WellnessChallenge.ChallengerName, out errorMessage);
            if (success)
            {
                Console.WriteLine("Participant {0} added to challenge", Data.WellnessChallenge.ChallengerName);
            }
            else
            {
                Console.Error.WriteLine("Failed to add {0} challenger. Error: {1}", Data.WellnessChallenge.ChallengerName, errorMessage);
                Assert.Fail(errorMessage);
            }


            addNewChallengePage.Step2_CreateButton.Click();
            Console.WriteLine("Create Challenge button clicked");

            WebDriverWait wait = GetWebDriverWait();
            var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
            if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
            {
                string message = string.Format("Challenge {0} added successfully", challengename);
                Console.WriteLine(message);
                Assert.IsTrue(true);
            }
            else
            {
                errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                Assert.Fail(errorMessage);
                Console.Error.WriteLine(errorMessage);
            }
        }


        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private PIV1703Data GetData()
        {
            PIV1703Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1703.json");
                data = JsonConvert.DeserializeObject<PIV1703Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
    }
}
