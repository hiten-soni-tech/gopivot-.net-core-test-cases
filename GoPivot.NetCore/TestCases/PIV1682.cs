﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1682", Category = "Nutrition")]
    public class PIV1682 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        string breakfastSaturatedFat = string.Empty;
        string lunchSaturatedFat = string.Empty;
        string dinnerSaturatedFat = string.Empty;

        string breakfastCaloriesFat = string.Empty;
        string lunchCaloriesFat = string.Empty;
        string dinnerCaloriesFat = string.Empty;
        [TestCase(Description = "PIV-1682 Sodium values for applicable dietary plans - Males & Females", TestName = "Sodium values for applicable dietary plans - Males & Females")]
        [Order(1)]
        public void Validate_Sodium_Values_For_Applicable_Dietary_Plan()
        {
            try
            {
                LoginParticipant();
                Thread.Sleep(4000);
                IWebElement profileTabElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("My Profile")));
                profileTabElement.Click();
                Console.WriteLine("My Profile Page Loaded");
                driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Nutrition')]")).Click();
                Console.WriteLine("Nutrition Tab selected from my profile");
                Thread.Sleep(4000);

                string defaultPlanSelected =
                    driver.FindElement(
                            By.XPath(
                                "//div[contains(@class,'panel-default dietPlan')]//a//div[@class='ng-scope']/span"))
                        .Text;
                Console.WriteLine("Default Plan selected: {0}", defaultPlanSelected);
                if (!string.Equals(defaultPlanSelected, "Balanced Weight Loss"))
                {
                    driver.FindElement(By.XPath("//div[contains(@class,'panel-default dietPlan')]//a")).Click();
                    driver.FindElement(By.XPath("//div[@class = 'panel-collapse in collapse']//ul/li/div/span/strong[contains(.,'Balanced Weight Loss')]")).Click();
                    Thread.Sleep(2000);
                    Console.WriteLine("Balanced Weight Loss Plan selected");

                }
                Thread.Sleep(3000);
                IWebElement nutritionWebElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                nutritionWebElement.Click();
                Console.WriteLine("Nutrition Page Loaded");

                Thread.Sleep(4000);

                var calorieGoalValueElement =
                    driver.FindElement(By.XPath("//div[contains(@class,'nutritionTotalsData')][1]"));
                Assert.IsTrue(calorieGoalValueElement != null && calorieGoalValueElement.Displayed,
                    "Calorie Goal element should be present");
                int.TryParse(calorieGoalValueElement.Text, out var calorieGoal);
                Assert.IsTrue(calorieGoal > 0, "Calorie Goal should be greater than 0");
                Console.WriteLine("Calorie goal value is {0}", calorieGoal);

                Thread.Sleep(2000);
                // Breakfast
                driver.FindElement(By.XPath(
                        "//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Breakfast')]/../div[@class='recipeContainer']//div[@class='recipeInfoPlate']/div[@class='recipePieChart']//a[contains(.,'Nutrition Facts')]")).Click();


                string breakFastSodiumMg = driver.FindElement(By.XPath("//span[@itemprop='sodiumContent']")).Text;
                breakFastSodiumMg = breakFastSodiumMg.Replace("mg", "");
                breakFastSodiumMg = breakFastSodiumMg.Replace("milligrams", "");
                breakFastSodiumMg = breakFastSodiumMg.Trim();
                string breakfastSodiumPercentage = driver.FindElement(By.XPath("//span[@itemprop='sodiumContent']/../div[@class='dv']/strong")).Text;
                breakfastSaturatedFat = driver.FindElement(By.XPath("//span[@itemprop='saturatedFatContent']/../div[@class='dv']/strong")).Text;
                Int32.TryParse(breakFastSodiumMg, out int breakFastSodiumMgInteger);
                Int32.TryParse(breakfastSodiumPercentage, out int breakFastSodiumPercentageInteger);

                Console.WriteLine("Breakfast Sodium Miligram Value: {0}", breakFastSodiumMgInteger);
                Console.WriteLine("Breakfast Sodium Percentage: {0}%", breakFastSodiumPercentageInteger);

                breakfastCaloriesFat = driver.FindElement(By.XPath("//*[@id='nutritionLabel']/div[1]/div[6]/div[1]")).Text;
                breakfastCaloriesFat = breakfastCaloriesFat.Replace("Calories from Fat", string.Empty).Trim();

                driver.FindElement(By.XPath("//button[contains(.,'Cancel')]")).Click();

                Thread.Sleep(2000);
                // Lunch

                driver.FindElement(By.XPath(
                     "//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Lunch')]/../div[@class='recipeContainer']//div[@class='recipeInfoPlate']/div[@class='recipePieChart']//a[contains(.,'Nutrition Facts')]")).Click();


                string lunchSodiumMg = driver.FindElement(By.XPath("//span[@itemprop='sodiumContent']")).Text;
                lunchSodiumMg = lunchSodiumMg.Replace("mg", "");
                lunchSodiumMg = lunchSodiumMg.Replace("milligrams", "");
                lunchSodiumMg = lunchSodiumMg.Trim();
                string lunchSodiumPercentage = driver.FindElement(By.XPath("//span[@itemprop='sodiumContent']/../div[@class='dv']/strong")).Text;
                lunchSaturatedFat = driver.FindElement(By.XPath("//span[@itemprop='saturatedFatContent']/../div[@class='dv']/strong")).Text;

                Int32.TryParse(lunchSodiumMg, out int lunchSodiumMgInteger);
                Int32.TryParse(lunchSodiumPercentage, out int lunchSodiumPercentageInteger);

                Console.WriteLine("Lunch Sodium Miligram Value: {0}", lunchSodiumMgInteger);
                Console.WriteLine("Lunch Sodium Percentage: {0}%", lunchSodiumPercentageInteger);

                lunchCaloriesFat = driver.FindElement(By.XPath("//*[@id='nutritionLabel']/div[1]/div[6]/div[1]")).Text;
                lunchCaloriesFat = lunchCaloriesFat.Replace("Calories from Fat", string.Empty).Trim();


                driver.FindElement(By.XPath("//button[contains(.,'Cancel')]")).Click();

                Thread.Sleep(2000);
                // Dinner

                driver.FindElement(By.XPath(
                     "//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Dinner')]/../div[@class='recipeContainer']//div[@class='recipeInfoPlate']/div[@class='recipePieChart']//a[contains(.,'Nutrition Facts')]")).Click();


                string dinnerSodiumMg = driver.FindElement(By.XPath("//span[@itemprop='sodiumContent']")).Text;
                dinnerSodiumMg = dinnerSodiumMg.Replace("mg", "");
                dinnerSodiumMg = dinnerSodiumMg.Replace("milligrams", "");
                dinnerSodiumMg = dinnerSodiumMg.Trim();
                string dinnerSodiumPercentage = driver.FindElement(By.XPath("//span[@itemprop='sodiumContent']/../div[@class='dv']/strong")).Text;
                dinnerSaturatedFat = driver.FindElement(By.XPath("//span[@itemprop='saturatedFatContent']/../div[@class='dv']/strong")).Text;

                Int32.TryParse(dinnerSodiumMg, out int dinnerSodiumMgInteger);
                Int32.TryParse(dinnerSodiumPercentage, out int dinnerSodiumPercentageInteger);

                Console.WriteLine("Dinner Sodium Miligram Value: {0}", dinnerSodiumMgInteger);
                Console.WriteLine("Dinner Sodium Percentage: {0}%", dinnerSodiumPercentageInteger);

                dinnerCaloriesFat = driver.FindElement(By.XPath("//*[@id='nutritionLabel']/div[1]/div[6]/div[1]")).Text;
                dinnerCaloriesFat = dinnerCaloriesFat.Replace("Calories from Fat", string.Empty).Trim();


                driver.FindElement(By.XPath("//button[contains(.,'Cancel')]")).Click();

                int totalSodiumValue = breakFastSodiumMgInteger + lunchSodiumMgInteger + dinnerSodiumMgInteger;
                Assert.IsTrue(totalSodiumValue <= 2300, "Total Sodium Value should be less than 2.3 grams per day");
                Console.WriteLine("Total Sodium Value: {0} is less than 2.3 grams per day", ((totalSodiumValue) / 1000));

                Assert.IsTrue(breakFastSodiumPercentageInteger <= 20, "Breakfast sodium value should be less than 20%");
                Console.WriteLine("Breakfast sodium value is less than 20%");
                Assert.IsTrue(lunchSodiumPercentageInteger <= 40, "Lunch sodium value should be less than 40%");
                Console.WriteLine("Lunch sodium value is less than 40%");
                Assert.IsTrue(dinnerSodiumPercentageInteger <= 40, "Dinner sodium value should be less than 40%");
                Console.WriteLine("Dinner sodium value is less than 40%");
                IsSuccess = true;
            }

            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1682 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1682 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-1682 Saturated fat values for applicable dietary plans - Males & Females", TestName = "Saturated fat values for applicable dietary plans - Males & Females")]
        [Order(2)]
        public void Validate_Saturated_Fat_For_Applicable_Dietary_Plan()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }

            if (string.IsNullOrEmpty(breakfastSaturatedFat))
                Assert.Fail("Breakfast Saturated Fat Values not found");

            if (string.IsNullOrEmpty(lunchSaturatedFat))
                Assert.Fail("Lunch Saturated Fat Values not found");

            if (string.IsNullOrEmpty(dinnerSaturatedFat))
                Assert.Fail("Dinner Saturated Fat Values not found");

            double.TryParse(breakfastCaloriesFat, out double breakfastCalories);
            double.TryParse(lunchCaloriesFat, out double lunchCalories);
            double.TryParse(dinnerCaloriesFat, out double dinnerCalories);

            Int32.TryParse(breakfastSaturatedFat, out int breakfastSaturatedFatValue); ;
            Int32.TryParse(lunchSaturatedFat, out int lunchSaturatedFatValue);
            Int32.TryParse(dinnerSaturatedFat, out int dinnerSaturatedFatValue);

            if (breakfastSaturatedFatValue <= 0 || breakfastSaturatedFatValue > 7)
            {
                Console.WriteLine("Invalid Breakfast Fat Value {0}%. Maximum allowed 7%", breakfastSaturatedFatValue);
                Assert.Fail("Invalid Breakfast Saturated Fat Value {0}%. Maximum allowed 7%", breakfastSaturatedFatValue);
            }
            else
            {
                Console.WriteLine("Valid Breakfast Fat Value {0}%", breakfastSaturatedFatValue);
            }

            if (lunchSaturatedFatValue <= 0 || lunchSaturatedFatValue > 7)
            {
                Console.WriteLine("Invalid lunch Fat Value {0}%. Maximum allowed 7%", lunchSaturatedFatValue);
                Assert.Fail("Invalid lunch Saturated Fat Value {0}%. Maximum allowed 7%", lunchSaturatedFatValue);
            }
            else
            {
                Console.WriteLine("Valid lunch Fat Value {0}%", lunchSaturatedFatValue);
            }

            if (dinnerSaturatedFatValue <= 0 || dinnerSaturatedFatValue > 7)
            {
                Console.WriteLine("Invalid dinner Fat Value {0}%. Maximum allowed 7%", dinnerSaturatedFatValue);
                Assert.Fail("Invalid dinner Saturated Fat Value {0}%. Maximum allowed 7%", dinnerSaturatedFatValue);
            }
            else
            {
                Console.WriteLine("Valid dinner Fat Value {0}%", dinnerSaturatedFatValue);
            }
        }
        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private void BuildRecipePlanIfNotExists()
        {
            try
            {
                driver.FindElement(By.LinkText("Build Your Recipe Plan")).Click();
                Console.WriteLine("Build Your Recipe Plan clicked");

                driver.FindElement(By.XPath("//input[@ng-model='model.desiredWeight']")).SendKeys("100");
                driver.FindElement(By.XPath("//input[@type='button' and @value = 'Save']")).Click();
                Console.WriteLine("Save Button clicked");

                Thread.Sleep(3000);

                var nutritionTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }
                Thread.Sleep(3000);
            }
            catch
            {
                Console.WriteLine("Button: Build Your Recipe Plan not found");
                Console.WriteLine("Recipe Plan already available for this user");
                // Do nothing
            }
        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }

}
