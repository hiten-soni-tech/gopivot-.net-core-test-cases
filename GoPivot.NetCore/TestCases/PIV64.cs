﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;
using GoPivot.NetCore.ComponentHelper;
namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-64", Category = "Biometrics")]

    public class PIV64 : BaseStepDefinition
    {
        public string InformationText { get; set; }
        public string InstructionText { get; set; }
        public bool IsSuccess { get; set; }
        [Order(1)]
        [TestCase(TestName = "Scenario 0 - Do not display biometrics link on toggle off")]
        public void Validate_Toggle_Off()
        {
            try
            {
                LoginAdministrator();
                string biometricsElementPath = "//div[contains(@class,'settingLabel') and contains(.,'Biometrics')]/preceding-sibling::md-switch[contains(@class,'md-checked')]";
                try
                {
                    driver.FindElement(By.XPath(biometricsElementPath)).Click();
                    Console.WriteLine("Disabled Biometrics Option from account setting");
                }
                catch
                {
                    // Element already disabled no actions needed.
                    Console.WriteLine("Biometrics option is disabled");
                }
                LogoutAdministrator();
                Thread.Sleep(3000);
                LoginParticipant(ObjectRepository.Setting.Credentials.Participant.Username, ObjectRepository.Setting.Credentials.Participant.Password);
                Thread.Sleep(4000);

                try
                {
                    Assert.IsFalse(driver.FindElement(By.LinkText("Biometrics")).Displayed, "Biometrics Link must be disabled");
                    Console.WriteLine("ERROR: Biometrics Link must be disabled");
                }
                catch
                {
                    Console.WriteLine("Biometrics Link not available");
                }

                LogoutParticipant();
                Thread.Sleep(3000);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-64 Scenario 0 Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-64 Scenario 0 Failed. Error: {0}", ex);
            }
        }

        [Order(2)]
        [TestCase(TestName = "Scenario 1 - Show Information Text to Participant")]
        public void Validate_Biometrics_Enabled_Information_Box()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 0 not completed"); return;
            }
            try
            {
                LoginAdministrator();
                Thread.Sleep(3000);
                try
                {
                    string biometricsElementPath = "//div[contains(@class,'settingLabel') and contains(.,'Biometrics')]/preceding-sibling::md-switch[not(contains(@class,'md-checked'))]";
                    driver.FindElement(By.XPath(biometricsElementPath)).Click();
                    Thread.Sleep(4000);
                    Console.WriteLine("Biometrics option enabled");
                }
                catch
                {
                    Console.WriteLine("Biometrics option already enabled");
                    driver.FindElement(By.XPath("//div[contains(@class,'settingLabel') and contains(.,'Biometrics')]/preceding-sibling::md-switch/..")).Click();
                    Thread.Sleep(5000);
                }
                Assert.IsTrue(driver.FindElement(By.ClassName("modal-dialog")).Displayed, "Biometrics modal popup must be displayed");
                Console.WriteLine("Biometrics Modal popup displayed");
                InformationText = driver.FindElement(By.XPath("//*[@id='informationEditor']//p")).Text;
                InstructionText = driver.FindElement(By.XPath("//*[@id='instructionsEditor']//p")).Text;

                driver.FindElement(By.XPath("//input[@value='Cancel' and @type='button']")).Click();
                Console.WriteLine("Cancel Button clicked");
                LogoutAdministrator();
                if (!string.IsNullOrEmpty(InformationText))
                {
                    LoginParticipant(ObjectRepository.Setting.Credentials.Participant.Username, ObjectRepository.Setting.Credentials.Participant.Password);
                    Thread.Sleep(12000);
                    GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.LinkText("Biometrics"))).Click();
                    Thread.Sleep(3000);
                    Console.WriteLine("Biometrics link clicked");

                    var informationTextElement = driver.FindElement(By.XPath("//*[contains(@class,'biometricsMobileWrapper')]//p"));
                    Assert.IsTrue(informationTextElement.Text.Equals(InformationText), "Information Text for Biometrics must be matching");
                    Console.WriteLine("Information Text for Biometrics is matching");
                    LogoutParticipant();
                }
                else
                {
                    LogoutParticipant();
                    Assert.Warn("No Information Text been set");
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-64 Scenario 1 Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-64 Scenario 1 Failed. Error: {0}", ex);
            }

        }


        [Order(3)]
        [TestCase(TestName = "Scenario 2 - Show Instruction Text to Participant")]
        public void Validate_Biometrics_Enabled_Instruction_Box()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {

                if (!string.IsNullOrEmpty(InstructionText))
                {
                    LoginParticipant(ObjectRepository.Setting.Credentials.NewParticipant.Username, ObjectRepository.Setting.Credentials.NewParticipant.Password);
                    Thread.Sleep(10000);
                    GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.LinkText("Biometrics"))).Click();
                    Thread.Sleep(3000);
                    Console.WriteLine("Biometrics link clicked");

                    var instructionTextElement = driver.FindElement(By.XPath("//*[contains(@ng-show,'noBiometrics')]//p"));
                    Assert.IsTrue(instructionTextElement.Text.Equals(InstructionText), "Instruction Text for Biometrics must be matching");
                    Console.WriteLine("Instruction Text for Biometrics is matching");
                    LogoutParticipant();
                }
                else
                {
                    Assert.Warn("No Instructions Text been set");
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-64 Scenario 2 Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-64 Scenario 2 Failed. Error: {0}", ex);
            }

        }

        [Order(3)]
        [TestCase(TestName = "Scenario 3 - Show Instruction Text to Participant for Non Instruction Text")]
        public void Validate_Biometrics_Enabled_Instruction_Box_For_No_Instruction_Text()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            try
            {

                if (string.IsNullOrEmpty(InstructionText))
                {
                    InstructionText = "We do not currently have any BioMetric Data for you to see. Please see your plan administrator for further details.";
                    LoginParticipant(ObjectRepository.Setting.Credentials.NewParticipant.Username, ObjectRepository.Setting.Credentials.NewParticipant.Password);
                    Thread.Sleep(10000);
                    GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.LinkText("Biometrics"))).Click();
                    Thread.Sleep(3000);
                    Console.WriteLine("Biometrics link clicked");

                    var instructionTextElement = driver.FindElement(By.XPath("//*[contains(@ng-show,'noBiometrics')]//p"));
                    Assert.IsTrue(instructionTextElement.Text.Contains(InstructionText), "Instruction Text for Biometrics must be matching");
                    Console.WriteLine("Instruction Text for Biometrics is matching");
                    LogoutParticipant();
                }
                else
                {
                    Assert.Warn("Instructions Text available. Scenario can't be tested");
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-64 Scenario 2 Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-64 Scenario 2 Failed. Error: {0}", ex);
            }

        }

        private void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(1000);
            loginPage.LoginButton.Click();

            Thread.Sleep(1000);

            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }
            Thread.Sleep(2000);
            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }


            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            Thread.Sleep(2000);
            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

        }

        private void LoginParticipant(string username, string password)
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(username);
            loginPage.PassTextBox.SendKeys(password);
            Thread.Sleep(3000);
            loginPage.LoginButton.Click();

            Thread.Sleep(3000);

            var logoutElement =
                GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement != null && logoutElement.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully",
                   username);
            }
            else
            {
                Assert.Fail("Participant {0} Login Failed", username);
                return;
            }
        }

        private void LogoutParticipant()
        {
            Thread.Sleep(3000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }
        private void LogoutAdministrator()
        {
            Thread.Sleep(4000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }
        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}
