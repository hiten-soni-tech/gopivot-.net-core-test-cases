﻿
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using GoPivot.NetCore.Settings;
using GoPivot.NetCore.ComponentHelper;
using System.Collections.Generic;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1860", Category = "Challenge")]
    public class PIV1860 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1860Data Data { get; set; }
        [TestCase(TestName = "Scenario 0: Create new team custom challenge")]
        [Order(1)]
        public void PreRequisite_Create_New_Custom_Challenge()
        {
            try
            {


                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAndSelectChallenges();
                AdminPage adminPage = new AdminPage(driver);

                adminPage.GetAddChallengeButton();
                if (adminPage.AddChallengeButton != null)
                {
                    adminPage.AddChallengeButton.Click();
                    Console.WriteLine("Add Challenge Button clicked");
                    Thread.Sleep(2000);
                }
                else
                {
                    Console.WriteLine("Cannot add new challenge");
                    Assert.Fail("cannot add new challenge");
                }

                Thread.Sleep(2000);

                var challengeCategory = adminPage.GetChallengeCategory("Custom");
                if (challengeCategory != null)
                {
                    challengeCategory.Click();
                    Console.WriteLine("Custom Challenge type selected");
                }
                else
                {
                    Console.WriteLine("Custom Challenge Type not available");
                    Assert.Fail("Custom Challenge Type not available");
                }

                Thread.Sleep(2000);
                var challengeType = adminPage.GetChallengeType("Custom");
                string errorMessage;
                if (challengeType != null)
                {
                    challengeType.Click();
                    Console.WriteLine("Custom Challenge selected");
                }
                else
                {
                    errorMessage = "Custom Challenge not found";
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }


                AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);

                string randomNumber = new Random().Next(1000).ToString();
                string challengename = Data.Challenge.ChallengeName.GetDynamicText();
                string startdate = Data.Challenge.StartDate.GetDynamicText();
                Data.GeneratedChallengeName = challengename;
                string enddate = Data.Challenge.EndDate.GetDynamicText();
                bool success = addNewChallengePage.FillStepForCustom(challengename, startdate, enddate, Data.Challenge.CustomChallengeActionText, Data.Challenge.CustomChallengeGoal.ToString(), Data.Challenge.CustomChallengeUnit, Data.Challenge.CustomChallengeTime, Data.Challenge.GoalText, out errorMessage);
                if (success)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                    addNewChallengePage.Step1_NextButton.Click();
                }
                else
                {
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                var participantCount = addNewChallengePage.Step2_ParticipantList;
                if (participantCount != null && participantCount.Count > 0)
                {
                    Console.WriteLine("Participants available to select");
                }
                else
                {
                    Console.WriteLine("No Participants available");
                    Assert.Fail("No Participants available");
                }

                addNewChallengePage.Step2_Teams.Click();
                Thread.Sleep(3000);

                driver.FindElement(By.Id("select_20")).Click();
                Thread.Sleep(1000);

                foreach (string team in Data.Challenge.Teams)
                {
                    driver.FindElement(By.XPath(string.Format("//md-option[@value = '{0}']", team))).Click();
                    Console.WriteLine("Team {0} added", team);
                    Thread.Sleep(1000);
                }

                Thread.Sleep(2000);

                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("arguments[0].click()", addNewChallengePage.Step2_CreateButton);

                Console.WriteLine("Create Challenge button clicked");

                WebDriverWait wait = GetWebDriverWait();
                var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
                if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
                {
                    Console.WriteLine("Challenge {0} added successfully", challengename);
                    Assert.IsTrue(true);
                }
                else
                {
                    errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                    Console.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                LogoutAdministrator();
                Thread.Sleep(3000);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1860 Scenario 0 Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1860 Scenario 0 Test Case Failed. Error: {0}", ex);

            }
        }

        [TestCase(TestName = "Scenario 1: Display all Teams who have joined the challenge")]
        [Order(2)]
        public void Validate_Display_All_Teams_Who_Joined_Challenge()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 0 not completed"); return;
                }
                //Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                //Data.GeneratedChallengeName = "Future Custom 1/9/2019-569";
                if (string.IsNullOrEmpty(Data.GeneratedChallengeName))
                {
                    Console.Error.WriteLine("Challenge not available");
                    Assert.Fail("Challenge not available");
                    return;
                }
                By loadingImage = By.TagName("http-busy");
                Thread.Sleep(2000);
                LoginParticipant();
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));
                IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
                if (challengeTab != null && challengeTab.Displayed)
                {
                    challengeTab.Click();
                    Console.WriteLine("Challenge Tab Clicked");
                }

                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));
                Thread.Sleep(5000);
                var challengeList = driver.FindElements(By.XPath("//div[@class='panel challenge ng-scope']"));
                if (challengeList != null && challengeList.Count > 0)
                {
                    Console.WriteLine("{0} Challenges available for participant", challengeList.Count);
                }
                else
                {
                    Console.Error.WriteLine("Failed to load challenges");
                    Assert.Fail("Failed to load challenges");
                }

                var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", Data.GeneratedChallengeName))));
                if (challenge != null && challenge.Displayed)
                {
                    challenge.Click();
                    Console.WriteLine("{0} available challenge found", Data.GeneratedChallengeName);
                }
                else
                {
                    Console.Error.WriteLine("{0} available challenge not found", Data.GeneratedChallengeName);
                    Assert.Fail("{0} available challenge not found", Data.GeneratedChallengeName);
                }
                Thread.Sleep(2000);

                try
                {
                    IWebElement joinButtonElement = driver.FindElement(By.XPath("//a[contains(.,'Join!')]"));
                    if (joinButtonElement != null && joinButtonElement.Displayed)
                    {
                        Thread.Sleep(3000);
                        joinButtonElement.Click();
                        Console.WriteLine("Join Button available and clicked to join this challenge");
                    }
                    else
                    {
                        Console.WriteLine("Join button not available or already joined");
                    }


                    driver.FindElement(By.Id("numericValue")).Clear();
                    driver.FindElement(By.Id("numericValue")).SendKeys(Data.Challenge.CustomChallengeGoal.ToString());

                    var button = driver.FindElement(By.XPath("/html/body/div/div/div[2]/div/div[2]/div/div[2]/ng-include/div/div[1]/div/div/div/form/div[4]/button"));
                    IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                    js.ExecuteScript("arguments[0].click()", button);
                    Console.WriteLine("{0} Goal added for date {1}", Data.Challenge.CustomChallengeGoal, DateTime.Now.ToShortDateString());
                    Thread.Sleep(3000);

                    var numberOfGoalTextElement = driver.FindElement(By.XPath("//div[@class='numericGoalMetNumber']/div"));
                    if (numberOfGoalTextElement != null && numberOfGoalTextElement.Displayed && numberOfGoalTextElement.Text == "Yes")
                    {
                        Console.WriteLine("Goal met successfully");
                        Assert.IsTrue(numberOfGoalTextElement.Text == "Yes");
                    }
                    else
                    {
                        Console.Error.WriteLine("Goal Achieve Text Element not available");
                        Assert.Fail("Goal Achieve Text Element not available");
                    }
                    Thread.Sleep(3000);

                }
                catch
                {
                    IWebElement logItElement = driver.FindElement(By.XPath("//h2[contains(.,'Log It')]"));
                    if (logItElement != null && logItElement.Displayed)
                    {
                        Console.WriteLine("Participant already joined");
                    }
                }


                Thread.Sleep(3000);

                IWebElement leaderBoardTabElement = driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Leaderboard')]"));
                if (leaderBoardTabElement != null && leaderBoardTabElement.Displayed)
                {
                    Console.WriteLine("Leaderboard tab is visible");
                }
                else
                {
                    Console.Error.WriteLine("Cannot find Leaderboard tab");
                    Assert.Fail("Cannot find Leaderboard tab");
                }
                Thread.Sleep(2000);
                leaderBoardTabElement.Click();
                Console.WriteLine("Leaderboard section loaded");

                Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='leaderboardHeader']/th[contains(.,'Rank')]")).Displayed, "Rank header displayed");
                Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='leaderboardHeader']/th[contains(.,'Team')]")).Displayed, "Team header displayed");
                Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='leaderboardHeader']/th[contains(.,'Total Time(s)')]")).Displayed, "Total Times header displayed");
                Assert.IsTrue(driver.FindElement(By.XPath("//tr[@class='leaderboardHeader']/th[contains(.,'Avg. Goal Met')]")).Displayed, "Average Goals Met header displayed");
                Console.WriteLine("All Headers matching");

                int teamFound = 0;
                foreach (var team in Data.Challenge.Teams)
                {
                    var teamAvailability = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'teamsLeaderboardlist')]//table/tbody/tr/td[contains(.,'{0}')]", team)));
                    if (teamAvailability != null && teamAvailability.Displayed)
                    {
                        teamFound++;
                        Console.WriteLine("Team {0} found in Participant List", team);

                    }
                    else
                    {
                        Assert.Fail("Team {0} not found in Participant List", team);
                    }
                }
                Assert.IsTrue(teamFound == Data.Challenge.Teams.Length, "Team Count in Participant Tab is matching with Challenge Teams");
                Console.WriteLine("Team Count in Participant Tab is matching with Challenge Teams");

                IWebElement topTeamElement = driver.FindElement(By.XPath("//div[contains(@class,'teamsLeaderboardlist')]//table/tbody/tr/td[2]"));
                Assert.IsTrue(topTeamElement.Text.Equals(ObjectRepository.Setting.Credentials.Administrator.Program), "First Team matching with {0}", ObjectRepository.Setting.Credentials.Administrator.Program);
                Console.WriteLine("Top ranked team matching with program {0}", ObjectRepository.Setting.Credentials.Administrator.Program);


            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1861 Scenario 0 Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1860 Scenario 1 Test Case Failed. Error: {0}", ex);

            }
        }

        [TestCase(TestName = "Scenario 2: Display a Leaderboard of the participants on a team")]
        [Order(3)]
        public void Validate_Display_A_LeaderBoard_Of_The_Participant_On_A_Team()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 2 not completed"); return;
                }
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                string challengerTeam = string.Empty;
                foreach (var team in Data.Challenge.Teams)
                {
                    var teamAvailability = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'teamsLeaderboardlist')]//table/tbody/tr/td[contains(.,'{0}')]", team)));
                    if (teamAvailability != null && teamAvailability.Displayed)
                    {
                        teamAvailability.Click();
                        Assert.IsTrue(
                            driver.FindElement(By.XPath("//div[contains(@class,'leaderboard')]//table/tbody/tr[@class='leaderboardHeader']/th[contains(.,'Rank')]")).Displayed,
                            "Rank Header must be displayed");

                        Assert.IsTrue(
    driver.FindElement(By.XPath("//div[contains(@class,'leaderboard')]//table/tbody/tr[@class='leaderboardHeader']/th[contains(.,'Name')]")).Displayed,
    "Name Header must be displayed");

                        Assert.IsTrue(
    driver.FindElement(By.XPath("//div[contains(@class,'leaderboard')]//table/tbody/tr[@class='leaderboardHeader']/th[contains(.,'Total Time(s)')]")).Displayed,
    "Total Times Header must be displayed");

                        Assert.IsTrue(
    driver.FindElement(By.XPath("//div[contains(@class,'leaderboard')]//table/tbody/tr[@class='leaderboardHeader']/th[contains(.,'Goal Met')]")).Displayed,
    "Goal Met Header must be displayed");

                        Console.WriteLine("All Participant Table Header for Team '{0}' are matching", team);
                        Thread.Sleep(1000);

                        if (ObjectRepository.Setting.Credentials.Administrator.Program == team)
                        {

                            Int32.TryParse(driver.FindElement(By.XPath("//tr[contains(@class,'my-rank')]//td[1]")).Text, out int rank);
                            Assert.IsTrue(rank > 0, "Rank must be greater than 1");
                            Console.WriteLine("Rank for participant set correctly");

                            Assert.IsTrue(driver.FindElement(By.XPath("//tr[contains(@class,'my-rank')]//td[3]")).Text == "You", "Rank Name be must be 'You'");
                            Console.WriteLine("Rank Name been set correctly");

                            Assert.IsTrue(driver.FindElement(By.XPath("//tr[contains(@class,'my-rank')]//td[4]")).Text == "50.00", "Total Time must be 50.00");
                            Console.WriteLine("Total Time been set correctly");

                            Assert.IsTrue(driver.FindElement(By.XPath("//tr[contains(@class,'my-rank')]//td[5]")).Text == "1", "Goals Met must be 1");
                            Console.WriteLine("Goals met must be 1 for team");

                            Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'leaderboard')]/table[contains(@class,'table')]//tr[4]")).Text.Contains("TOP 10"), "Top 10 section must be displayed");
                            Console.WriteLine("Top 10 section displayed successfully for team {0}", team);

                        }
                        else
                        {
                            Assert.IsTrue(driver.FindElement(By.XPath("//div[contains(@class,'leaderboard')]/table[contains(@class,'table')]//tr[2]")).Text.Contains("TOP 10"), "Top 10 section must be displayed");
                            Console.WriteLine("Top 10 section displayed successfully for team {0}", team);
                        }
                        IList<IWebElement> participants = driver.FindElements(By.XPath("//tr[@ng-if='usersLeaderboardList.length']"));
                        Assert.IsTrue(participants.Count <= 10, "Participants count must be less than or equal to 10");
                        Console.WriteLine("Participant count is less than 10. Current Count: {0} for Team {1}", participants.Count, team);

                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1861 Scenario 2 Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1860 Scenario 2 Test Case Failed. Error: {0}", ex);

            }
        }


        #region Utilities
        private void LogoutAdministrator()
        {
            Thread.Sleep(4000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private void LoginParticipant()
        {
            Thread.Sleep(3000);
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private static void LoginAndSelectChallenges()
        {
            Thread.Sleep(3000);
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(1000);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);
            var challengesTab = adminPage.GetProgramTab("Challenges");
            if (challengesTab == null)
            {
                Console.WriteLine("Unable to find challenges tab");
                Assert.Fail("Unable to find challenges tab");
            }
            else
            {
                Console.WriteLine("Activities Tab loaded");
                challengesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(6000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

        }
        private void LogoutParticipant()
        {
            Thread.Sleep(3000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private PIV1860Data GetData()
        {
            PIV1860Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1860.json");
                data = JsonConvert.DeserializeObject<PIV1860Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
