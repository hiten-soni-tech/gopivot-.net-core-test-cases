﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1673", Category = "Nutrition")]
    public class PIV1673 : BaseStepDefinition
    {
        [TestCase(Description = "PIV-1673 Administrator - Carbohydrate values for applicable dietary plans - Males & Females", TestName = "Carbohydrate values for applicable dietary plans - Males & Females")]
        public void Validate_Carbohydrate_Values_For_Applicable_Dietary_Plan()
        {
            try
            {
                LoginParticipant();
                Thread.Sleep(4000);
                IWebElement profileTabElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("My Profile")));
                profileTabElement.Click();
                Console.WriteLine("My Profile Page Loaded");
                driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Nutrition')]")).Click();
                Console.WriteLine("Nutrition Tab selected from my profile");
                Thread.Sleep(2000);

                string defaultPlanSelected =
                    driver.FindElement(
                            By.XPath(
                                "//div[contains(@class,'panel-default dietPlan')]//a//div[@class='ng-scope']/span"))
                        .Text;
                Console.WriteLine("Default Plan selected: {0}", defaultPlanSelected);
                if (!string.Equals(defaultPlanSelected, "Diabetic"))
                {
                    driver.FindElement(By.XPath("//div[contains(@class,'panel-default dietPlan')]//a")).Click();
                    driver.FindElement(By.XPath("//div[@class = 'panel-collapse in collapse']//ul/li/div/span/strong[contains(.,'Diabetic')]")).Click();
                    Thread.Sleep(2000);
                    Console.WriteLine("Diabetic Plan selected");
                 
                }
                IWebElement nutritionWebElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                nutritionWebElement.Click();
                Console.WriteLine("Nutrition Page Loaded");

                Thread.Sleep(2000);

                var calorieGoalValueElement =
                    driver.FindElement(By.XPath("//div[contains(@class,'nutritionTotalsData')][1]"));
                Assert.IsTrue(calorieGoalValueElement != null && calorieGoalValueElement.Displayed,
                    "Calorie Goal element should be present");
                int.TryParse(calorieGoalValueElement.Text, out var calorieGoal);
                Assert.IsTrue(calorieGoal > 0, "Calorie Goal should be greater than 0");
                Console.WriteLine("Calorie goal value is {0}", calorieGoal);

                int dailyCaloriesPercentageValue = calorieGoal - Convert.ToInt32(calorieGoal * 0.75);
                Console.WriteLine("Take 75% of the daily calorie goal {0}", dailyCaloriesPercentageValue);

                int subtractForSnacks = dailyCaloriesPercentageValue - 300;
                Console.WriteLine("Subtract 300 calories (to account for snacks) {0}", subtractForSnacks);

                int breakFastGoalValue = Convert.ToInt32(subtractForSnacks * 0.30);
                Console.WriteLine("Breakfast Percentage Goal Value - 30% of Subtracted Calories i.e. {0}",breakFastGoalValue);

                int lunchGoalValue = Convert.ToInt32(subtractForSnacks * 0.30);
                Console.WriteLine("Lunch Percentage Goal Value - 30% of Subtracted Calories i.e. {0}", lunchGoalValue);

                int dinnerGoalValue = Convert.ToInt32(subtractForSnacks * 0.40);
                Console.WriteLine("Dinner Percentage Goal Value - 40% of Subtracted Calories i.e. {0}", dinnerGoalValue);

                int breakFastCarbPercentageValue = Convert.ToInt32(breakFastGoalValue * 0.40);
                int lunchCarbPercentageValue = Convert.ToInt32(lunchGoalValue * 0.40);
                int dinnerCarbPercentageValue = Convert.ToInt32(dinnerGoalValue * 0.40);

                int.TryParse(
                    driver.FindElement(By.XPath(
                            "//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Breakfast')]/../div[@class='recipeContainer']//div[@class='recipeInfoFooter']/div[1]"))
                        .Text.Replace("g",""), out int breakfastValue);
                Assert.Less(breakfastValue,breakFastCarbPercentageValue, "Breakfast recipe should contain less Carbohydrates then allowed");
                Console.WriteLine("Breakfast recipe contains less Carbohydrates then allowed. Current Recipe Carbohydrates Grams: {0}. Allowed Carbohydrates Grams: {1}", breakfastValue, breakFastCarbPercentageValue);
                int.TryParse(
                    driver.FindElement(By.XPath(
                            "//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Lunch')]/../div[@class='recipeContainer']//div[@class='recipeInfoFooter']/div[1]"))
                        .Text.Replace("g", ""), out int lunchValue);
                Assert.Less(lunchValue, lunchCarbPercentageValue, "Breakfast recipe should contain less Carbohydrates then allowed");
                Console.WriteLine("Lunch recipe contains less Carbohydrates then allowed. Current Recipe Carbohydrates Grams: {0}. Allowed Carbohydrates Grams: {1}", lunchValue, lunchCarbPercentageValue);

                int.TryParse(
                    driver.FindElement(By.XPath(
                            "//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Dinner')]/../div[@class='recipeContainer']//div[@class='recipeInfoFooter']/div[1]"))
                        .Text.Replace("g", ""), out int dinnerValue);
                Assert.Less(dinnerValue, dinnerCarbPercentageValue, "Breakfast recipe should contain less Carbohydrates then allowed");
                Console.WriteLine("Dinner recipe contains less Carbohydrates then allowed. Current Recipe Carbohydrates Grams: {0}. Allowed Carbohydrates Grams: {1}", dinnerValue, dinnerCarbPercentageValue);

            }

            catch (Exception ex)
            {
                Console.WriteLine("PIV-1673 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1673 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }

}
