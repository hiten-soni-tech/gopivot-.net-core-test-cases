﻿
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using System.Collections.Generic;
using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1561", Category = "Nutrition")]
    public class PIV1561 : BaseStepDefinition
    {
        
        [TestCase(Description = "PIV 1561 -  Nutrition UI - View Nutrition Facts Table", TestName = "Nutrition UI - View Nutrition Facts Table")]
        public void ValidateNutritionFactPopup()
        {
            try
            {
                
                var loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
                loginPage.LoginButton.Click();

                Thread.Sleep(2000);

                var logoutElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);

                }
                else
                {
                    Assert.Fail("Participant Login Failed");
                    return;
                }

                var nutritionTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }
                Thread.Sleep(1000);
                BuildRecipePlanIfNotExists();

                var title = string.Empty;
                IList<IWebElement> mealsTitleElement = driver.FindElements(By.XPath("//div[contains(@class,'recipeTitle')]"));
                if (mealsTitleElement != null && mealsTitleElement.Count > 0)
                {
                    var firstElement = mealsTitleElement.FirstOrDefault();
                    if (firstElement != null) title = firstElement.Text;
                    Console.WriteLine("Meal {0} found containing nutrition facts link", title);
                }
                else
                {
                    Console.Error.WriteLine("No Nutrition Facts found");
                    Assert.Fail("No Meals section found");
                }

                IList<IWebElement> nutritionFactLinkElements = driver.FindElements(By.LinkText("Nutrition Facts"));
                if(nutritionFactLinkElements != null && nutritionFactLinkElements.Count > 0)
                {
                    nutritionFactLinkElements.FirstOrDefault()?.Click();
                   
                    Console.WriteLine("Nutrition Facts link clicked");
                }
                else
                {
                    Console.Error.WriteLine("No Nutrition Fact Link found");
                    Assert.Fail("No Nutrition Fact Link found");

                }
                Thread.Sleep(2000);
                Assert.IsTrue(driver.FindElement(By.ClassName("modal-dialog")).Displayed, "Model Popup opened in browser");
                Console.WriteLine("Model Popup opened in browser");

                var popupHeaderElement = driver.FindElement(By.XPath("//div[contains(@class,'modal-header')]/h2[@class='ng-binding']"));
                Assert.IsTrue(popupHeaderElement.Displayed,"Popup Header element must be displayed");
                Assert.IsTrue(title.Contains(popupHeaderElement.Text), "Popup header text must match with meal title");
                Console.WriteLine("Popup header text matches with meal title");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("PIV-1561 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1561 Failed. Error: {0}", ex);
            }

        }

        private void BuildRecipePlanIfNotExists()
        {
            try
            {
                driver.FindElement(By.LinkText("Build Your Recipe Plan")).Click();
                Console.WriteLine("Build Your Recipe Plan clicked");

                driver.FindElement(By.XPath("//input[@ng-model='model.desiredWeight']")).SendKeys("100");
                driver.FindElement(By.XPath("//input[@type='button' and @value = 'Save']")).Click();
                Console.WriteLine("Save Button clicked");

                Thread.Sleep(3000);

                var nutritionTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }
                Thread.Sleep(3000);
            }
            catch
            {
                Console.WriteLine("Button: Build Your Recipe Plan not found");
                Console.WriteLine("Recipe Plan already available for this user");
                // Do nothing
            }
        }

        public WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}
