﻿using System;
using System.IO;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.DataEntities;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Linq;
using System.Collections.Generic;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(Category = "Exercise")]
    public class PIV1390 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1390Data Data { get; set; }
        [Order(1)]
        [TestCase(TestName = "Scenario 1: Presentation of Workout Plan")]
        public void Validate_Presentation_Of_Workout_Plan()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginParticipant();
                Thread.Sleep(5000);
                var exerciseTab = GetWebDriverWait()
               .Until(ExpectedConditions.ElementToBeClickable(By.LinkText("Exercise")));
                if (exerciseTab != null && exerciseTab.Displayed)
                {
                    exerciseTab.Click();
                    Console.WriteLine("Exercise Tab Clicked");
                }
                Thread.Sleep(6000);

                var headerElement = driver.FindElement(By.XPath("//h2[contains(@class,'fitnessPlanHeader')]"));
                Assert.IsTrue(headerElement.Text.Equals(Data.Header), "Header must be matching");
                Console.WriteLine("Header is matching '{0}'", Data.Header);
                var week1TextElement = driver.FindElement(By.XPath("//div[contains(@class,'fitnessThisWeek')]//h3"));
                Assert.IsTrue(week1TextElement.Text.Equals(Data.Week1Label), "Week 1 Label must be matching");
                Console.WriteLine("This Week Label is matching '{0}'", Data.Week1Label);
                var week2TextElement = driver.FindElement(By.XPath("//div[contains(@class,'fitnessNextWeek')]/h3"));
                Assert.IsTrue(week2TextElement.Text.Equals(Data.Week2Label), "Week 2 Label must be matching");
                Console.WriteLine("Next Week Label is matching '{0}'", Data.Week2Label);

                IList<IWebElement> planTitleElements = driver.FindElements(By.XPath("//div[@class='fitnessPlanTitle']"));
                foreach (var element in planTitleElements)
                {
                    Assert.IsTrue(Data.WorkoutTypes.Contains(element.Text), "Plan {0} available in valid list", element.Text);
                    Console.WriteLine("Plan {0} is available in valid list of plan categories", element.Text);
                }
                Thread.Sleep(2000);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1390 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1390 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [Order(2)]
        [TestCase(TestName = "Scenario 2: Initial Calculation of Plan Logic - Getting Started Regimen")]
        public void Validate_Initial_Calculation_Of_Plan_Logic_Getting_Started_Regimen()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("First Scenario not completed"); return;
                }
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                var myProfileTab = GetWebDriverWait()
            .Until(ExpectedConditions.ElementToBeClickable(By.LinkText("My Profile")));
                if (myProfileTab != null && myProfileTab.Displayed)
                {
                    myProfileTab.Click();
                    Console.WriteLine("My Profile Tab Clicked");
                }
                Thread.Sleep(6000);
                driver.FindElement(By.XPath("//li[contains(@class,'nav-item')]/a[contains(.,'Exercise')]")).Click();
                Thread.Sleep(3000);
                Console.WriteLine("Exercise Section selected");
                Thread.Sleep(3000);
                var ageElement = driver.FindElement(By.Id("fitnessAge"));
                ageElement.Clear();
                ageElement.SendKeys(60.ToString());
                Thread.Sleep(3000);
                Console.WriteLine("Age been updated to 60");
                var exerciseTab = GetWebDriverWait()
           .Until(ExpectedConditions.ElementToBeClickable(By.LinkText("Exercise")));
                if (exerciseTab != null && exerciseTab.Displayed)
                {
                    exerciseTab.Click();
                    Console.WriteLine("Exercise Tab Clicked");
                }
                Thread.Sleep(6000);

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1390 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1390 Scenario 2 Failed. Error: {0}", ex);
            }
        }
 

        #region Utilities
        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.NewParticipant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.NewParticipant.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();

            Thread.Sleep(2000);

            var logoutElement =
                GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement != null && logoutElement.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully",
                    ObjectRepository.Setting.Credentials.NewParticipant.Username);
            }
            else
            {
                Assert.Fail("Participant Login Failed");
                return;
            }
        }

        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private PIV1390Data GetData()
        {
            PIV1390Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1390.json");
                data = JsonConvert.DeserializeObject<PIV1390Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
        #endregion

    }
}
