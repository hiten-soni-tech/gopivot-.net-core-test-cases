﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.DataEntities;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Linq;
using GoPivot.NetCore.ComponentHelper;


namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1894", Category = "WhiteLabel")]
    public class PIV1894 : BaseStepDefinition
    {public bool IsSuccess { get; set; }
        [Order(1)]
        [TestCase(TestName = "Scenario 1: Display User Guide Asset")]
        public void Validate_Display_User_Guide_Asset()
        {
            try
            {
                LoginAdministrator();

                Assert.IsTrue(driver.FindElement(By.XPath("//*[contains(text(),\"User's Guide\")]")).Displayed, "User Guide Label must be Displayed");
                Console.WriteLine("User Guide Label displayed");

                Assert.IsTrue(driver.FindElement(By.XPath("//*[contains(text(),\"User's Guide\")]/../div[@class='col-sm-8']/div/input[@type='text']")).Displayed, "Input File element textbox must be displayed");
                Console.WriteLine("Input File Textbox displayed");

                Assert.IsTrue(driver.FindElement(By.XPath("//*[contains(text(),\"User's Guide\")]/../div[@class='col-sm-8']/div/label[@class='input-group-btn']/*[contains(text(),'Browse')]")).Displayed, "Input File element browse button must be displayed");
                Console.WriteLine("Input File Browse button displayed");
                IsSuccess = true;

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1894 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1894 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [Order(2)]
        [TestCase(TestName = "Scenario 2: Upload User's Guide")]
        public void Validate_Upload_User_Guide()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                IWebElement userGuidePdfUploadElement = driver.FindElement(By.XPath("//*[contains(text(),\"User's Guide\")]/../div[@class='col-sm-8']/div/label[@class='input-group-btn']/*[contains(text(),'Browse')]/input[@type='file']"));
                string fileName = TestContext.CurrentContext.TestDirectory + "\\files\\userGuide.pdf";
                userGuidePdfUploadElement.SendKeys(fileName);

                Thread.Sleep(8000);

                IWebElement uploadMessageElement = driver.FindElement(By.XPath("//alert-label[@show-success='userGuideSuccess']//div[contains(@class,'alert-success')]"));
                Assert.IsTrue(uploadMessageElement.Text.Contains("success"), "User Guide upload must be success");
                Console.WriteLine("User Guide uploaded successfully");

                Assert.IsTrue(driver.FindElement(By.XPath("//a[@ng-if='program.userGuidePDF']")).Displayed, "Remove button for user guide file must be displayed");
                Console.WriteLine("Delete link for user Guide PDF available");

                Assert.IsTrue(driver.FindElement(By.XPath("//div[@ng-show='program.userGuidePDF']//a")).Displayed, "User Guide must be displayed");
                Console.WriteLine("User Guide link must be visible");

                var textboxElement = driver.FindElement(By.XPath("//*[contains(text(),\"User's Guide\")]/../div[@class='col-sm-8']/div/input[@type='text']"));
                Assert.IsTrue(textboxElement.GetAttribute("placeholder") == "userGuide.pdf", "Placeholder of file must be changed to name of file");
                Console.WriteLine("Textbox placeholder been changed to file name: userGuide.pdf");

                Thread.Sleep(3000);
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1894 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1894 Scenario 2 Failed. Error: {0}", ex);
            }
        }

        [Order(3)]
        [TestCase(TestName = "Scenario 3: Upload errors")]
        public void Validate_Upload_Errors()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            try
            {
                IWebElement userGuidePdfUploadElement = driver.FindElement(By.XPath("//*[contains(text(),\"User's Guide\")]/../div[@class='col-sm-8']/div/label[@class='input-group-btn']/*[contains(text(),'Browse')]/input[@type='file']"));
                string fileName = TestContext.CurrentContext.TestDirectory + "\\files\\heavy-pdf.pdf";
                userGuidePdfUploadElement.SendKeys(fileName);

                Thread.Sleep(7000);

                IWebElement uploadMessageElement = driver.FindElement(By.XPath("//alert-label[@show-success='userGuideSuccess']//div[@ng-show='showDanger']/div/div[contains(@class,'alert-danger')]"));
                Assert.IsTrue(uploadMessageElement.Text.Contains("The maximum file size is 5 MB. Please try again with a smaller file."), "File uploaded more than 5 MB. Error message must be displayed");
                Console.WriteLine("Error message for large pdf displayed successfully");

                Assert.IsTrue(driver.FindElement(By.XPath("//a[@ng-if='program.userGuidePDF']")).Displayed, "Remove button for user guide file must be displayed");
                Console.WriteLine("Delete link for user Guide PDF available for old file: userGuide.pdf");

                Assert.IsTrue(driver.FindElement(By.XPath("//div[@ng-show='program.userGuidePDF']//a")).Displayed, "User Guide must be displayed");
                Console.WriteLine("User Guide link visible for old file userGuide.pdf");

                var textboxElement = driver.FindElement(By.XPath("//*[contains(text(),\"User's Guide\")]/../div[@class='col-sm-8']/div/input[@type='text']"));
                Assert.IsTrue(textboxElement.GetAttribute("placeholder") == "userGuide.pdf", "Placeholder of file must be changed to name of file");
                Console.WriteLine("Textbox placeholder been changed to OLD file name: userGuide.pdf");

            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1894 Scenario 3 Error: {0}", ex.Message);
                Assert.Fail("PIV-1894 Scenario 3 Failed. Error: {0}", ex);
            }
        }

        [Order(4)]
        [TestCase(TestName = "Scenario 4: Delete asset")]
        public void Validate_Delete_Asset()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 3 not completed"); return;
            }
            try
            {
                IWebElement removeUserGuideElement = driver.FindElement(By.XPath("//a[contains(@class,'removeButton') and @ng-if='program.userGuidePDF']"));
                removeUserGuideElement.Click();
                Console.WriteLine("User Guide Delete button clicked");
                Thread.Sleep(3000);
                IWebElement deleteMessageElement = driver.FindElement(By.XPath("//alert-label[@show-success='userGuideSuccess']//div[contains(@class,'alert-success')]"));
                Assert.IsTrue(deleteMessageElement.Text.Contains("success"), "User Guide Removal success message should be appreared");
                Console.WriteLine("User Guide deleted successfully");

                var textboxElement = driver.FindElement(By.XPath("//*[contains(text(),\"User's Guide\")]/../div[@class='col-sm-8']/div/input[@type='text']"));
                Assert.IsTrue(textboxElement.GetAttribute("placeholder") == "Upload User's Guide", "Placeholder of file must be changed to Upload User's Guide");
                Console.WriteLine("Textbox placeholder been changed to Upload User's Guide");
                try
                {
                    Assert.IsFalse(driver.FindElement(By.XPath("//div[@ng-show='program.userGuidePDF']//a")).Displayed, "User Guide link must be removed");

                }
                catch
                {
                    Console.WriteLine("No User Guide available");
                }
                try
                {
                    Assert.IsFalse(driver.FindElement(By.XPath("//a[contains(@class,'removeButton') and @ng-if='program.userGuidePDF']")).Displayed, "Delete button for User Guide must NOT be displayed");

                }
                catch
                {
                    Console.WriteLine("No Delete button for User Guide found");
                }
                driver.FindElement(By.XPath("//alert-label[@show-success='userGuideSuccess']//div[contains(@class,'alert-success')]/a")).Click();
                Console.WriteLine("Success message element closed");
                Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1894 Scenario 4 Error: {0}", ex.Message);
                Assert.Fail("PIV-1894 Scenario 4 Failed. Error: {0}", ex);
            }
        }

        [Order(5)]
        [TestCase(TestName = "Scenario 5: Save asset")]
        public void Validate_Save_Asset()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 4 not completed"); return;
            }
            try
            {

                // Click on save button
                driver.FindElement(By.XPath("//input[@type='button' and @value='Save']")).Click();
                Console.WriteLine("Save Button Clicked");
                Thread.Sleep(3000);

                try
                {

                    Assert.IsFalse(driver.FindElement(By.ClassName("modal-dialog")).Displayed, "White label modal popup must be closed");
                    Console.WriteLine("Error: Modal Popup of white label still displaying");
                }
                catch
                {
                    Console.WriteLine("Modal Popup been closed");
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1894 Scenario 5 Error: {0}", ex.Message);
                Assert.Fail("PIV-1894 Scenario 5 Failed. Error: {0}", ex);
            }
        }

        private void LoginAdministrator()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");
            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }

            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            if (!adminPage.EditClient.Displayed)
            {
                Console.WriteLine("Clients Details not found");
                Assert.Fail("Clients Details not found");

            }
            else
            {
                Console.WriteLine("Client Details page loaded");
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            Thread.Sleep(2000);
            driver.FindElement(By.XPath("//div[contains(@class,'settingLabel') and contains(.,'White Label')]")).Click();
            Console.WriteLine("White Label section clicked");
            Thread.Sleep(3000);
            Assert.IsTrue(driver.FindElement(By.ClassName("modal-dialog")).Displayed, "White label modal popup must be displayed");
            Console.WriteLine("White label Modal popup displayed");

            Thread.Sleep(2000);
        }

        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}
