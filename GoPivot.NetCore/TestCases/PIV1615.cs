﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;
using System.Collections.Generic;
using System.Linq;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1615", Category = "Exercise")]
    public class PIV1615 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1615Data Data { get; set; }
        [TestCase(Description = "PIV-1615 Scenario 1: Participant browses the Get Moving fitness program video collection", TestName = "Scenario 1: Participant browses the Get Moving fitness program video collection")]
        [Order(1)]
        public void Validate_Get_Moving_Fitness_Video_Collection()
        {
            try
            {
                LoginParticipant();
                ValidateFitnessProgram("Get Moving");
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1615 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1615 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        private void ValidateFitnessProgram(string name)
        {
            if (Data == null)
            {
                Data = GetData();
            }

            if (Data == null)
            {
                Console.WriteLine("No Valid Input Data found");
                Assert.Fail("No Valid Input Data found");
            }
            IWebElement exerciseTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Exercise")));
            exerciseTab.Click();
            Console.WriteLine("Exercise Link clicked");
            Thread.Sleep(5000);

            var fitnessProgramData = Data.ExerciseProgram.FirstOrDefault(p => p.Name == name);
            if (fitnessProgramData == null)
            {
                Assert.Fail("{0} Fitness program not found", name);
            }
            var fitnessProgramElement = driver.FindElement(By.XPath(string.Format("//div[contains(@class,'fitnessProgram')]/h2[contains(.,'{0}')]", name)));
            fitnessProgramElement.Click();
            Thread.Sleep(4000);

            var fitnessProgramTabElement = driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li[contains(@class,'active')]"));
            Assert.IsTrue(fitnessProgramTabElement.Text.Contains(name), "{0} Tab must be active", name);
            Console.WriteLine("{0} Tab is active", name);
            Thread.Sleep(3000);
            IList<IWebElement> videoList = driver.FindElements(By.XPath("//div[@class='playlistVideo']"));
            Assert.IsTrue(videoList.Count == fitnessProgramData.VideoCount, "Video count must be {0} for {1} fitness program", fitnessProgramData.Name);
            Console.WriteLine("{0} Fitness Program Video count matching", videoList.Count);

            IList<string> iFrameSourceList = new List<string>();
            var iframeElement = driver.FindElement(By.XPath("//div[contains(@class,'watchVideo')]//iframe")).GetAttribute("src");
            iFrameSourceList.Add(iframeElement);
            Assert.IsTrue(iframeElement.Contains("youtube"), "Default iFrame must contains reference to youtube video");
            foreach (var video in videoList)
            {
                video.Click();
                Console.WriteLine("{0} Video clicked", video.Text);
                Thread.Sleep(2000);
                iframeElement = string.Empty;
                iframeElement = driver.FindElement(By.XPath("//div[contains(@class,'watchVideo')]//iframe")).GetAttribute("src");
                Assert.IsTrue(iframeElement.Contains("youtube"), "iFrame must contains reference to youtube video");
                if (!iFrameSourceList.Contains(iframeElement))
                {
                    iFrameSourceList.Add(iframeElement);
                    Console.WriteLine("Video {0} having Video Link {1}", video.Text, iframeElement);
                }
            }
            Assert.IsTrue(iFrameSourceList.Count == videoList.Count, "All Video Links must be matching to video count");
            Console.WriteLine("All video links are having valid video url");
        }

        [TestCase(Description = "PIV-1615 Scenario 2: Participant browses the Foundation fitness program video collection", TestName = "Scenario 2: Participant browses the Foundation fitness program video collection")]
        [Order(2)]
        public void Validate_Fitness_Foundation_Program_Video_Collection()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 1 not completed"); return;
            }
            try
            {
                ValidateFitnessProgram("Foundation");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1615 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1615 Scenario 2 Failed. Error: {0}", ex);
            }

        }

        [TestCase(Description = "PIV-1615 Scenario 3: Participant browses the Compact fitness program video collection", TestName = "Scenario 3: Participant browses the Compact fitness program video collection")]
        [Order(3)]
        public void Validate_Compact_Fitness_Program_Video_Collection()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 2 not completed"); return;
            }
            try
            {
                ValidateFitnessProgram("Compact");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1615 Scenario 3 Error: {0}", ex.Message);
                Assert.Fail("PIV-1615 Scenario 3 Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-1615 Scenario 4: Participant browses the Amplify fitness program video collection", TestName = "Scenario 4: Participant browses the Amplify fitness program video collection")]
        [Order(4)]
        public void Validate_Amplify_Fitness_Program_Video_Collection()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 3 not completed"); return;
            }
            try
            {
                ValidateFitnessProgram("Amplify");
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1615 Scenario 4 Error: {0}", ex.Message);
                Assert.Fail("PIV-1615 Scenario 4 Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-1615 Scenario 5: Participant browses the Unleasehd fitness program video collection", TestName = "Scenario 5: Participant browses the Unleasehd fitness program video collection")]
        [Order(5)]
        public void Validate_Unleashed_Fitness_Program_Video_Collection()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Scenario 4 not completed"); return;
            }
            try
            {
                ValidateFitnessProgram("Unleashed");
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1615 Scenario 5 Error: {0}", ex.Message);
                Assert.Fail("PIV-1615 Scenario 5 Failed. Error: {0}", ex);
            }
        }

        #region Utilities

        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }


        private void LogoutParticipant()
        {
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private PIV1615Data GetData()
        {
            PIV1615Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1615.json");
                data = JsonConvert.DeserializeObject<PIV1615Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
