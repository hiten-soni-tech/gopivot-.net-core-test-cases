﻿using System;
using System.IO;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.DataEntities;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Linq;
using System.Collections.Generic;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(Category = "Activity")]
    public class PIV1518 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1518Data Data { get; set; }
        [Order(1)]
        [TestCase(TestName = "Scenario 0 - Pre-requisite Create new activity for sync devices only")]
        public void Prerequisite()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAndSelectActivity();
                Thread.Sleep(3000);
                driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
                Console.WriteLine("Add Activity button clicked");

                Thread.Sleep(5000);
                Data.NewActivity = new AdminPage(driver).CreateNewActivity(Data.NewActivity, errorMessage: out string errorMessage);
                if (Data.NewActivity == null)
                {
                    Assert.Fail(errorMessage);
                }
                Console.WriteLine("Activity created successfully. Name: {0}", Data.NewActivity.Name);

                LogoutAdministrator();
                Thread.Sleep(1000);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1518 Scenario 0 Error: {0}", ex.Message);
                Assert.Fail("PIV-1518 Scenario 0 Failed. Error: {0}", ex);
            }

        }
        [Order(2)]
        [TestCase(TestName = "Scenario 1 - Validate Activity Visibility for Non Active Device Users")]
        public void Validate_Acvitity_Visibility_For_Not_Active_Device()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 1 not completed"); return;
                }
                string activityName = string.Empty;
                if (Data?.NewActivity != null)
                {
                    activityName = Data.NewActivity.Name;
                }
                else
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                Thread.Sleep(3000);
                LoginParticipant(ObjectRepository.Setting.Credentials.Participant.Username, ObjectRepository.Setting.Credentials.Participant.Password);
                Thread.Sleep(8000);
                var myProfileTab = GetWebDriverWait()
         .Until(ExpectedConditions.ElementToBeClickable(By.LinkText("My Profile")));
                if (myProfileTab != null && myProfileTab.Displayed)
                {
                    myProfileTab.Click();
                    Console.WriteLine("My Profile Tab Clicked");
                }
                Thread.Sleep(8000);
                driver.FindElement(By.XPath("//li[contains(@class,'nav-item')]/a[contains(.,'Trackers')]")).Click();
                Thread.Sleep(3000);
                Console.WriteLine("Trackers Section selected");


                var elements = driver.FindElements(By.LinkText("Disconnect"));
                Assert.IsTrue(elements.Count == 0, "No Devices should be connected for participant {0}", ObjectRepository.Setting.Credentials.Participant.Username);
                Console.WriteLine("No Devices connected for participant {0}", ObjectRepository.Setting.Credentials.Participant.Username);

                Thread.Sleep(2000);
                IWebElement activityTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Activities")));
                activityTab.Click();
                Console.WriteLine("Activities Link clicked");
                Thread.Sleep(5000);

                try
                {
                    string activityLinkXPath =
                     string.Format(
                         "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
                         activityName);
                    IWebElement activityElement = driver.FindElement(By.XPath(activityLinkXPath));
                    Assert.IsFalse(activityElement.Displayed, string.Format("Activity {0} must be visible in activity list", activityName));
                    Console.WriteLine("ERROR: Activity {0} available in activity list", activityName);
                }
                catch
                {
                    Console.WriteLine("Activity {0} NOT available in activity list", activityName);
                }

                LogoutParticipant();
                Thread.Sleep(3000);
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1518 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1518 Scenario 1 Failed. Error: {0}", ex);
            }

        }

        [Order(3)]
        [TestCase(TestName = "Scenario 2 - Validate Activity Visibility for Active Device Users")]
        public void Validate_Activity_Visibility_For_Active_Device()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 2 not completed"); return;
                }
                string activityName = string.Empty;
                if (Data?.NewActivity != null)
                {
                    activityName = Data.NewActivity.Name;
                }
                else
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                Thread.Sleep(3000);
                LoginParticipant(ObjectRepository.Setting.Credentials.NewParticipant.Username, ObjectRepository.Setting.Credentials.NewParticipant.Password);
                Thread.Sleep(10000);
                var myProfileTab = GetWebDriverWait()
         .Until(ExpectedConditions.ElementToBeClickable(By.LinkText("My Profile")));
                if (myProfileTab != null && myProfileTab.Displayed)
                {
                    myProfileTab.Click();
                    Console.WriteLine("My Profile Tab Clicked");
                }
                Thread.Sleep(10000);
                driver.FindElement(By.XPath("//li[contains(@class,'nav-item')]/a[contains(.,'Trackers')]")).Click();
                Thread.Sleep(3000);
                Console.WriteLine("Trackers Section selected");


                var elements = driver.FindElements(By.LinkText("Disconnect"));
                Assert.IsTrue(elements.Count > 0, "Atleast one Device should be connected for participant {0}", ObjectRepository.Setting.Credentials.NewParticipant.Username);
                Console.WriteLine("Atleast one Device connected for participant {0}", ObjectRepository.Setting.Credentials.NewParticipant.Username);
                Thread.Sleep(3000);

                IWebElement activityTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Activities")));
                activityTab.Click();
                Console.WriteLine("Activities Link clicked");
                Thread.Sleep(5000);

                string activityLinkXPath =
                  string.Format(
                      "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
                      activityName);
                IWebElement activityElement = driver.FindElement(By.XPath(activityLinkXPath));
                Assert.IsTrue(activityElement.Displayed, string.Format("Activity {0} must be visible in activity list", activityName));
                Console.WriteLine("Activity {0} available in activity list", activityName);
                LogoutParticipant();
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1518 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1518 Scenario 2 Failed. Error: {0}", ex);
            }
        }

        #region Utilities
        private void LoginParticipant(string username, string password)
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(username);
            loginPage.PassTextBox.SendKeys(password);
            Thread.Sleep(5000);
            loginPage.LoginButton.Click();

            Thread.Sleep(3000);

            var logoutElement =
                GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement != null && logoutElement.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully",
                   username);
            }
            else
            {
                Assert.Fail("Participant {0} Login Failed", username);
                return;
            }
        }

        private void LogoutParticipant()
        {
            Thread.Sleep(3000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }

        private void LoginAndSelectActivity()
        {
            Thread.Sleep(3000);
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);
            var activitiesTab = adminPage.GetProgramTab("Activities");
            if (activitiesTab == null)
            {
                Console.WriteLine("Unable to find activities tab");
                Assert.Fail("Unable to find activities tab");
            }
            else
            {
                Console.WriteLine("Activities Tab loaded");
                activitiesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(6000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

        }

        private void LogoutAdministrator()
        {
            Thread.Sleep(4000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                Thread.Sleep(2000);
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }
        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private PIV1518Data GetData()
        {
            PIV1518Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1518.json");
                data = JsonConvert.DeserializeObject<PIV1518Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
        #endregion
    }
}
