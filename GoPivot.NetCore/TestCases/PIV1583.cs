﻿using System;
using System.Threading;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1583", Category = "Nutrition")]
    public class PIV1583 : BaseStepDefinition
    {
        [TestCase(
            Description =
                "PIV 1583  - As a participant, I want an option to switch out the recipe that has been presented for a particular meal so that I can consider another option.",
            TestName = "Nutrition UI - Change Recipe")]
        public void SwitchRecipe()
        {
            var errorMessage = string.Empty;
            try
            {
                var loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
                loginPage.LoginButton.Click();

                Thread.Sleep(2000);

                var logoutElement =
                    GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    Console.WriteLine("Participant {0} Logged in successfully",
                        ObjectRepository.Setting.Credentials.Participant.Username);
                }
                else
                {
                    Assert.Fail("Participant Login Failed");
                    return;
                }

                var nutritionTab = GetWebDriverWait()
                    .Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }

                Thread.Sleep(1000);

                BuildRecipePlanIfNotExists();
                Thread.Sleep(3000);
                var dinnerMealElement = driver.FindElement(By.XPath(
                    "//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Dinner')]/../div[@class='recipeContainer']"));
                Assert.IsTrue(dinnerMealElement.Displayed, "Dinner element should be displayed");
                Console.WriteLine("Dinner Meal element available");

                var dinnerMealTextElement = dinnerMealElement.FindElement(By.ClassName("recipeTitle"));
                Assert.IsTrue(dinnerMealElement.Displayed, "Dinner Meal Title should be displayed");
                var currentMealTitle = dinnerMealTextElement.Text.Replace("Change Recipe", "");
                Console.WriteLine("Current Dinner Meal Title is {0}", currentMealTitle);

                dinnerMealElement.FindElement(By.LinkText("Change Recipe")).Click();
                Thread.Sleep(5000);
                dinnerMealElement = driver.FindElement(By.XPath(
                    "//div[contains(@class,'recipeWrapper')]/h2[contains(.,'Dinner')]/../div[@class='recipeContainer']"));
                var newMealTitle = dinnerMealElement.FindElement(By.ClassName("recipeTitle")).Text
                    .Replace("Change Recipe", "");
                Console.WriteLine("New Dinner Meal Title is {0}", newMealTitle);

                Assert.IsFalse(currentMealTitle.Equals(newMealTitle),
                    "New Dinner meal recipe should not be equal to old meal served");
                Console.WriteLine("New Dinner Meal is not matching with Old Dinner Meal");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("PIV-1583 Failed. Error: {0}", ex.Message);
                Assert.Fail("PIV-1583 Failed. Error: {0}", ex);
            }
        }



        private void BuildRecipePlanIfNotExists()
        {
            try
            {
                driver.FindElement(By.LinkText("Build Your Recipe Plan")).Click();
                Console.WriteLine("Build Your Recipe Plan clicked");

                driver.FindElement(By.XPath("//input[@ng-model='model.desiredWeight']")).SendKeys("100");
                driver.FindElement(By.XPath("//input[@type='button' and @value = 'Save']")).Click();
                Console.WriteLine("Save Button clicked");

                Thread.Sleep(3000);

                var nutritionTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Nutrition")));
                if (nutritionTab != null && nutritionTab.Displayed)
                {
                    nutritionTab.Click();
                    Console.WriteLine("Nutrition Tab Clicked");
                }
                Thread.Sleep(3000);
            }
            catch
            {
                Console.WriteLine("Button: Build Your Recipe Plan not found");
                Console.WriteLine("Recipe Plan already available for this user");
                // Do nothing
            }
        }

        private WebDriverWait GetWebDriverWait()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}