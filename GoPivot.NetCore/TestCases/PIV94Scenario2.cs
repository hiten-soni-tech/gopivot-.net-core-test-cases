﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;
using GoPivot.NetCore.Settings;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-94 Scenario 2", Category = "Challenge")]
    public class PIV94Scenario2 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        private LoginPage loginPage;
        private string GeneratedChallengeName;
        [TestCase(Description = "Create new Jump Start Your Day Challenge", TestName = "Create new Jump Start Your Day Challenge")]
        public void CreateNewJumpStartYourDayChallenge()
        {
            string errorMessage = string.Empty;
            try
            {
                var data = GetData();
                if (data == null)
                {
                    Assert.Fail("No Valid Input Data found");
                    Console.WriteLine("No Valid Input Data found");
                }
                loginPage = new LoginPage(driver);
                loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
                loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
                loginPage.LoginButton.Click();

                Thread.Sleep(3000);
                var adminPage = new AdminPage(driver);
                if (adminPage.Clients != null && adminPage.Clients.Displayed)
                {
                    adminPage.Clients.Click();
                    Console.WriteLine("Clients link clicked");
                }
                else
                {
                    Console.Error.WriteLine("Clients Link not found");
                    Assert.Fail("Clients link not found");
                }

                Thread.Sleep(3000);
                var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
                if (clientElement != null && clientElement.Displayed)
                {
                    Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                    clientElement.Click();
                }
                else
                {
                    Console.Error.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                    Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                }

                if (!adminPage.EditClient.Displayed)
                {
                    Console.Error.WriteLine("Clients Details not found");
                    Assert.Fail("Clients Details not found");

                }
                else
                {
                    Console.WriteLine("Client Details page loaded");
                }

                var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
                if (programElement != null && programElement.Displayed)
                {
                    programElement.Click();
                    Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
                }
                else
                {
                    Console.Error.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
                }

                if (adminPage.ProgramTabs.Count > 0)
                {
                    Console.WriteLine("Program Listing page loaded");

                }
                else
                {
                    Console.Error.WriteLine("Program details page not loaded");
                    Assert.Fail("Failed to load Program details");
                }

                Thread.Sleep(1000);
                var challengeTab = adminPage.GetProgramTab("Challenges");
                if (challengeTab == null)
                {
                    Console.Error.WriteLine("Unable to find challenges tab");
                    Assert.Fail("Unable to find challenges tab");
                }
                else
                {
                    Console.WriteLine("Challenge Tab loaded");
                    challengeTab.Click();
                }

                Thread.Sleep(5000);
                adminPage.GetAddChallengeButton();
                if (adminPage.AddChallengeButton != null)
                {
                    adminPage.AddChallengeButton.Click();
                    Console.WriteLine("Add Challenge Button clicked");
                    Thread.Sleep(2000);
                }
                else
                {
                    Console.Error.WriteLine("Cannot add new challenge");
                    Assert.Fail("cannot add new challenge");
                }

                Thread.Sleep(2000);
                var challengeCategory = adminPage.GetChallengeCategory("Wellness");
                if (challengeCategory != null)
                {
                    challengeCategory.Click();
                    Console.WriteLine("Wellness Challenge type selected");
                }
                else
                {
                    Console.Error.WriteLine("Wellness Challenge Type not available");
                    Assert.Fail("Wellness Challenge Type not available");
                }

                Thread.Sleep(2000);
                var challengeType = adminPage.GetChallengeType(data.Challenge.WellnessChallengeType);
                if (challengeType != null)
                {
                    challengeType.Click();
                    Console.WriteLine("{0} Challenge Type selected", data.Challenge.WellnessChallengeType);
                }
                else
                {
                    errorMessage = string.Format("{0} Challenge type not found", data.Challenge.WellnessChallengeType);
                    Console.Error.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
                string randomNumber = new Random().Next(1000).ToString();
                string challengename = data.Challenge.ChallengeName.Replace("{currentday}", DateTime.Now.ToShortDateString());
                challengename = challengename.Replace("{randomnumber}", randomNumber);
                GeneratedChallengeName = challengename;
                string startdate = data.Challenge.StartDate.Replace("{currentday}", DateTime.Now.ToShortDateString());
                bool success = addNewChallengePage.FillStepForJumpStart(challengename, startdate, data.Challenge.EndDate, data.Challenge.Description, out errorMessage);
                if (success)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Add Challenge information submitted {{Step 1}}");
                    addNewChallengePage.Step1_NextButton.Click();
                }
                else
                {
                    Console.Error.WriteLine(errorMessage);
                    Assert.Fail(errorMessage);
                }

                var participantCount = addNewChallengePage.Step2_ParticipantList;
                if (participantCount != null && participantCount.Count > 0)
                {
                    Console.WriteLine("Participants available to select");
                }
                else
                {
                    Console.Error.WriteLine("No Participants available");
                    Assert.Fail("No Participants available");
                }

                success = addNewChallengePage.AddChallengers(data.Challenge.ChallengerName, out errorMessage);
                if (success)
                {
                    Console.WriteLine("Participant {0} added to challenge", data.Challenge.ChallengerName);
                }
                else
                {
                    Console.Error.WriteLine("Failed to add {0} challenger. Error: {1}", data.Challenge.ChallengerName, errorMessage);
                    Assert.Fail(errorMessage);
                }

                addNewChallengePage.Step2_CreateButton.Click();
                Console.WriteLine("Create Challenge button clicked");

                WebDriverWait wait = GetWebDriverWait();
                var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
                if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
                {
                    string message = string.Format("Challenge {0} added successfully", challengename);
                    Console.WriteLine(message);
                    Assert.IsTrue(addedChallenge.Displayed);
                }
                else
                {
                    errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                    Assert.Fail(errorMessage);
                    Console.Error.WriteLine(errorMessage);
                }
                IsSuccess = true;
            }

            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("Test Case Failed. Error: {0}", ex.Message);
                Assert.Fail("Creating Jump Start Your Day Challenge Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "Join the Challenge and Add Goals", TestName = "Join the Challenge and Add Goals")]
        public void JoinTheChallengeAndAddGoals()
        {
            if (!IsSuccess)
            {
                Assert.Warn("Failed to create challenge. Scenario cannot be validated."); return;
            }

            if (string.IsNullOrEmpty(GeneratedChallengeName))
            {
                Console.Error.WriteLine("Challenge not available");
                Assert.Fail("Challenge not available");
            }

            var data = GetData();
            if (data == null)
            {
                Console.Error.WriteLine("Data not available");
                Assert.Fail("Data not available");
            }
            Thread.Sleep(2000);

            By loadingImage = By.TagName("http-busy");
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }


            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));
            Thread.Sleep(2000);
            loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);

            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }

            IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
            if (challengeTab != null && challengeTab.Displayed)
            {
                challengeTab.Click();
                Console.WriteLine("Challenge Tab Clicked");
            }

            Thread.Sleep(1000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

            var challengeList = driver.FindElements(By.XPath("//div[@class='panel challenge ng-scope']"));
            if (challengeList != null && challengeList.Count > 0)
            {
                Console.WriteLine("{0} Challenges available for participant", challengeList.Count);
            }
            else
            {
                Console.Error.WriteLine("Failed to load challenges");
                Assert.Fail("Failed to load challenges");
            }

            var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", GeneratedChallengeName))));
            if (challenge != null && challenge.Displayed)
            {
                challenge.Click();
                Console.WriteLine("{0} available challenge found", GeneratedChallengeName);
            }
            else
            {
                Console.Error.WriteLine("{0} available challenge not found", GeneratedChallengeName);
                Assert.Fail("{0} available challenge not found", GeneratedChallengeName);
            }

            Thread.Sleep(3000);

            IWebElement joinButtonElement = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(.,'Join!')]")));
            joinButtonElement.Click();
            Console.WriteLine("Join Button available and clicked to join this challenge");
            string challengestartdate = data.Challenge.StartDate;
            string challengeenddate = data.Challenge.EndDate;
            challengestartdate = challengestartdate.Replace("{currentday}", DateTime.Now.ToShortDateString());
            DateTime challengeStartDate = DateTime.Now;
            DateTime challengeEndDate = DateTime.Now;

            DateTime.TryParse(challengestartdate, out challengeStartDate);
            DateTime.TryParse(challengeenddate, out challengeEndDate);

            int numberOfMonths = Math.Abs(((challengeStartDate.Year - challengeEndDate.Year) * 12) + challengeStartDate.Month - challengeEndDate.Month) + 1;

            IList<IWebElement> calendarContainers = driver.FindElements(By.XPath("//div[@class = 'calendarMonth ng-binding']"));
            if (calendarContainers != null && calendarContainers.Count > 0 && calendarContainers.Count == numberOfMonths)
            {
                Console.WriteLine("{0} Calendar(s) found", calendarContainers.Count);

            }
            else
            {
                Console.Error.WriteLine("No Calendars available");
                Assert.Fail("No Calendars available");
            }

            string month = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);

            IList<IWebElement> calendarDays = driver.FindElements(By.XPath(string.Format("//div[@class = 'calendarMonth ng-binding'  and contains(.,'{0}')]/../div[contains(@class,'calendarDays')]", month)));
            if (calendarDays != null && calendarDays.Count > 0)
            {
                var dateElement = calendarDays.FirstOrDefault(p => p.Text == DateTime.Now.Day.ToString());
                if (dateElement != null)
                {
                    var iconCheck = dateElement.FindElement(By.ClassName("icon-check"));
                    dateElement.Click();
                    Console.WriteLine("Current Date {0} selected to log goal for the day", DateTime.Now.ToShortDateString());
                }
                else
                {
                    Console.WriteLine("Goal already logged for current date {0}", DateTime.Now.ToShortDateString());
                }

            }
            else
            {
                Console.Error.WriteLine("No Calendar found to select goal for current date");
                Assert.Fail("No Calendar found to select goal for current date");
            }
            Thread.Sleep(4000);
            var goalCountElement = driver.FindElement(By.XPath("//div[contains(@class,'goalMetNumber')]"));
            string goalCountText = goalCountElement.Text;

            int.TryParse(goalCountText, out int goalCount);
            Assert.True(goalCount > 0, "Goal Count should be greater than 0");
            Console.WriteLine("Goal Count is greater than 0. Actual Count: {0}", goalCount);



        }

        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private DateTime GetStartOfWeek(DateTime input)
        {
            // Using +6 here leaves Monday as 0, Tuesday as 1 etc.
            int dayOfWeek = (((int)input.DayOfWeek) + 6) % 7;
            return input.Date.AddDays(-dayOfWeek);
        }

        private int GetWeeks(DateTime start, DateTime end)
        {
            start = GetStartOfWeek(start);
            end = GetStartOfWeek(end);
            int days = (int)(end - start).TotalDays;
            return (days / 7) + 1; // Adding 1 to be inclusive
        }

        private PIV94Scenario2Data GetData()
        {
            PIV94Scenario2Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV94Scenario2.json");
                data = JsonConvert.DeserializeObject<PIV94Scenario2Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }
    }
}
