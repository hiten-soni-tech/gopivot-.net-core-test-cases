﻿using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;
using NUnit.Framework;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1521", Category = "Activity")]
    public class PIV1521 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1521Data Data { get; set; }
        [TestCase(Description = "PIV-1521 Pre-requisities (Create Activities)", TestName = "Pre-requisities (Create Activities)")]
        [Order(1)]
        public void Validate_Pre_Requisites()
        {
            try
            {
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAndSelectActivity();

                driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
                Console.WriteLine("Add Activity button clicked");

                Thread.Sleep(5000);

                Data.NewActivity1 = new AdminPage(driver).CreateNewActivity(Data.NewActivity1, errorMessage: out string errorMessage);
                if (Data.NewActivity1 == null)
                {
                    Assert.Fail(errorMessage);
                }
                Console.WriteLine("Activity 1 created successfully. Name: {0}", Data.NewActivity1.Name);
                Thread.Sleep(7000);

                driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
                Console.WriteLine("Add Activity button clicked");
                Thread.Sleep(5000);
                Data.NewActivity2.ExcludedActivity = Data.NewActivity1.Name;
                Data.NewActivity2 = new AdminPage(driver).CreateNewActivity(Data.NewActivity2, errorMessage: out errorMessage);
                if (Data.NewActivity2 == null)
                {
                    Assert.Fail(errorMessage);
                }
                Console.WriteLine("Activity 2 created successfully. Name: {0}", Data.NewActivity2.Name);
                LogoutAdministrator();
                Thread.Sleep(1000);
                Console.WriteLine("Activities created successfully");
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1521 Pre-requisites Error: {0}", ex.Message);
                Assert.Fail("PIV-1521 Pre-requisites Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-1521 An activity has been selected in the exclusion dropdown; Participant is eligible for the excluded activity", TestName = "An activity has been selected in the exclusion dropdown; Participant is eligible for the excluded activity")]
        [Order(2)]
        public void Validate_Excluded_Activity_Visibility_In_Participant()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 1 not completed"); return;
                }
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                string activity1Name = Data.NewActivity1.Name;
                string activity2Name = Data.NewActivity2.Name;
                LoginParticipant();

                IWebElement activityTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Activities")));
                activityTab.Click();
                Console.WriteLine("Activities Link clicked");
                Thread.Sleep(5000);

                string activity1LinkXPath =
                  string.Format(
                      "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
                      activity1Name);

                IWebElement activity1Element = driver.FindElement(By.XPath(activity1LinkXPath));
                Assert.IsTrue(activity1Element.Displayed, string.Format("Activity {0} must be visible in activity list", activity1Name));
                Console.WriteLine("Activity {0} available in activity list", activity1Name);

                string activity2LinkXPath =
  string.Format(
      "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
      activity2Name);

                try
                {
                    IWebElement activity2Element = driver.FindElement(By.XPath(activity2LinkXPath));
                    Assert.IsFalse(activity2Element.Displayed, string.Format("Activity {0} must NOT be visible in activity list", activity2Name));
                }
                catch
                {
                    Console.WriteLine("Activity {0} not available in activity list", activity2Name);
                }
                LogoutParticipant();
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Console.WriteLine("PIV-1521 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1521 Scenario 1  Failed. Error: {0}", ex);
         
            }
        }

        [TestCase(Description = "PIV-1521 An activity has been selected in the exclusion dropdown; Participant is NOT eligible for the excluded activity", TestName = "An activity has been selected in the exclusion dropdown; Participant is NOT eligible for the excluded activity")]
        [Order(3)]
        public void Validate_Actual_Activity_Visibility_In_Participant()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("Scenario 2 not completed"); return;
                }
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                string activity1Name = Data.NewActivity1.Name;
                string activity2Name = Data.NewActivity2.Name;
                LoginAndSelectActivity();
                Thread.Sleep(2000);
                string activityXPath =
                    string.Format("//tr[contains(@class,'itemRow')]/td[contains(.,'{0}')]", activity1Name);
                driver.FindElement(By.XPath(activityXPath)).Click();
                Console.WriteLine("Activity {0} clicked to edit", activity1Name);
                Thread.Sleep(8000);
                GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//button[contains(.,'Next')]"))).Click();
                Thread.Sleep(3000);
                GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//button[contains(.,'Next')]"))).Click();
                Thread.Sleep(5000);
                GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//md-radio-button[@aria-label='Men Only']"))).Click();

                Thread.Sleep(3000);
                GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//button[contains(.,'Save')]"))).Click();

                Console.WriteLine("Activity {0} updated successfully", activity1Name);
                LogoutAdministrator();
                Thread.Sleep(3000);
                LoginParticipant();



                IWebElement activityTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Activities")));
                activityTab.Click();
                Console.WriteLine("Activities Link clicked");
                Thread.Sleep(5000);

                string activity1LinkXPath =
                  string.Format(
                      "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
                      activity1Name);

                try
                {
                    IWebElement activity1Element = driver.FindElement(By.XPath(activity1LinkXPath));
                    Assert.IsFalse(activity1Element.Displayed, string.Format("Activity {0} must NOT be visible in activity list", activity1Name));
                    Console.WriteLine("Activity {0} NOT available in activity list", activity1Name);
                }
                catch
                {
                    Console.WriteLine("Activity {0} NOT available in activity list", activity1Name);
                }

                string activity2LinkXPath =
  string.Format(
      "//div[contains(@class,'activity')]//div[contains(@class,'name') and contains(.,'{0}')]",
      activity2Name);
                try
                {
                    IWebElement activity2Element = driver.FindElement(By.XPath(activity2LinkXPath));
                    Assert.True(activity2Element.Displayed, string.Format("Activity {0} must be visible in activity list", activity2Name));
                    Console.WriteLine("Activity {0} visible in activity list", activity2Name);
                }
                catch
                {
                    Console.WriteLine("BUG: Activity {0} not available in activity list", activity2Name);
                    Assert.Fail("BUG: Activity {0} not available in activity list", activity2Name);
                }
                LogoutParticipant();
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1521 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1521 Scenario 2  Failed. Error: {0}", ex);
            }
        }



        #region Utilities

        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            Thread.Sleep(2000);
            loginPage.LoginButton.Click();
            Thread.Sleep(1000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }

        private void LogoutParticipant()
        {
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private void LogoutAdministrator()
        {
            Thread.Sleep(5000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static void LoginAndSelectActivity()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            Thread.Sleep(5000);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);
            var activitiesTab = adminPage.GetProgramTab("Activities");
            if (activitiesTab == null)
            {
                Console.WriteLine("Unable to find activities tab");
                Assert.Fail("Unable to find activities tab");
            }
            else
            {
                Console.WriteLine("Activities Tab loaded");
                activitiesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(6000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

        }

        private PIV1521Data GetData()
        {
            PIV1521Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1521.json");
                data = JsonConvert.DeserializeObject<PIV1521Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
