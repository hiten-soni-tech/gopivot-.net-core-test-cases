﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.Collections.Generic;
using System.Linq;
using GoPivot.NetCore.DataEntities;
using System.IO;
using Newtonsoft.Json;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1339", Category = "Exercise")]
    public class PIV1339 : BaseStepDefinition
    {
        [TestCase(Description = "PIV-1339 Administrator - Update the text at the top of the Change Workout modal.", TestName = "Update the text at the top of the Change Workout modal")]
        public void Update_Text_At_The_Top_Of_Change_Workout_Model()
        {
            try
            {
                var data = GetData();
                if (data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginParticipant();
                IWebElement exerciseTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Exercise")));
                exerciseTab.Click();
                Console.WriteLine("Exercise Link clicked");
                Thread.Sleep(3000);

                IList<IWebElement> workoutElements = driver.FindElements(By.XPath("//div[@class='fitnessPlanWorkout']//div[@class='fitnessPlanTitle']/a"));
                Console.WriteLine("{0} Workout elements found", workoutElements.Count);

                if (workoutElements.Count > 0)
                {
                    workoutElements.FirstOrDefault().Click();
                    Console.WriteLine("{0} Workout selected", workoutElements.FirstOrDefault().Text);
                    Thread.Sleep(2000);
                }
                else
                {
                    Console.WriteLine("No Workouts available");
                    Assert.Fail("No Workouts available");
                }
                driver.FindElement(By.XPath("//button[contains(.,'Change Workout')]")).Click();
                Console.WriteLine("Change Worklog clicked");
                Thread.Sleep(2000);
                Assert.IsTrue(driver.FindElement(By.ClassName("modal-dialog")).Displayed, "Modal popup should be displayed");
                Console.WriteLine("Modal popup displayed");

                IWebElement textElement = driver.FindElement(By.XPath("//div[contains(@class,'modal-body')]/p"));
                Assert.IsTrue(textElement.Text.Equals(data.ChangeWorkoutText), "Modal popup should contains text message provided");
                Console.WriteLine("Text matching in modal popup");
                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//div[@class='modal-footer']//button[contains(.,'Cancel')]")).Click();
                Console.WriteLine("Cancel button clicked");
                Thread.Sleep(1000);
                LogoutParticipant();

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1339 Error: {0}", ex.Message);
                Assert.Fail("PIV-1339 Failed. Error: {0}", ex);
            }
        }

        #region Utilities

        private PIV1339Data GetData()
        {
            PIV1339Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1339.json");
                data = JsonConvert.DeserializeObject<PIV1339Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private void LoginParticipant()
        {
            var loginPage = new LoginPage(driver);
            Console.WriteLine("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Participant.Password);
            loginPage.LoginButton.Click();
            Thread.Sleep(3000);
            var logoutElement1 = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
            if (logoutElement1 != null && logoutElement1.Displayed)
            {
                Console.WriteLine("Participant {0} Logged in successfully", ObjectRepository.Setting.Credentials.Participant.Username);
            }
            else
            {
                Console.Error.WriteLine("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }
        }


        private void LogoutParticipant()
        {
            var dropdownMenuElement = driver.FindElement(By.XPath("//div[contains(@class,'profileMenu')]"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Participant logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
