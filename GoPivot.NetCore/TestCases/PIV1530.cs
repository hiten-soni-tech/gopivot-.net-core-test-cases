﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using GoPivot.NetCore.BaseClasses;
using GoPivot.NetCore.PageObject;
using GoPivot.NetCore.Settings;
using System.IO;
using GoPivot.NetCore.DataEntities;
using Newtonsoft.Json;
using GoPivot.NetCore.ComponentHelper;

namespace GoPivot.NetCore.TestCases
{
    [TestFixture(TestName = "PIV-1530", Category = "Activity")]
    public class PIV1530 : BaseStepDefinition
    {
        public bool IsSuccess { get; set; }
        public PIV1530Data Data { get; set; }
        [TestCase(Description = "PIV-1530  Point value no longer required for One & Done and Recurring Activities", TestName = "Point value no longer required for One & Done and Recurring Activities")]
        [Order(1)]
        public void Validate_If_Points_No_Longer_Required_While_Creating_Activity()
        {
            try
            {
                
                Data = GetData();
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }
                LoginAndSelectActivity();

                driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
                Console.WriteLine("Add Activity button clicked");

                Thread.Sleep(5000);
                Data.NewActivity = new AdminPage(driver).CreateNewActivity(Data.NewActivity, errorMessage: out string errorMessage);
                if (Data.NewActivity == null)
                {
                    Assert.Fail(errorMessage);
                }
                Console.WriteLine("Activity created successfully. Name: {0}", Data.NewActivity.Name);
                Console.WriteLine("Points been set 0 which means points are not required while creating new activity");
                Thread.Sleep(1000);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1530 Scenario 1 Error: {0}", ex.Message);
                Assert.Fail("PIV-1530 Scenario 1 Failed. Error: {0}", ex);
            }
        }

        [TestCase(Description = "PIV-1530 Add Point Display Attribute section for One & Done & Recurring Activities", TestName = "Add Point Display Attribute section for One & Done & Recurring Activities")]
        [Order(2)]
        public void Validate_Point_Display_Attribute_Section()
        {
            try
            {
                if (!IsSuccess)
                {
                    Assert.Warn("First Scenario not completed"); return;
                }
                if (Data == null)
                {
                    Console.WriteLine("No Valid Input Data found");
                    Assert.Fail("No Valid Input Data found");
                }

                driver.FindElement(By.XPath("//button[contains(.,'Add Activity')]")).Click();
                Console.WriteLine("Add Activity button clicked");
                string randomNumber = new Random().Next(1000).ToString();
                string activityName = Data.NewActivity.Name.Replace("{randomnumber}", randomNumber);
                Data.NewActivity.Name = activityName;
                driver.FindElement(By.Id("name")).SendKeys(activityName);
                ComboBoxHelper.SelectElement(By.XPath("//select[@ng-model = 'model.activitySectionId']"),
                    Data.NewActivity.Section);
                ComboBoxHelper.SelectElement(By.XPath("//select[@ng-model = 'model.activityCategoryId']"), Data.NewActivity.Type);
                Thread.Sleep(5000);
                if (Data.NewActivity.HideActivityEndDate)
                {
                    CheckBoxHelper.CheckedCheckBox(By.XPath("//md-checkbox[@ng-model = 'model.hideActivityEndDate']"));
                }
                Thread.Sleep(5000);
                driver.FindElement(By.Id("activeDateRange")).Click();
                Thread.Sleep(3000);
                var element = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[contains(@class,'dropup') and contains(@style,'display: block')]/div[contains(@class,'ranges')]/ul/li[contains(.,'{0}')]", Data.NewActivity.ActivityRangeMonths))));
                element.Click();
                Thread.Sleep(5000);
                driver.FindElement(By.XPath("//button[contains(.,'Next')]")).Click();

                Thread.Sleep(2000);
                Console.WriteLine("Step 2 loaded");
                Assert.IsTrue(driver.FindElement(By.XPath("//md-checkbox[@ng-model='model.hideNonPointAwardsInActivityHistory']")).Displayed, "Hide Points Awards in activity checkbox must be displayed");
                Console.WriteLine("Hide non-point award transactions in particpant's Activity History Checkbox displayed");

                IWebElement pointsDisplayAttributeLabel = driver.FindElement(By.XPath("/html/body/div[1]/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div[3]/div[1]/div[1]/label"));

                Assert.IsTrue(pointsDisplayAttributeLabel.Displayed, "Points Display Attribute must be displayed");
                Console.WriteLine("Points Display Attribute displayed");

                Assert.IsTrue(pointsDisplayAttributeLabel.Text == Data.Content.PointsDisplayedAttributeLabel, "Points Displayed Attribute label text must be matched");
                Console.WriteLine("Points Display Attribute Label Text matching");
                IWebElement pointsTextSection = driver.FindElement(By.XPath("/html/body/div[1]/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div[3]/div[1]/div[2]"));
                Console.WriteLine(pointsTextSection.Text);
                Assert.IsTrue(pointsTextSection.Text.Equals(Data.Content.PointsDisplayedText), "Points section content must be matching");
                Console.WriteLine("Points Display Attribute content matching");
                Assert.IsTrue(driver.FindElement(By.XPath("/html/body/div[1]/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div[3]/div[2]/label")).Text == Data.Content.MinimumPointsLabel, "Minimum Points label must be matching");
                Console.WriteLine("Minimum Points label matching");
                Assert.IsTrue(driver.FindElement(By.XPath("/html/body/div[1]/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div[3]/div[3]/label")).Text == Data.Content.MaximumPointsLabel, "Maximum Points label must be matching");
                Console.WriteLine("Maximum Points label matching");

            }
            catch (Exception ex)
            {
                Console.WriteLine("PIV-1530 Scenario 2 Error: {0}", ex.Message);
                Assert.Fail("PIV-1530 Scenario 2 Failed. Error: {0}", ex);
            }
        }



        #region Utilities
        private void LogoutAdministrator()
        {
            Thread.Sleep(3000);
            var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
            if (dropdownMenuElement.Displayed)
            {
                dropdownMenuElement.Click();

                var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    logoutElement.Click();
                    Console.WriteLine("Administrator logged off successfully");
                }
                else
                {
                    Console.Error.WriteLine("Logout link not clickable");
                    Assert.Fail("Logout link not clickable");
                }

            }
            else
            {
                Console.Error.WriteLine("Logout Dropdown not visible");
                Assert.Fail("Logout Dropdown not visible");
            }
        }



        private static void LoginAndSelectActivity()
        {
            var loginPage = new LoginPage(driver);
            loginPage.LoginTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Username);
            loginPage.PassTextBox.SendKeys(ObjectRepository.Setting.Credentials.Administrator.Password);
            loginPage.LoginButton.Click();


            Thread.Sleep(1000);
            Console.WriteLine("Administrator Logged in successfully");

            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                Console.WriteLine("Clients link clicked");
            }
            else
            {
                Console.WriteLine("Clients Link not found");
                Assert.Fail("Clients link not found");
            }


            var clientElement = adminPage.GetClient(ObjectRepository.Setting.Credentials.Administrator.Client);
            if (clientElement != null && clientElement.Displayed)
            {
                Console.WriteLine("Client {0} selected", ObjectRepository.Setting.Credentials.Administrator.Client);
                clientElement.Click();
            }
            else
            {
                Console.WriteLine("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
                Assert.Fail("Client {0} not found", ObjectRepository.Setting.Credentials.Administrator.Client);
            }

            var programElement = adminPage.GetProgram(ObjectRepository.Setting.Credentials.Administrator.Program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                Console.WriteLine("Program {0} selected", ObjectRepository.Setting.Credentials.Administrator.Program);
            }
            else
            {
                Console.WriteLine("Program {0} not found", ObjectRepository.Setting.Credentials.Administrator.Program);
            }

            if (adminPage.ProgramTabs.Count > 0)
            {
                Console.WriteLine("Program Listing page loaded");

            }
            else
            {
                Console.WriteLine("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }

            Thread.Sleep(1000);
            var activitiesTab = adminPage.GetProgramTab("Activities");
            if (activitiesTab == null)
            {
                Console.WriteLine("Unable to find activities tab");
                Assert.Fail("Unable to find activities tab");
            }
            else
            {
                Console.WriteLine("Activities Tab loaded");
                activitiesTab.Click();
            }


            By loadingImage = By.TagName("http-busy");
            Thread.Sleep(6000);
            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

        }

        private PIV1530Data GetData()
        {
            PIV1530Data data = null;
            try
            {
                string fileData = File.ReadAllText(TestContext.CurrentContext.TestDirectory + "//Data//PIV1530.json");
                data = JsonConvert.DeserializeObject<PIV1530Data>(fileData);
            }
            catch
            {
                // Do nothing
            }
            return data;
        }

        private static WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
        #endregion
    }
}
